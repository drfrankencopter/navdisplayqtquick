import QtQuick 2.0
import QtQuick.Controls 2.15
Item {
    property bool checked
    property var text
    property alias checkState: checkbox.checkState
    height:50
    CheckBox{
        id: checkbox
        anchors{
            right: parent.right
            rightMargin: parent.width/6
        }
        checked: parent.checked
        onCheckStateChanged: parent.checkStateChanged()
    }
    Label{
        anchors{
            verticalCenter: checkbox.verticalCenter
            left: parent.left
            leftMargin: 10
        }
        text:parent.text
        font{
            pointSize: 12
        }
    }
}
