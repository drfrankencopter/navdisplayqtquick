import QtQuick 2.0

Rectangle {
    id: _root
    property var buttonText
    property var textColor
    property var textHoverColor
    property var normalColor
    property var hoverColor

    signal pressed

    property var _onPressed: onPressed ? onPressed : {}
    property var _buttonText: buttonText ? buttonText : "Text not set"
    property var _textColor: textColor ? textColor : "black"
    property var _textHoverColor: textHoverColor ? textHoverColor : _textColor
    property var _normalColor: normalColor ? normalColor : paletteHolder.hudColor
    property var _hoverColor: hoverColor ? hoverColor : paletteHolder.hudHoverColor

    width: buttonTextObject.implicitWidth + 30
    height: buttonTextObject.implicitHeight + 30
    color: _normalColor
    radius: 10

    Text {
        id: buttonTextObject
        anchors.centerIn: parent
        text: _root.buttonText
        color: _root._textColor
    }

    HoverArea {
        target: parent
        anchors.fill: parent
        colorChange: true
        normalColor: _root._normalColor
        hoverColor: _root._hoverColor
        onClicked: {
            _root.pressed()
        }
        onHoverStart: {
            enteredColorAnimation.start()
        }

        onHoverEnd: {
            exitedColorAnimation.start()
        }
    }

    ColorAnimation {
        id: enteredColorAnimation
        target: buttonTextObject
        property: "color"
        from: _root._textColor
        to: _root._textHoverColor
        duration: 200
        easing.type: Easing.InOutQuad
    }

    ColorAnimation {
        id: exitedColorAnimation
        target: buttonTextObject
        property: "color"
        from: _root._textHoverColor
        to: _root._textColor
        duration: 200
        easing.type: Easing.InOutQuad
    }
}
