import QtQuick 2.0
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.15
import QtQuick.Dialogs 1.3

Item{
    id:root
    property alias dialog: colorDialog
    property string color
    property var text
    height:50
    ColorDialog{
        id:colorDialog
        title:"Select Colour"
        onAccepted: {
            root.color= colorDialog.currentColor
        }
    }
    Label{
        text:parent.text
        anchors{
            verticalCenter: colorButton.verticalCenter
            left: parent.left
            leftMargin: 10
        }
        font{
            pointSize: 12
        }
    }
    Button{
        id:colorButton
        anchors{
            right: parent.right
            rightMargin: parent.width/6
        }
        onClicked: {
            colorDialog.open()
        }
        ColorOverlay{
            anchors.fill:colorButton
            source:colorButton
            color: root.color
        }
    }
}
