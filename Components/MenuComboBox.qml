import QtQuick 2.0
import QtQuick.Controls 2.15

Item {
    property alias model: comboBox.model
    property alias combobox: comboBox
    property var text
    signal valueChanged
    signal currentTextChanged
    height:50
    Label{
        text:parent.text
        anchors{
            verticalCenter: comboBox.verticalCenter
            left: parent.left
            leftMargin: 10
        }
        font{
            pointSize: 12
        }
    }
    ComboBox{
        id:comboBox
        anchors{
            right: parent.right
            rightMargin: parent.width/6

        }
        font.pointSize: 12
        onCurrentTextChanged: parent.currentTextChanged()
    }
}
