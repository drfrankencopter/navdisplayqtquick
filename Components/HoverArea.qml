import QtQuick 2.0

MouseArea {
    id: _root

    // Settable properties
    property var    target

    property bool   colorChange
    property color  normalColor
    property color  hoverColor

    property bool   opacityChange
    property real   normalOpacity
    property real   hoverOpacity

    // Local properties
    property var    _target: target ? target : parent

    property bool   _colorChange: colorChange ? colorChange : false
    property color  _normalColor: normalColor
    property color  _hoverColor: hoverColor

    property bool   _opacityChange: opacityChange ? opacityChange : false
    property real   _normalOpacity: normalOpacity ? normalOpacity : 0.5
    property real   _hoverOpacity: hoverOpacity ? hoverOpacity : 1

    signal hoverStart
    signal hoverEnd

    hoverEnabled: true

    onEntered: {
        _root.hoverStart()

        cursorShape = Qt.PointingHandCursor
        if (_colorChange)
            enteredColorAnimation.start()
        if (_opacityChange)
            enteredOpacityAnimation.start()
    }

    onExited: {
        _root.hoverEnd()

        cursorShape = Qt.ArrowCursor
        if (_colorChange)
            exitedColorAnimation.start()
        if (_opacityChange)
            exitedOpacityAnimation.start()

    }

    ColorAnimation {
        id: enteredColorAnimation
        target: _target
        property: "color"
        from: _normalColor
        to: _hoverColor
        duration: 200
        easing.type: Easing.InOutQuad
    }

    ColorAnimation {
        id: exitedColorAnimation
        target: _target
        property: "color"
        from: _hoverColor
        to: _normalColor
        duration: 200
        easing.type: Easing.InOutQuad
    }

    NumberAnimation {
        id: enteredOpacityAnimation
        target: _target
        property: "opacity"
        from: _normalOpacity
        to: _hoverOpacity
        duration: 200
        easing.type: Easing.InOutQuad
    }

    NumberAnimation {
        id: exitedOpacityAnimation
        target: _target
        property: "opacity"
        from: _hoverOpacity
        to: _normalOpacity
        duration: 200
        easing.type: Easing.InOutQuad
    }
}
