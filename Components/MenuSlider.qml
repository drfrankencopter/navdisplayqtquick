import QtQuick 2.0
import QtQuick.Controls 2.15

Item {
    property bool showValue: false
    property var text
    property alias slider: slider
    height:50
    Slider{
        id:slider
        anchors{
            right:parent.right
        }
        width: parent.width*0.5
        stepSize: 1
        property Rectangle normalHandle: Rectangle{
            x: slider.leftPadding + slider.visualPosition * (slider.availableWidth - width)
            y: slider.topPadding + slider.availableHeight / 2 - height / 2
            implicitWidth: 18
            implicitHeight: 18
            radius: 13
            color: slider.pressed ? "#f0f0f0" : "#f6f6f6"
            border.color: "#bdbebf"
        }
        property Rectangle valueHandle: Rectangle{
            x: slider.leftPadding + slider.visualPosition * (slider.availableWidth - width)
            y: slider.topPadding + slider.availableHeight / 2 - height / 2
            implicitWidth: valueText.width>14 ? valueText.width+10: 18
            implicitHeight: 18
            radius: 5
            color: slider.pressed ? "#f0f0f0" : "#f6f6f6"
            border.color: "#bdbebf"
            Label{
                id:valueText
                text:slider.value
                anchors.centerIn: parent
            }
        }
        handle:parent.showValue?valueHandle:normalHandle
    }
    Label{
        text:parent.text
        anchors{
            verticalCenter: slider.verticalCenter
            left: parent.left
            leftMargin: 10
        }
        font{
            pointSize: 12
        }
    }
}
