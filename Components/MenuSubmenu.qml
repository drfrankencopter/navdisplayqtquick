import QtQuick 2.0

Rectangle{
    id:submenu
    property var menuHeight
    property alias openAnim: slideDownAnim
    property alias closeAnim: slideUpAnim
    property string text:""
    Text{
        anchors{
            top:parent.top
            horizontalCenter: parent.horizontalCenter
        }
        text:parent.text
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(submenu.height===0){
                    slideDownAnim.start()
                }else{
                    slideUpAnim.start()
                }
            }
        }
    }

    radius:10
    anchors{
        right:parent.right
        left:parent.left
        margins:10
    }
    height:menuHeight
    clip:true

    NumberAnimation {
        id:slideDownAnim
        target: submenu
        property: "height"
        from:0
        to:menuHeight
        duration: 300
        easing.type: Easing.OutQuart
    }
    NumberAnimation {
        id:slideUpAnim
        target: submenu
        property: "height"
        to:0
        from:menuHeight
        duration: 300
        easing.type: Easing.OutQuart
    }
}
