#include "unitsconverter.h"
#include "enums.h"

UnitsConverter::UnitsConverter(NavSettings* navSettings, QObject* parent)
    : QObject(parent)
    , _navSettings(navSettings)
{
    // We initialize the state of the unitsConverter to reflect the navSettings
    auto hu = _determineUnits(_navSettings->horizontalUnits());
    _horizontalFactor = hu.first;
    _horizontalUnits  = hu.second;

    auto vu = _determineUnits(_navSettings->verticalUnits());
    _verticalFactor = vu.first;
    _verticalUnits  = vu.second;

    auto hsu = _determineSpeedUnits(_navSettings->horizontalSpeedUnits());
    _horizontalSpeedFactor = hsu.first;
    _horizontalSpeedUnits = hsu.second;

    auto vsu = _determineSpeedUnits(_navSettings->verticalSpeedUnits());
    _verticalSpeedFactor    = vsu.first;
    _verticalSpeedUnits     = vsu.second;

    // The state of the members will then be updated according to the changes in the navSettings
    connect(navSettings, &NavSettings::horizontalUnitsChanged,      this,   &UnitsConverter::handleHorizontalUnitsChanged);
    connect(navSettings, &NavSettings::verticalUnitsChanged,        this,   &UnitsConverter::handleVerticalUnitsChanged);
    connect(navSettings, &NavSettings::horizontalSpeedUnitsChanged, this,   &UnitsConverter::handleHorizontalSpeedUnitsChanged);
    connect(navSettings, &NavSettings::verticalSpeedUnitsChanged,   this,   &UnitsConverter::handleVerticalSpeedUnitsChanged);
}

std::pair<double, QString> UnitsConverter::_determineUnits(int units_enum)
{
    static constexpr double M_TO_M = 1;
    static constexpr double M_TO_FEET = 3.28084;
    static constexpr double M_TO_NAUT_MILES = 0.000539957;

    std::pair<double, QString> result;
    switch (units_enum) {
    case Enums::DIST_UNITS::Metric:
        result.first    = M_TO_M;
        result.second   = QString("m");
        break;
    case Enums::DIST_UNITS::Imperial:
        result.first    = M_TO_FEET;
        result.second   = QString("ft");
        break;
    case Enums::DIST_UNITS::Nautical:
        result.first    = M_TO_NAUT_MILES;
        result.second   = QString("NM");
        break;
    default:
        break;
    }

    return result;
}
std::pair<double, QString> UnitsConverter::_determineSpeedUnits(int units_enum)
{
    // MS is meters per sec
    static constexpr double MS_TO_MS = 1;
    static constexpr double MS_TO_FT_PER_MIN = 196.85;
    static constexpr double MS_TO_KT = 1.94384;

    std::pair<double, QString> result;
    switch (units_enum) {
    case Enums::SPEED_UNITS::MetersPerSec:
        result.first    = MS_TO_MS;
        result.second   = QString("m/s");
        break;
    case Enums::SPEED_UNITS::FtPerMin:
        result.first    = MS_TO_FT_PER_MIN;
        result.second   = QString("ft/min");
        break;
    case Enums::SPEED_UNITS::Knots:
        result.first    = MS_TO_KT;
        result.second   = QString("kt");
        break;
    default:
        break;
    }

    return result;
}

void UnitsConverter::handleHorizontalUnitsChanged(int newUnitsEnum)
{
    auto hu = _determineUnits(newUnitsEnum);
    _horizontalFactor = hu.first;
    _horizontalUnits  = hu.second;

    emit horizontalFactorChanged(_horizontalFactor);
    emit horizontalUnitsChanged(_horizontalUnits);
}

void UnitsConverter::handleVerticalUnitsChanged(int newUnitsEnum)
{
    auto vu = _determineUnits(newUnitsEnum);
    _verticalFactor = vu.first;
    _verticalUnits  = vu.second;

    emit verticalFactorChanged(_verticalFactor);
    emit verticalUnitsChanged(_verticalUnits);
}

void UnitsConverter::handleHorizontalSpeedUnitsChanged(int newUnitsEnum)
{
    auto hsu = _determineSpeedUnits(newUnitsEnum);
    _horizontalSpeedFactor = hsu.first;
    _horizontalSpeedUnits = hsu.second;

    emit horizontalSpeedFactorChanged(_horizontalSpeedFactor);
    emit horizontalSpeedUnitsChanged(_horizontalSpeedUnits);
}

void UnitsConverter::handleVerticalSpeedUnitsChanged(int newUnitsEnum)
{
    auto vsu = _determineSpeedUnits(newUnitsEnum);
    _verticalSpeedFactor    = vsu.first;
    _verticalSpeedUnits     = vsu.second;

    emit verticalSpeedFactorChanged(_verticalSpeedFactor);
    emit verticalSpeedUnitsChanged(_verticalSpeedUnits);
}
