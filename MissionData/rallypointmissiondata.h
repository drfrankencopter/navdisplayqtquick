#ifndef RALLYPOINTMISSIONDATA_H
#define RALLYPOINTMISSIONDATA_H

#include "missiondata.h"
#include <QGeoCoordinate>

// Holds the rally points mission items

class RallyPointMissionData : public MissionData
{
    Q_OBJECT

public:
    RallyPointMissionData(missionData_t* mission_data, SharedMemoryThread* activeLookThread);

    Q_INVOKABLE int rallyPointCount() const;

protected:
    MAV_MISSION_TYPE _getMissionType() const override { return MAV_MISSION_TYPE_RALLY; }
};

#endif // RALLYPOINTMISSIONDATA_H
