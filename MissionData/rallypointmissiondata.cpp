#include "rallypointmissiondata.h"

RallyPointMissionData::RallyPointMissionData(missionData_t* mission_data, SharedMemoryThread* activeLookThread)
    : MissionData(mission_data, MAV_MISSION_TYPE_RALLY, activeLookThread)
{
}

int RallyPointMissionData::rallyPointCount() const
{
    return static_cast<int>(_items.size());
}
