#include "fencemissiondata.h"

FenceMissionData::FenceMissionData(missionData_t* mission_data, SharedMemoryThread* activeLookThread)
    : MissionData(mission_data, MAV_MISSION_TYPE_FENCE, activeLookThread)
{
}

int FenceMissionData::fenceCount() const
{
    return _items.size();
}

int FenceMissionData::getFenceType(int index) const
{
    return _items.at(index).command;
}

QList<QVariant> FenceMissionData::getPolygonPath(int index_start) const
{
    QList<QVariant> path;
    int vertex_count = getParam(index_start, 1);

    for (int i = 0; i < vertex_count; ++i)
    {
        QGeoCoordinate vertex_coords = getCoordinates(index_start + i);
        path.append(QVariant::fromValue(QGeoCoordinate(vertex_coords.latitude(), vertex_coords.longitude())));
    }

    return path;
}

int FenceMissionData::getNextFenceIndex(int index_start) const
{
    int next_fence_index = index_start;
    int type = getFenceType(index_start);
    bool isAPolygon = type == MAV_CMD_NAV_FENCE_POLYGON_VERTEX_INCLUSION || type == MAV_CMD_NAV_FENCE_POLYGON_VERTEX_EXCLUSION;
    if (isAPolygon)
    {
        // Go at the end of the polygon
        // Param1 is the number of vertices
        next_fence_index += getParam(index_start, 1);
    }
    else
    {
        ++next_fence_index;
    }

    // Make sure we don't return a out of bounds index.
    if (next_fence_index > static_cast<int>(fenceCount()))
    {
        next_fence_index = -1;
    }

    return next_fence_index;
}
