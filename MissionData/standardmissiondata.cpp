#include <QDebug>
#include "standardmissiondata.h"
#include "enums.h"
#include <limits>

StandardMissionData::StandardMissionData(missionData_t* mission_data, SharedMemoryThread* activeLookThread)
    : MissionData(mission_data, MAV_MISSION_TYPE_MISSION, activeLookThread)
{

    QObject::connect(activeLookThread, &SharedMemoryThread::missionCurrentChanged, this, &StandardMissionData::missionCurrentChanged);

    _missionCount = mission_data->missionCount;
    _currentMissionItem = mission_data->myCurrentMission.currentMissionItem;
    _isMissionStarted = mission_data->myCurrentMission.missionStarted;
    _parseWaypoints();
}

void StandardMissionData::_parseWaypoints()
{
    _waypoints.clear();

    float currentSpeed = 0;

    for (auto item = std::begin(_items); item != std::end(_items); item++)
    {
        if (_isAWaypoint(item->command))
        {
            _waypoints.emplace_back(&(*item), currentSpeed);
        }
        else if (item->command == MAV_CMD_DO_CHANGE_SPEED)
        {
            if (item->param2 >= 0)
                currentSpeed = item->param1;
        }
    }
}

bool StandardMissionData::_isAWaypoint(int type) const
{
    return type == Enums::WAYPOINT_TYPE::Waypoint
            || type == Enums::WAYPOINT_TYPE::Takeoff
            || type == Enums::WAYPOINT_TYPE::Land;
}

int StandardMissionData::waypointCount() const
{
    return static_cast<int>(_items.size());
}

int StandardMissionData::waypointType(int index) const
{
    return _items.at(index).command;
}

double StandardMissionData::waypointLat(int index) const
{
    return _items.at(index).x * 1E-7;
}

double StandardMissionData::waypointLon(int index) const
{
    return _items.at(index).y * 1E-7;
}

float StandardMissionData::waypointAlt(int index) const
{
    return _items.at(index).z;
}

float StandardMissionData::waypointAcceptanceRadius(int index) const
{
    if (waypointType(index) == Enums::WAYPOINT_TYPE::Land)
    {
        return _items.at(index).param3;
    }
    else
    {
        return _items.at(index).param2;
    }
}

int StandardMissionData::getCurrentWaypoint() const
{
    return _currentMissionItem;
}

int StandardMissionData::getMissionNumber() const
{
    return _missionCount;
}

int StandardMissionData::actualWaypointCount() const
{
    return _waypoints.size();
}

Waypoint* StandardMissionData::get(int index)
{

    if (index >= static_cast<int>(_waypoints.size()) || index < 0)
    {
        return nullptr;
    }

    Waypoint* waypoint = &_waypoints.at(index);

    // Previously caused problems in QML with garbage collection
    // Ref: https://stackoverflow.com/questions/33792876/qml-garbage-collection-deletes-objects-still-in-use
    // Solution is the folloing line:
    QQmlEngine::setObjectOwnership(waypoint, QQmlEngine::CppOwnership);
    return waypoint;
}

Waypoint* StandardMissionData::get_seq(int seq)
{
    auto it = std::find_if(_waypoints.begin(), _waypoints.end(), [seq](const Waypoint& wpt) { return wpt.seq() == seq; });
    if (it != _waypoints.end())
    {
        return &(*it);
    }
    else
    {
        return nullptr;
    }
}

int StandardMissionData::seqToIndex(int seq)
{
    for(int i =0; i<_waypoints.size() ; ++i)
    {
        if(_waypoints[i].seq() == seq)
            return i;
    }
    return -1;
}

void StandardMissionData::missionCurrentChanged(int newCurrentItem)
{
    _currentMissionItem = newCurrentItem;
    emit newMissionCurrent(_currentMissionItem);
}

void StandardMissionData::_initMission(missionData_t* mission_data)
{
    _parseWaypoints();

    _missionCount = mission_data->missionCount;
    _currentMissionItem = mission_data->myCurrentMission.currentMissionItem;
    _isMissionStarted = mission_data->myCurrentMission.missionStarted;

    emit actualWaypointCountChanged(actualWaypointCount());
}

void StandardMissionData::_clearMission()
{
    _currentMissionItem = 0;
    _missionCount = 0;
    _isMissionStarted = 0;
}
