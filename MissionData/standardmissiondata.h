#ifndef STANDARDMISSIONDATA_H
#define STANDARDMISSIONDATA_H
#include <QList>
#include "missiondata.h"
#include "waypoint.h"

// Holds the actual mission items of the main current mission. Waypoints, change in speed, etc.
// There are a lot of utility functions because there are a lot of different types of items encoded
// in the same mission type items.

class StandardMissionData : public MissionData
{
    Q_OBJECT
public:
    StandardMissionData(missionData_t* mission_data, SharedMemoryThread* activeLookThread);

    Q_PROPERTY(int actualWaypointCount READ actualWaypointCount NOTIFY actualWaypointCountChanged)

    Q_INVOKABLE int         waypointCount() const;
    Q_INVOKABLE int         waypointType(int index) const;
    Q_INVOKABLE double      waypointLat(int index) const;
    Q_INVOKABLE double      waypointLon(int index) const;
    Q_INVOKABLE float       waypointAlt(int index) const;
    Q_INVOKABLE float       waypointAcceptanceRadius(int index) const;
    Q_INVOKABLE int         getCurrentWaypoint() const;
    Q_INVOKABLE int         getMissionNumber() const;

    // Functions for the list of actual waypoints
    Q_INVOKABLE int         actualWaypointCount() const;    // Count of waypoints, without counting non-waypoints mission items.
    Q_INVOKABLE Waypoint*   get(int index);                 // Get based on the index in the list
    Q_INVOKABLE Waypoint*   get_seq(int seq_number);        // Get based on the seq number stored inside each waypoint
    Q_INVOKABLE int         seqToIndex(int seq);            // Seq number to the index in the list

public slots:
    void    missionCurrentChanged(int newCurrentItem);

signals:
    void    newMissionCurrent(int newCurrentItem);
    void    actualWaypointCountChanged(int newCount);

protected:
    MAV_MISSION_TYPE _getMissionType() const override { return MAV_MISSION_TYPE_MISSION; }
    void    _initMission(missionData_t* mission_data) override;
    void    _clearMission() override;

private:
    void    _parseWaypoints();
    bool    _isAWaypoint(int type) const;

    int     _missionCount;
    int     _currentMissionItem;
    bool    _isMissionStarted;

    std::vector<Waypoint> _waypoints;
};

#endif // STANDARDMISSIONDATA_H
