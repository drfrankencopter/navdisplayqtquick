#include "missiondata.h"
#include "missionmanager.h"
#include <cassert>
#include <QDebug>

MissionData::MissionData(missionData_t* mission_data, MAV_MISSION_TYPE mission_type, SharedMemoryThread* activeLookThread)
    : _activeLookThread(activeLookThread)
{
    missionType_t* specificMission = getSpecificMissionType(&mission_data->myCurrentMission, mission_type);

    for (int i = 0; i < specificMission->numMissionItems; ++i)
    {
        _items.push_back(specificMission->items[i]);
    }

    assert(static_cast<int>(_items.size()) == specificMission->numMissionItems);

    QObject::connect(_activeLookThread, &SharedMemoryThread::missionNumberChanged, this, &MissionData::handleNewMission);
}

MissionData::~MissionData()
{
    QObject::disconnect(_activeLookThread, &SharedMemoryThread::missionNumberChanged, this, &MissionData::handleNewMission);
}

void MissionData::handleNewMission(missionData_t* mission_data)
{
    missionType_t* specificMission = getSpecificMissionType(&mission_data->myCurrentMission, _getMissionType());

    // Remove old mission
    _clearMission();
    _items.clear();

    // Start updating for the new mission
    for (int i = 0; i < specificMission->numMissionItems; ++i)
    {
        _items.push_back(specificMission->items[i]);
    }

    _initMission(mission_data);

    assert(_items.size() == static_cast<size_t>(specificMission->numMissionItems));

    emit newMissionDownloaded(_getMissionType());
}

float MissionData::getParam(int index,int param) const
{
    switch(param)
    {
    case 1:
        return _items.at(index).param1;
    case 2:
        return _items.at(index).param2;
    case 3:
        return _items.at(index).param3;
    case 4:
        return _items.at(index).param4;
    case 5:
        return _items.at(index).x;
    case 6:
        return _items.at(index).y;
    case 7:
        return _items.at(index).z;
    default:
        qDebug() << "Accessing out of bounds parameter " << param;
        return std::numeric_limits<float>::max();
    }
}

QGeoCoordinate MissionData::getCoordinates(int index) const
{
    const auto* item = &_items.at(index);
    double lat = item->x * 1E-7;
    double lon = item->y * 1E-7;
    return {lat, lon, item->z};
}

QString MissionData::getCoordinatesFormatted(int index) const
{
    QGeoCoordinate coords = getCoordinates(index);
    QString formattedCoords = "{ latitude: %1; longitude: %2; }";

    formattedCoords = formattedCoords.arg(coords.latitude());
    formattedCoords = formattedCoords.arg(coords.longitude());

    return formattedCoords;
}

int MissionData::getFrame(int index) const
{
    return _items.at(index).frame;
}

void MissionData::printItemDetails(const mavlink_mission_item_int_t& i) const
{
    qDebug() << "Param1: " << i.param1;
    qDebug() << "Param2: " << i.param2;
    qDebug() << "Param3: " << i.param3;
    qDebug() << "Param4: " << i.param4;
    qDebug() << "x: " << i.x;
    qDebug() << "y: " << i.y;
    qDebug() << "z: " << i.z;
}

void MissionData::printItemDetails(int index) const
{
    const mavlink_mission_item_int_t* item = &_items.at(index);
    qDebug() << "Param1: " << item->param1;
    qDebug() << "Param2: " << item->param2;
    qDebug() << "Param3: " << item->param3;
    qDebug() << "Param4: " << item->param4;
    qDebug() << "x: " << item->x;
    qDebug() << "y: " << item->y;
    qDebug() << "z: " << item->z;
}
