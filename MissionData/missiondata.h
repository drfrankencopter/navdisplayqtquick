#ifndef MISSIONDATA_H
#define MISSIONDATA_H

#include <QObject>
#include <QGeoCoordinate>
#include <standard/mavlink.h>
#include <vector>
extern "C" {    // Otherwise can't see the getSpecificMission function in C++
#include "mavNrc/globals.h"
}
#include "sharedmemorythread.h"
#include "enums.h"

class MissionManager;

// MissionData is the base class of mission data that holds a list of mission items. It reacts/handles
// when the mission manager detects a new mission.

class MissionData : public QObject
{
    Q_OBJECT
public:
    MissionData(missionData_t* mission_data, MAV_MISSION_TYPE mission_type, SharedMemoryThread* activeLookThread);
    ~MissionData();

    Q_INVOKABLE float getParam(int index,int param) const;
    Q_INVOKABLE void printItemDetails(const mavlink_mission_item_int_t& i) const;
    Q_INVOKABLE void printItemDetails(int index) const;
    Q_INVOKABLE QGeoCoordinate getCoordinates(int index) const;
    Q_INVOKABLE QString getCoordinatesFormatted(int index) const;
    Q_INVOKABLE int getFrame(int index) const;

signals:
    void newMissionDownloaded(int mission_type);

public slots:
    void handleNewMission(missionData_t* mission_data);

protected:
    virtual MAV_MISSION_TYPE _getMissionType() const = 0;
    virtual void _initMission(missionData_t* mission_data) { Q_UNUSED(mission_data); } // Template method to add additional init for derived classes.
    virtual void _clearMission() { _items.clear(); }

    std::vector<mavlink_mission_item_int_t> _items;

private:
    SharedMemoryThread* _activeLookThread;
};

#endif // MISSIONDATA_H
