#ifndef WAYPOINT_H
#define WAYPOINT_H
#include <QObject>
#include <QQmlApplicationEngine>
#include "mavNrc/globals.h"

class Waypoint : public QObject
{
    Q_OBJECT

public:
    Waypoint(): _item(nullptr) {};
    Waypoint(mavlink_mission_item_int_t* item, float speed);
    ~Waypoint() = default;
    Waypoint(Waypoint&& other);
    Waypoint(const Waypoint& other);
    Waypoint& operator=(const Waypoint& other);
    Waypoint& operator=(Waypoint&& other);

    Q_PROPERTY(float    speed               READ speed              CONSTANT)
    Q_PROPERTY(double   lat                 READ lat                CONSTANT)
    Q_PROPERTY(double   lon                 READ lon                CONSTANT)
    Q_PROPERTY(float    alt                 READ alt                CONSTANT)
    Q_PROPERTY(int      command             READ command            CONSTANT)
    Q_PROPERTY(float    acceptanceRadius    READ acceptanceRadius   CONSTANT)
    Q_PROPERTY(int      seq                 READ seq                CONSTANT)

    float   speed()             const   { return _speed; }
    double  lat()               const   { return _item->x * 1E-7; }
    double  lon()               const   { return _item->y * 1E-7; }
    float   alt()               const   { return _item->z; }
    int     command()           const   { return _item->command; }
    float   acceptanceRadius()  const   { return _item->command == MAV_CMD_NAV_LAND ? _item->param3 : _item->param2; }
    int     seq()               const   { return _item->seq; }

private:
    mavlink_mission_item_int_t* _item;
    float _speed;
};

#endif // WAYPOINT_H
