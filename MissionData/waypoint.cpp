#include "waypoint.h"

Waypoint::Waypoint(mavlink_mission_item_int_t* item, float speed)
{
    _item   = item;
    _speed  = speed;
}

Waypoint::Waypoint(Waypoint&& other)
{
    std::swap(_item, other._item);
    _speed  = other._speed;
}

Waypoint::Waypoint(const Waypoint& other)
{
    _item   = other._item;
    _speed  = other._speed;
}

Waypoint& Waypoint::operator=(const Waypoint& other)
{
    _item   = other._item;
    return *this;
}

Waypoint& Waypoint::operator=(Waypoint&& other)
{
    std::swap(_item, other._item);
    _speed  = other._speed;
    return *this;
}
