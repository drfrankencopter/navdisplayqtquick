#ifndef FENCEMISSIONDATA_H
#define FENCEMISSIONDATA_H
#include <QObject>
#include <QString>
#include <QQmlListProperty>
#include <QGeoCoordinate>
#include "missiondata.h"

// Holds the geofences mission items. There are some more utility methods because managing the mission items
// of the geofences requires more gymnastics since most fences have different types and are spread across
// several mission items.

class FenceMissionData : public MissionData
{
    Q_OBJECT

public:
    FenceMissionData(missionData_t* mission_data, SharedMemoryThread* activeLookThread);

protected:
    MAV_MISSION_TYPE _getMissionType() const override { return MAV_MISSION_TYPE_FENCE; }

    Q_INVOKABLE int fenceCount() const;
    Q_INVOKABLE int getFenceType(int index) const;
    Q_INVOKABLE QList<QVariant> getPolygonPath(int index_start) const;
    Q_INVOKABLE int getNextFenceIndex(int index_start) const;
};

#endif // FENCEMISSIONDATA_H
