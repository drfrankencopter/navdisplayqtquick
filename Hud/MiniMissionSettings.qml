import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.3
import NavEnums 1.0
Item {
    id:root
    anchors.fill:parent
    property alias statusText: statusText
    function newCurrent(){
        rejoinWaypointInput.text = missionManager.standard.get(missionManager.standard.seqToIndex(missionManager.standard.getCurrentWaypoint())+1).seq +1
        deviateWaypointInput.text =missionManager.standard.getCurrentWaypoint() +1;
    }

    Rectangle{
        anchors{
            left:parent.left
            verticalCenter: parent.verticalCenter
        }
        radius:4
        opacity:0.8
        height:parent.height/2
        width:150
        color:Qt.rgba(0.1,0.1,0.1,0.8)
        Text{
            id:statusText
            text:"No Active Mini Mission"
            color:"white"
            anchors{
                topMargin: 5
                top:parent.top
                right:parent.right
                left:parent.left
            }
            height:30
            font{
                bold:true
                pixelSize: 15
            }
            wrapMode: Text.Wrap
            elide:Text.ElideLeft
            horizontalAlignment: Text.AlignHCenter
        }
        Rectangle{
            id:startMiniMissionButton
            anchors{
                right:parent.right
                left:parent.left
                bottom:parent.bottom
                margins:5
            }
            radius:10
            visible:mapHud.deviateState === Enums.NoMission
            height:40
            color:Qt.rgba(0.1,0.1,0.1,0.8)
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    mapHud.deviateState=Enums.Viewing
                    mapOverlay.setupMiniMission()
                }
            }
            Text{
                anchors.centerIn: parent
                text:"New Mini Mission"
                color:"white"
            }
        }
        Rectangle{
            id:cancelMiniMission
            anchors{
                right:parent.right
                left:parent.left
                bottom:executeMiniMission.top
                margins:5
            }
            radius:10
            visible:mapHud.deviateState !== Enums.NoMission
            height:40
            color:Qt.rgba(0.1,0.1,0.1,0.8)
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    cancelConfirmDialog.open()
                }
            }
            Text{
                anchors.centerIn: parent
                text:"Cancel Mini Mission"
                color:"white"
            }
        }
        Dialog{
            id:cancelConfirmDialog
            title: "Cancel Mini Mission?"
            standardButtons: StandardButton.Yes | StandardButton.No
            onYes:{
                mapHud.deviateState=Enums.Adding
                mapOverlay.endMiniMission()
                close()
            }
            onNo:{
                close()
            }
        }

        Rectangle{
            id:toggleAddMiniMissionWaypoints
            anchors{
                right:parent.right
                left:parent.left
                bottom:cancelMiniMission.top
                margins:5
            }
            color:Qt.rgba(0.1,0.1,0.1,0.8)
            radius:10
            height:40
            visible:mapHud.deviateState!==Enums.NoMission
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if(mapHud.deviateState===Enums.Adding){
                        mapHud.deviateState=Enums.Viewing
                    }else{
                        mapHud.deviateState=Enums.Adding
                    }
                }
            }
            Text{
                id:toggleAddText
                color:"white"
                text:mapHud.deviateState===Enums.Adding?"Stop":"Add"
                anchors.centerIn: parent
            }
        }
        Rectangle{
            id:executeMiniMission
            anchors{
                right:parent.right
                left:parent.left
                bottom:parent.bottom
                margins:5
            }
            color:Qt.rgba(0.1,0.1,0.1,0.8)
            radius:10
            height:40
            visible:mapHud.deviateState!==Enums.NoMission
            MouseArea{
                anchors.fill: parent
                onClicked: {

                }
            }
            Text{
                id:executeText
                color:"white"
                text:"Execute"
                anchors.centerIn: parent
            }
        }
        ColumnLayout{
            visible:mapHud.deviateState !== Enums.NoMission
            anchors{
                top:statusText.bottom
            }

            Text{
                Layout.margins: 2
                text:"Deviate Waypoint"
                font{
                    pixelSize: 14
                }
                color: "white"
            }

            Rectangle{
                color: "white"
                height:deviateWaypointInput.height+5
                width:50
                radius:10
                opacity:0.8
                Layout.margins: 4
                TextInput{
                    id:deviateWaypointInput
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    text: missionManager.standard.getCurrentWaypoint() +1
                    width:parent.width
                    onTextChanged: {
                        if(mapHud.deviateState===Enums.NoMission)return
                        if(Number(text)<Number(rejoinWaypointInput.text)){
                            mapOverlay.moveMiniMissionStart(Number(text)-1)
                        }
                    }
                    font{
                        pixelSize: 14
                    }
                }
            }


            Text{
                text:"Rejoin Waypoint"
                Layout.margins: 2
                font{
                    pixelSize: 14
                }
                color: "white"
            }
            Rectangle{
                color: "white"
                height:rejoinWaypointInput.height+5
                width:50
                radius:10
                opacity:0.8
                Layout.margins: 4
                TextInput{
                    id:rejoinWaypointInput
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    text: missionManager.standard.get(missionManager.standard.seqToIndex(missionManager.standard.getCurrentWaypoint())+1).seq +1
                    width:parent.width
                    onTextChanged: {
                        if(mapHud.deviateState===Enums.NoMission)return
                        if(Number(text)<missionManager.standard.actualWaypointCount()){
                            mapOverlay.moveMiniMissionEnd(Number(text)-1)
                        }
                    }
                    font{
                        pixelSize: 14
                    }
                }

            }
        }
    }
    Rectangle{
        anchors.fill: parent
        color:Qt.rgba(0,0,0.5,0.1)
        visible:mapHud.deviateState === Enums.Adding
        Text{
            anchors{
                horizontalCenter: parent.horizontalCenter
                top: parent.top
                topMargin: parent.height/10
            }
            text:"Adding..."
            font.pixelSize: 20
        }
    }
}
