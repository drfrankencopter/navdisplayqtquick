import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts  1.15
Rectangle {
    width: telemetryGrid.width + 25
    height: telemetryGrid.height + 25
    anchors.right: parent.right
    anchors.rightMargin: 10
    color: Qt.rgba(0,0,0,0.5)

    property real groundSpeed: 0.0
    property real altitude
    property int timeToNextWaypoint: missionHelper.timeToNextWaypoint()
    property int timeLeftForMission: missionHelper.timeLeftForMission()
    property int mxNumber: missionManager.standard.getMissionNumber()
    property int engageNumber: vehicleManager.engageNumber()
    property bool engageStatus: vehicleManager.engageStatus()

    property var timeToNextWaypointText: formatSecondsIntoHoursMinSec(timeToNextWaypoint)
    property var timeLeftForMissionText: formatSecondsIntoHoursMinSec(timeLeftForMission)

    function formatSecondsIntoHoursMinSec(time_in_sec) {
        let hours = Math.floor(time_in_sec / 3600)
        var remainder = time_in_sec % 3600
        let minutes = Math.floor(remainder / 60)
        remainder = remainder % 60
        let seconds = remainder

        // Numbers formatting
        let hoursFormatted      = hours > 9     ? hours : "0" + hours
        let minutesFormatted    = minutes > 9   ? minutes : "0" + minutes
        let secondsFormatted    = seconds > 9   ? seconds : "0" + seconds

        return hoursFormatted + ":" + minutesFormatted + ":" + secondsFormatted
    }

    function onTimeToNextWaypointChanged() {
        timeToNextWaypointText = formatSecondsIntoHoursMinSec(timeToNextWaypoint)
    }

    function onTimeLeftForMissionChanged() {
        timeLeftForMissionText = formatSecondsIntoHoursMinSec(timeLeftForMission)
    }

    Connections {
        target: missionManager
        function onNewMissionAvailable() {
            mxNumber = missionManager.standard.getMissionNumber()
        }
    }

    Connections {
        target: vehicleManager
        function onEngageNumberChanged(num) {
            engageNumber = num;
        }

        function onEngageStatusChanged(isEngaged) {
            engageStatus = isEngaged;
        }
    }

    Connections {
        target: timer
        function onFullSecond() {
            groundSpeed = vehicleManager.groundSpeed().toFixed(1)
            timeToNextWaypoint = missionHelper.timeToNextWaypoint()
            timeLeftForMission = missionHelper.timeLeftForMission()
        }
    }

    GridLayout {
        id: telemetryGrid
        columns: 2
        anchors.centerIn: parent

        Label {
            text: "Total Time Left:"
            color: "white"
            Layout.fillWidth: true
        }

        Label {
            text: timeLeftForMissionText
            color: "white"
            Layout.fillWidth: true
        }

        Label {
            text: "Next Waypoint In:"
            color: "white"
        }

        Label {
            text: timeToNextWaypointText
            color: "white"
            Layout.fillWidth: true
        }

        Label {
            text: "Ground speed:"
            color: "white"
            Layout.fillWidth: true
        }

        Label {
            text: groundSpeed
            color: "white"
            Layout.fillWidth: true
        }

        Label {
            text: "Mx"
            color: "white"
            Layout.fillWidth: true
        }

        Label {
            text: mxNumber
            color: "white"
            Layout.fillWidth: true
        }

        Label {
            text: engageStatus === true ? "Engage number" : "Last engage"
            color: engageStatus === true ? Qt.rgba(0, 1, 0, 1) : "white"
            Layout.fillWidth: true
        }

        Label {
            text: engageNumber
            color: engageStatus === true ? Qt.rgba(0, 1, 0, 1) : "white"
            Layout.fillWidth: true
        }
    }

}
