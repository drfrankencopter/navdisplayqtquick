import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtPositioning 5.6

Item {//this whole system should be redone
    id:root
    anchors.fill:parent
    property var waypointNumber: -1
    property var latValue:0.00
    property var lonValue:0.00
    property var altValue:0.00
    property var radiusValue: 20
    property var speedValue: 1
    Rectangle{
        anchors{
            right:parent.right
            verticalCenter: parent.verticalCenter
        }
        color:Qt.rgba(0.1,0.1,0.1,0.8)
        opacity:0.8
        height:parent.height/3
        width:120
        radius:4
        visible: waypointNumber!==-1
        Text {
            id: titleText
            anchors{
                horizontalCenter: parent.horizontalCenter
                top:parent.top
                topMargin: 5
            }
            color:"white"
            text: "Mini Waypoint "+waypointNumber
            font{
                pixelSize: 13
                bold:true
            }
        }
        ColumnLayout{
            id:layout
            anchors{
                top: titleText.bottom
            }
            height:children.length*15
            width:parent.width
            Text{
                id:latText
                text:"Latitude:"
                Layout.leftMargin: 5
                font{
                    pixelSize: 14
                }
                color:"white"
            }

            Rectangle{
                height:latInput.height+5
                Layout.fillWidth: true
                color: "white"
                radius:10
                opacity:0.8
                Layout.margins: 4
                TextInput{
                    id:latInput
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    text: root.latValue
                    width:parent.width
                    onTextChanged: {
                        if(root.waypointNumber!==-1){
                            mapOverlay.miniMissionWaypoints.get(waypointNumber-1).latitude = Number(text)
                            mapOverlay.syncMiniMissionLine()
                        }
                    }
                    font{
                        pixelSize: 12
                    }
                }
            }
            Text{
                id:lonText
                text:"Longitude:"
                font{
                    pixelSize: 14
                }
                Layout.leftMargin: 5
                color: "white"
            }
            Rectangle{
                color: "white"
                height:lonInput.height+5
                Layout.fillWidth: true
                radius:10
                opacity:0.8
                Layout.margins: 4
                TextInput{
                    id:lonInput
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    text: root.lonValue
                    width:parent.width
                    onTextChanged: {
                        if(root.waypointNumber!==-1){
                            mapOverlay.miniMissionWaypoints.get(waypointNumber-1).longitude = Number(text)
                            mapOverlay.syncMiniMissionLine()
                        }
                    }
                    font{
                        pixelSize: 12
                    }
                }
            }
            Text{
                id:altText
                text:"Altitude:"
                font{
                    pixelSize: 14
                }
                Layout.leftMargin: 5
                color: "white"
            }
            Rectangle{
                color: "white"
                height:altInput.height+5
                Layout.fillWidth: true
                radius:10
                opacity:0.8
                Layout.margins: 4
                TextInput{
                    id:altInput
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    text: root.altValue
                    width:parent.width
                    onTextChanged: {
                        if(root.waypointNumber!==-1)
                        mapOverlay.miniMissionWaypoints.get(waypointNumber-1).altitude = Number(text)
                    }
                    font{
                        pixelSize: 12
                    }
                }
            }
            Text{
                id:radiusText
                text:"Radius:"
                font{
                    pixelSize: 14
                }
                Layout.leftMargin: 5
                color: "white"
            }
            Rectangle{
                color: "white"
                height:radiusInput.height+5
                Layout.fillWidth: true
                radius:10
                opacity:0.8
                Layout.margins: 4
                TextInput{
                    id:radiusInput
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    text: root.radiusValue
                    width:parent.width
                    onTextChanged: {
                        if(root.waypointNumber!==-1)
                        mapOverlay.miniMissionWaypoints.get(waypointNumber-1).acceptanceRadius = Number(text)
                    }
                    font{
                        pixelSize: 12
                    }
                }
            }
            Text{
                id:speedText
                text:"Speed:"
                font{
                    pixelSize: 14
                }
                Layout.leftMargin: 5
                color: "white"
            }
            Rectangle{
                color: "white"
                height:speedInput.height+5
                Layout.fillWidth: true
                radius:10
                opacity:0.8
                Layout.margins: 4
                TextInput{
                    id:speedInput
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    text: root.speedValue
                    width:parent.width
                    onTextChanged: {
                        if(root.waypointNumber!==-1)
                        mapOverlay.miniMissionWaypoints.get(waypointNumber-1).speed = Number(text)
                    }
                    font{
                        pixelSize: 12
                    }
                }
            }

        }
        Button{
            width:80
            height:30
            anchors{
                top: parent.bottom
                horizontalCenter: parent.horizontalCenter
            }
            text:"Remove"
            onClicked: {
                mapOverlay.removeMiniMissionItem(waypointNumber-1)
            }
        }
    }
}
