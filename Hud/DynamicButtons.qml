import QtQuick 2.2
import QtQml.Models 2.1
import Qt.labs.platform 1.1
import QtQuick.Controls 2.2
import QtLocation 5.15
import QtGraphicalEffects 1.15

Item {
    function addButton(name,img,func){
        listModel.buttonActions[name]= func;
        listModel.append({"name":name,"img":img})
    }
    AbstractButton{
        id:showButton
        anchors{
            top:parent.top
            left:parent.left
        }
        checkable: true
        height:30
        width:30
        onCheckedChanged: {
            if(checked){
                openButtonsAnim.start()
                showNamesButton.visible=true
                showButtonImg.rotation=180
            }else{
                closeButtonsAnim.start()
                showNamesButton.visible=false
                showButtonImg.rotation=0
            }
        }
        Image {
            id: showButtonImg
            anchors.fill: parent
            opacity:0.7
            source: "qrc:/imgs/imgs/down-arrow.png"

            ColorOverlay {
                source: parent
                anchors.fill: parent
                color: paletteHolder.hudIcons
            }
        }

        MouseArea {
            acceptedButtons: Qt.RightButton
            anchors.fill: parent
            hoverEnabled: true
            onEntered: {
                showButtonImg.opacity = 1
                cursorShape = Qt.PointingHandCursor
            }

            onExited: {
                showButtonImg.opacity = 0.7
                cursorShape = Qt.ArrowCursor
            }
        }
    }

    AbstractButton{
        id:showNamesButton
        anchors{
            verticalCenter: showButton.verticalCenter
            left:showButton.right
        }
        checkable: true
        height:20
        width:20
        visible:false
        onCheckedChanged: {
            if(checked){
                buttons.showButtonText=false
                showNamesButtonImg.rotation=180
            }else{
                buttons.showButtonText=true
                showNamesButtonImg.rotation=0
            }
        }
        Image {
            id: showNamesButtonImg
            anchors.fill: parent
            opacity:0.7
            source: "qrc:/imgs/imgs/right-arrow.png"
        }
        MouseArea {
            acceptedButtons: Qt.RightButton
            anchors.fill: parent
            hoverEnabled: true
            onEntered: {
                showNamesButtonImg.opacity = 1
                cursorShape = Qt.PointingHandCursor
            }

            onExited: {
                showNamesButtonImg.opacity = 0.7
                cursorShape = Qt.ArrowCursor
            }
        }
    }


    NumberAnimation {
        id:openButtonsAnim
        target: buttons
        property: "height"
        from:0
        to:buttons.buttonsHeight
        duration: 300
        easing.type: Easing.OutQuart
        onStarted: {
            buttons.visible=true
        }
    }
    NumberAnimation {
        id:closeButtonsAnim
        target: buttons
        property: "height"
        from:buttons.buttonsHeight
        to:0
        duration: 300
        easing.type: Easing.OutQuart
        onStopped: {
            buttons.visible = false
        }
    }

    Item{
        id:buttons
        anchors{
            top:showButton.bottom
            right:parent.right
            left:parent.left
        }
        visible:false
        clip:true
        height:buttonsHeight
        property real buttonsHeight: listView.count*(buttonHeight+5)
        property real buttonHeight: 45
        property bool showButtonText: true

        ListModel{
            id:listModel
            property var buttonActions:({})
        }

        ListView{
            id: listView
            model:listModel
            anchors.fill:parent
            orientation:ListView.Vertical
            spacing:2
            interactive: false
            delegate:Item{
                height:buttons.buttonHeight
                width:parent.width
                Rectangle {
                    id:buttonIcon
                    anchors.verticalCenter: model.verticalCenter
                    width:buttons.buttonHeight
                    height:buttons.buttonHeight
                    border.width: 2
                    border.color: "black"
                    color: paletteHolder.hudColor

                    radius:10
                    Image{
                        source:model.img
                        anchors.margins: 5
                        anchors.fill: parent
                        layer.enabled: true
                        layer.effect: ColorOverlay {
                            color: paletteHolder.hudTextColor
                        }
                    }

                    MouseArea{
                        anchors.fill: parent
                        hoverEnabled: true
                        onClicked:{
                            listModel.buttonActions[model.name]()
                        }

                        onEntered: {
                            dynamicButtonHoverEnter.start()
                            cursorShape = Qt.PointingHandCursor
                        }

                        onExited: {
                            dynamicButtonHoverExit.start()
                            cursorShape = Qt.ArrowCursor
                        }

                    }

                    ColorAnimation {
                        id: dynamicButtonHoverEnter
                        target: buttonIcon
                        property: "color"
                        from: buttonIcon.color
                        to: paletteHolder.hudHoverColor
                        duration: 200
                        easing.type: Easing.InOutQuad
                    }

                    ColorAnimation {
                        id: dynamicButtonHoverExit
                        target: buttonIcon
                        property: "color"
                        from: buttonIcon.color
                        to: paletteHolder.hudColor
                        duration: 200
                        easing.type: Easing.InOutQuad
                    }

                }
                Label{
                    text:model.name
                    visible: buttons.showButtonText
                    opacity: 1
                    anchors{
                        verticalCenter: buttonIcon.verticalCenter
                        left: buttonIcon.right
                        leftMargin: 10
                    }
                    font{
                        pointSize: 8
                    }
                    color:paletteHolder.hudColor
                }
            }
        }




    }
}
