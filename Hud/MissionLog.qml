import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15

import NavEnums 1.0

Rectangle {
    id: _root
    width: parent.width
    height: parent.height / 2
    color: Qt.rgba(60/255, 60/255, 60/255, 1)
    anchors.bottom: parent.bottom

    Rectangle {
        id: toggleBarLog
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width
        height: missionLogText.height
        color: paletteHolder.hudHoverColor

        Text {
            id: missionLogText
            text: "⌄ Mission Log ⌄"
            font.pointSize: 13
            color: paletteHolder.hudTextColor
            anchors.horizontalCenter: parent.horizontalCenter
        }

        MouseArea {
            hoverEnabled: true
            anchors.fill: parent

            onEntered: {
                cursorShape = Qt.PointingHandCursor
            }

            onExited: {
                cursorShape = Qt.ArrowCursor
            }

            onClicked: {
                _root.visible = false
            }
        }
    }

    Item {
        id: _content
        anchors.top: toggleBarLog.bottom
        width: parent.width
        height: parent.height - toggleBarLog.height

        property int numColumns: columnTitles.count

        ListModel {
            id: columnTitles
            ListElement { col_title: "Waypoint";                                            }
            ListElement { col_title: "Info";                                                }
            ListElement { col_title: "Distance";    staggered_: true;                       }
            ListElement { col_title: "ETE";         staggered_: true;                       }
            ListElement { col_title: "Climb";  staggered_: true;                       }
            ListElement { col_title: "ETA";                                 refresh_: true; }
            ListElement { col_title: "Speed";                                               }
            ListElement { col_title: "Altitude";                                            }
        }

        GridLayout {
            id: titleRow
            anchors.horizontalCenter: parent.horizontalCenter
            height: 25
            width: parent.width
            columns: columnTitles.count
            columnSpacing: -1

            Repeater {
                model: columnTitles
                Rectangle {
                    property string _title: col_title

                    color: paletteHolder.hudHoverColor
                    border.color: paletteHolder.hudTextColor
                    border.width: 2

                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    Text {
                        text: parent._title
                        font.pointSize: 13
                        color: paletteHolder.hudTextColor
                        anchors.centerIn: parent
                    }
                }
            }
        }

        // This MouseArea is required so the flickable doesn't propagate wheel events to the items behind it.
        MouseArea {
            id: mapMouseArea
            anchors.fill: parent
            enabled: _root.visible
            preventStealing:true
            hoverEnabled:   true
            onWheel:        { wheel.accepted = true; }
            onPressed:      { mouse.accepted = true; }
            onReleased:     { mouse.accepted = true; }
        }

        Flickable {
            function calcScrollPosition() {
                // We want to scroll to the current mission item
                var current     = missionManager.standard.getCurrentWaypoint()
                var numEntries  = missionManager.standard.actualWaypointCount

                // -3 rect height so we can see a bit of the past legs
                var pos = (current * flickableContent.rectHeight) - (3 * flickableContent.rectHeight)


                return Math.max(Math.min(flickableContent.height - logFlickable.height, Math.max(0, pos)), 0)
            }

            id: logFlickable
            height: parent.height - titleRow.height
            width: parent.width
            contentWidth: width
            contentHeight: flickableContent.height
            anchors.top: titleRow.bottom
            clip: true
            Component.onCompleted: {
                logFlickable.contentY = logFlickable.calcScrollPosition()
            }

            Connections {
                target: mapOverlay.missionWaypoints
                function onCountChanged() {
                    logFlickable.contentY = logFlickable.calcScrollPosition()
                }
            }

            // Recenter the scroll view when visibility toggled on
            Connections {
                target: _root
                function onVisibleChanged() {
                    if (_root.visible === true) {
                        logFlickable.contentY = logFlickable.calcScrollPosition()
                    }
                }
            }

            ListView {
                id: flickableContent
                anchors.fill: parent
                width: parent.width
                contentHeight: height
                height: (rectHeight * missionManager.standard.actualWaypointCount + rectHeight)
                orientation: ListView.Horizontal
                interactive: false

                onHeightChanged: {
                    console.log("Flickable content height: " + height)
                    console.log("   rectHeight: " + rectHeight)
                    console.log("   waypointsCount: " + mapOverlay.missionWaypoints.count)
                    console.log("   actualWaypointCount: " + missionManager.standard.actualWaypointCount)
                    console.log("   should be: " + (rectHeight * mapOverlay.missionWaypoints.count + rectHeight))
                }

                property int rectHeight: 50

                model: columnTitles
                delegate: MissionLogColumn {
                    id: waypointsContent
                    contentHeight: flickableContent.height
                    height: flickableContent.height
                    width: flickableContent.width / _content.numColumns
                    column: col_title
                    staggered: staggered_
                    refresh: refresh_
                }
            }
        }

        DropShadow {
            height: titleRow.height
            width: titleRow.width
            verticalOffset: 5
            radius: 12
            samples: 25
            color: "black"
            source: titleRow
        }
    }
}

