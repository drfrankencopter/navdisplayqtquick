import QtQuick 2.0
import QtQuick.Layouts 1.3

Item {
    property string txt: ""
    Layout.fillWidth: true;
    Text{
        id:logtext;
        anchors.fill: parent
        horizontalAlignment: Text.AlignHCenter;
        text: parent.txt;
    }
    NumberAnimation {
        target: logtext;
        property: "opacity";
        from:1;
        to:0;
        duration: 5000;
        easing.type: Easing.InQuart;
        running:true;
        onStopped: {
            logtext.destroy();
        }
    }
}
