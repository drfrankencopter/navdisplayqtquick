import QtQuick 2.0
import QtGraphicalEffects 1.15

Item{
    id: menuIcon
    property alias image: menuButtonImage
    property alias mouseArea: menuButtonMouseArea
    Image{
        id:menuButtonImage
        anchors.fill: parent

        ColorOverlay {
            anchors.fill: parent
            source: parent
            color: paletteHolder.hudIcons
        }
    }

    MouseArea{
        id:menuButtonMouseArea
        hoverEnabled: true
        anchors.fill:parent
        onEntered: {
            cursorShape = Qt.PointingHandCursor
            menuIconOpacityIn.start()

        }
        onExited: {
            cursorShape = Qt.ArrowCursor
            menuIconOpacityOut.start()
        }

        NumberAnimation {
            id: menuIconOpacityIn
            target: menuIcon
            property: "opacity"
            from:menuIcon.opacity
            to:1
            duration: 200
            easing.type: Easing.InOutQuad
        }

        NumberAnimation {
            id: menuIconOpacityOut
            target: menuIcon
            property: "opacity"
            from:menuIcon.opacity
            to:0.5
            duration: 200
            easing.type: Easing.InOutQuad
        }
    }
}
