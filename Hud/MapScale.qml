import QtQuick 2.0
import QtQuick.Controls 2.0

Item {
    id: scale
    z: parent.z + 3
    visible: scaleText.text != "0 m"
    anchors.margins: 20
    height: scaleText.height * 2
    width: scaleImage.width

    property alias image: scaleImage
    property alias leftImage: scaleImageLeft
    property alias rightImage: scaleImageRight
    property alias text: scaleText

    Image {
        id: scaleImageLeft
        source: "qrc:/imgs/imgs/scale_end.png"
        anchors.bottom: parent.bottom
        anchors.right: scaleImage.left
    }
    Image {
        id: scaleImage
        source: "qrc:/imgs/imgs/scale.png"
        anchors.bottom: parent.bottom
        anchors.right: scaleImageRight.left
    }
    Image {
        id: scaleImageRight
        source: "qrc:/imgs/imgs/scale_end.png"
        anchors.bottom: parent.bottom
        anchors.right: parent.right
    }
    Label {
        id: scaleText
        color: "#004EAE"
        anchors.centerIn: parent
        text: "0 m"
    }
    Component.onCompleted: {
        mapHud.calculateScale();
    }
}
