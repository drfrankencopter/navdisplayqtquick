import QtQuick 2.0

Item{
    width: 65
    height: 30
    Rectangle{
        id: zoomOut
        anchors{
            right: parent.right
        }
        height: parent.height
        width: parent.height
        color: paletteHolder.hudColor
        border.width: 1
        border.color: Qt.rgba(37/255, 37/255, 37/255, 1)
        radius: width*0.5

        Text {
            text: "-"
            anchors.centerIn: parent
            color: paletteHolder.hudTextColor

            font.pointSize: 15
        }

        MouseArea{
            anchors.fill: parent
            hoverEnabled: true

            onEntered: {
                zoomOutButtonOpacityEntered.start()
                cursorShape = Qt.PointingHandCursor
            }

            onExited: {
                zoomOutButtonOpacityExit.start()
                cursorShape = Qt.ArrowCursor
            }

            onClicked:{
                mainMap.zoomLevel -= 0.5
                if (mainMap.controllerActive)
                    mainMap.controllerMove()
            }

            ColorAnimation {
                id: zoomOutButtonOpacityEntered
                target: zoomOut
                property: "color"
                from: zoomOut.color
                to: paletteHolder.hudHoverColor
                duration: 200
                easing.type: Easing.InOutQuad
            }

            ColorAnimation {
                id: zoomOutButtonOpacityExit
                target: zoomOut
                property: "color"
                from: zoomOut.color
                to: paletteHolder.hudColor
                duration: 200
                easing.type: Easing.InOutQuad
            }
        }
    }
    Rectangle{
        id: zoomIn
        anchors{
            left: parent.left
        }
        color: paletteHolder.hudColor
        height:parent.height
        width:parent.height
        border.width: 1
        border.color: Qt.rgba(37/255, 37/255, 37/255, 1)
        radius: width*0.5

        Text {
            text: "+"
            anchors.centerIn: parent
            color: paletteHolder.hudTextColor

            font.pointSize: 15
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true

            onEntered: {
                zoomInButtonOpacityEntered.start()
                cursorShape = Qt.PointingHandCursor
            }

            onExited: {
                zoomInButtonOpacityExit.start()
                cursorShape = Qt.ArrowCursor
            }

            onClicked:{
                mainMap.zoomLevel+=0.5
                if(mainMap.controllerActive)
                    mainMap.controllerMove()
            }

            ColorAnimation {
                id: zoomInButtonOpacityEntered
                target: zoomIn
                property: "color"
                from: zoomIn.color
                to: paletteHolder.hudHoverColor
                duration: 200
                easing.type: Easing.InOutQuad
            }

            ColorAnimation {
                id: zoomInButtonOpacityExit
                target: zoomIn
                property: "color"
                from: zoomIn.color
                to: paletteHolder.hudColor
                duration: 200
                easing.type: Easing.InOutQuad
            }
        }
    }
}
