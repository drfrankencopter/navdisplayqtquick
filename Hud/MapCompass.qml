import QtQuick 2.0
import QtGraphicalEffects 1.15
import "../helper.js" as Helper

Item
{
    id:mapCompassRoot
    property real bearingAngle: 0
    property real compassVertical:0
    opacity:0.8
    Item{
        clip:true
        width:parent.width
        height:parent.height/2

        Image
        {
            id:compassImg
            source: "qrc:/imgs/imgs/compass.png"
            property var imageSize: mapCompassRoot.width*1.2 <mapCompassRoot.height ? mapCompassRoot.width*1.2 : mapCompassRoot.height
            anchors.horizontalCenter: parent.horizontalCenter
            y:mapCompassRoot.compassVertical
            sourceSize.width: imageSize
            sourceSize.height: imageSize
            fillMode:Image.Pad
            transform:Rotation {
                origin{
                    x:compassImg.sourceSize.width/2
                    y:compassImg.sourceSize.height/2
                }
                angle:-bearingAngle
            }
        }

    }


    Image{
        id:compassCaret
        source:"qrc:/imgs/imgs/caretup.png"
        anchors{
            horizontalCenter:parent.horizontalCenter
            top:parent.top
            topMargin: parent.height/12
        }
        width:20
        height:20
    }
    Text{
        id:bearingText
        text:Helper.formatBearing(bearingAngle)+"°"
        anchors{
            top:compassCaret.bottom
            horizontalCenter: parent.horizontalCenter
        }
        font{
            bold:true
        }
    }
    Text{
        id:aircraftBearingText
        text:Helper.formatBearing(vehicleManager.heading() )+"°"
        anchors{
            top:bearingText.bottom
            horizontalCenter: parent.horizontalCenter
        }
        visible: mainMap.controllerMode != "Heading"
    }
}

