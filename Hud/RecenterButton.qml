import QtQuick 2.0
import QtGraphicalEffects 1.15

Rectangle {
    id: recenterButton

    Item {
        anchors.fill: recenterButton
        Image {
            id: imgRecenter
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            height: recenterButton.height - 25
            width: recenterButton.width - 25
            source: "qrc:/imgs/imgs/Controller.png"
        }

        ColorOverlay {
            anchors.fill: imgRecenter
            color: paletteHolder.hudTextColor
            source: imgRecenter
        }

        Text {
            text: "Recenter"
            color: paletteHolder.hudTextColor
            anchors.top: imgRecenter.bottom
            anchors.horizontalCenter: imgRecenter.horizontalCenter
        }
    }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true

        onEntered: {
            recenterButtonOpacityEntered.start()
            cursorShape = Qt.PointingHandCursor
        }

        onExited: {
            recenterButtonOpacityExit.start()
            cursorShape = Qt.ArrowCursor
        }

        onClicked: {
            mainMap.controllerPaused = false
            mainMap.controllerMove()
        }

        ColorAnimation {
            id: recenterButtonOpacityEntered
            target: recenterButton
            property: "color"
            from: recenterButton.color
            to: paletteHolder.hudHoverColor
            duration: 200
            easing.type: Easing.InOutQuad
        }

        ColorAnimation {
            id: recenterButtonOpacityExit
            target: recenterButton
            property: "color"
            from: recenterButton.color
            to: paletteHolder.hudColor
            duration: 200
            easing.type: Easing.InOutQuad
        }
    }
}
