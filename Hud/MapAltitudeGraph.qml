import QtQuick 2.0
import QtQuick.Controls 2.5
import QtCharts 2.13

Item {
    id:altitudeGraph

    function moveAircraft(x,y){
        aircraftPosition.remove(0)
        aircraftPosition.append(x,y)
    }

    width:300
    height:200
    property real graphHeight
    property real graphWidth
    property alias chart: chart
    property alias graph: lineSeries
    property alias activeLine:activeLine
    property alias groundLine:groundLine
    property alias missionPoints: missionPoints
    property alias aircraft:aircraftPosition

    Rectangle{
        id:background
        color: Qt.rgba(0.1,0.1,0.1,0.6)
        anchors.fill:parent
        radius:5


        ChartView{
            id:chart
            anchors.fill:parent
            theme: ChartView.ChartThemeDark
            legend.visible: false
            backgroundColor: Qt.rgba(0,0,0,0)
            plotAreaColor: Qt.rgba(0.1,0.1,0.1,0.1)
            margins{
                top:0
                bottom:0
                left:0
                right:0
            }

            LineSeries{
                id:lineSeries
                width:3
                function niceNumbers(){
                    yAxis.applyNiceNumbers()
                }

                axisX: ValueAxis{
                    id:xAxis
                    min:0
                    max:graphWidth
                    tickCount: 4
                }
                axisY: ValueAxis{
                    id:yAxis
                    min:0
                    max:graphHeight
                    tickCount: 3
                }
                color:navSettings.missionLineColor
            }
            LineSeries{
                id:activeLine
                width:3
                axisX:lineSeries.axisX
                axisY:lineSeries.axisY
                color:navSettings.currentMissionLineColor
            }

            LineSeries{
                id:groundLine
                width:3
                axisX:lineSeries.axisX
                axisY:lineSeries.axisY
                color:"green"
            }
            ScatterSeries{
                id:missionPoints
                axisX:lineSeries.axisX
                axisY:lineSeries.axisY
                borderWidth: 0.1
                markerSize: 5
                color:navSettings.missionLineColor
            }
            ScatterSeries{
                id:aircraftPosition
                axisX:lineSeries.axisX
                axisY:lineSeries.axisY
                borderWidth: 0.1
                color:"blue"
            }
        }
    }
}
