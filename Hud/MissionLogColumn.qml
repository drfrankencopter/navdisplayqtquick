import QtQuick 2.0

import NavEnums 1.0

ListView {
    id: _root
    spacing: -1
    interactive: false
    y: staggered ? flickableContent.rectHeight / 2 : 0

    // Properties to set when creating this object
    property string column
    property bool   staggered:          false
    property bool   refresh:            false

    property color  pastBorderColor:    "gray"
    property color  borderColor:        "white"
    property color  pastTextColor:      "gray"
    property color  textColor:          "white"
    property color  currentBackColor:   navSettings.currentMissionLineColor
    property color  oddBackColor:       Qt.rgba(0, 0, 0, 0)
    property color  evenBackColor:      "black"

    function generateTextFunction(column, number) {
        switch (column) {
        case "Info": {
            return number < 10 ? "0" + number : number
        }
        case "Waypoint": {
            return _root.formatType(missionManager.standard.waypointType(number - 1))
        }
        case "Distance": {
            return _root.formatDistance(number - 1)
        }
        case "ETE": {
            return _root.formatTimeEnRoute(number - 1)
        }
        case "Climb": {//changed climb rate to just climb at bryan's request
            if (missionHelper.getNextWaypointOf(number - 1) === -1) {
                return "N/A"
            }

            let climb = missionHelper.climbFromWaypointToNext(number - 1)
            return (climb * units.verticalFactor).toFixed(0) + " " + units.verticalUnits
        }
        case "ETA": {
            return _root.formatTimeArrival(number - 1)
        }
        case "Speed": {
            return (missionHelper.speedAtWaypoint(number - 1) * units.horizontalSpeedFactor).toFixed(0) + " " + units.horizontalSpeedUnits
        }
        case "Altitude": {
            return (missionManager.standard.waypointAlt(number - 1) * units.verticalFactor).toFixed(0) + " " + units.verticalUnits
        }
        default: {
            return "Undefined"
        }
        }
    }

    function formatType(type) {
        switch(type) {
        case Enums.Waypoint:
            return "WAYPOINT";
        case Enums.Land:
            return "LAND";
        case Enums.Takeoff:
            return "T/O";
        default:
            return "UNKNOWN"
        }
    }

    function formatDistance(index) {
        var next = missionHelper.getNextWaypointOf(index)

        if (next < 0) {
            return "N/A"
        }

        var dist = missionHelper.distanceBetweenWaypoints(index, next)

        var formattedDist = ""

        if ((dist / 1000000) >= 1) {
            formattedDist += (dist / 1000000).toFixed(0) + " "
        }

        if (((dist % 1000000) / 1000) >= 1) {
            formattedDist += ((dist % 1000000) / 1000).toFixed(0) + " "
        }

        formattedDist += ((dist % 1000) * units.horizontalFactor).toFixed(0) + " " + units.horizontalUnits

        return formattedDist
    }

    function formatTimeEnRoute(index) {
        var next = missionHelper.getNextWaypointOf(index)

        if (next < 0) {
            return "N/A"
        }

        var time_in_sec = missionHelper.timeBetweenWaypoints(index, next)
        let hours = Math.floor(time_in_sec / 3600)
        var remainder = time_in_sec % 3600
        let minutes = Math.floor(remainder / 60)
        remainder = remainder % 60
        let seconds = remainder

        // Numbers formatting
        let hoursFormatted      = hours > 9     ? hours : "0" + hours
        let minutesFormatted    = minutes > 9   ? minutes : "0" + minutes
        let secondsFormatted    = seconds > 9   ? seconds : "0" + seconds

        return hoursFormatted + ":" + minutesFormatted + ":" + secondsFormatted
    }

    function formatTimeArrival(index) {
        var currentTime = new Date()
        var time_in_sec = missionHelper.timeFromCurrentToWaypoint(index)
        var newDateObj = new Date(currentTime.getTime() + (time_in_sec / 60)*60000)

        return newDateObj.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })
    }

    function determineBackColor(isStaggered, isCurrent) {
        if ( typeof determineBackColor.counter == 'undefined' ) {
            determineBackColor.counter = 0;
        }

        if (isCurrent) {
            ++determineBackColor.counter;
            return _root.currentBackColor
        }
        else if (isStaggered) {
            return _root.oddBackColor
        }

        if (++determineBackColor.counter % 2 === 0) {
            return _root.evenBackColor
        }
        else {
            return _root.oddBackColor
        }
    }

    model: mapOverlay.missionWaypoints
    delegate: Rectangle {
        height: flickableContent.rectHeight
        width: waypointsContent.width
        color: _root.determineBackColor(_root.staggered, isCurrent)
        border.width: 1
        border.color: isPast ? _root.pastBorderColor : _root.borderColor
        visible: true//textFormatted !== "N/A"

        property int    currentItem:    missionManager.standard.getCurrentWaypoint()
        property int    targetItem:     _root.staggered ? missionHelper.getPreviousWaypointOf(currentItem) : currentItem
        property bool   isPast:         (number - 1) < targetItem
        property bool   isCurrent:      (number - 1) === targetItem

        property string textFormatted:  _root.generateTextFunction(_root.column, number)

        Text {
            text: textFormatted
            anchors.centerIn: parent
            color: isPast ? _root.pastTextColor : _root.textColor
            font.pointSize: 15
        }

        Connections {
            target: timer
            function onFullMinute() {
                if (_root.refresh) {
                    textFormatted = _root.generateTextFunction(_root.column, number)
                }
            }
        }

        Connections {
            target: missionManager
            function onNewMissionCurrent(current) {
                currentItem = current
            }
        }
    }
}
