#ifndef SOCKETS_H
#define SOCKETS_H

#include "mavNrc/globals.h"

#ifdef _WIN32
#include <winsock2.h>
#include <windows.h>
#define CLOSESOCKET(sockfd) (closesocket(sockfd))
#else
#define CLOSESOCKET(sockfd) (close(sockfd))
#endif

#endif // SOCKETS_H
