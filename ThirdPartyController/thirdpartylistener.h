#ifndef THIRDPARTYLISTENER_H
#define THIRDPARTYLISTENER_H
#include <QUdpSocket>
#include <QTimer>
#include <QDebug>
#include <QNetworkDatagram>
#include <QHostAddress>
#include "sockets.h"

// The ThirdPartyListener is not currently used and not fully implemented. It was implemented here for exploratory purposes
// because we might have wanted the NavDisplay to communicate with the MavNRC (supervisor or mission manager) for things like
// mini-missions, or anything else similar where the pilot can give input on the screen to the autonomous system which is controlled
// in MavNRC.

class ThirdPartyListener : public QObject
{
    Q_OBJECT

public:
    ThirdPartyListener();
    ~ThirdPartyListener();

signals:
    void newLandingPoint(double recent_lat, double recent_lon, float recent_heading);

private slots:
    void _readPendingDatagrams();
    void _sendHeartbeat();

private:

    void _handleMessage(const QNetworkDatagram& datagram);

    void _handleLandingPointMessage(const mavlink_message_t& msg);

    bool _receivedInfoRemote;
    QHostAddress _senderAddr;
    int _senderPort;

    // Stores the most recent landing point receives. We only want to draw the last landing point.
    double _lpLat;
    double _lpLon;
    float  _lpBearing;

    QUdpSocket* _sock;
    QTimer* _timer;
};

#endif // THIRDPARTYLISTENER_H
