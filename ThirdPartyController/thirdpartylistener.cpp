#include <standard/mavlink.h>
#include "thirdpartylistener.h"
#ifdef _WIN32
#include <winsock2.h>
#include <windows.h>
#else
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#endif
#include <stdio.h>

ThirdPartyListener::ThirdPartyListener()
    : _receivedInfoRemote(false)
{
    _sock = new QUdpSocket(this);
    _sock->bind(QHostAddress::AnyIPv4, 12346);

    _timer = new QTimer(this);

    connect(_timer, &QTimer::timeout, this, &ThirdPartyListener::_sendHeartbeat);
    connect(_sock, &QUdpSocket::readyRead, this, &ThirdPartyListener::_readPendingDatagrams);

    _timer->start(1000);
}

ThirdPartyListener::~ThirdPartyListener()
{
    delete _timer;
    delete _sock;
}

void ThirdPartyListener::_readPendingDatagrams()
{
    while (_sock->hasPendingDatagrams())
    {
        QNetworkDatagram datagram = _sock->receiveDatagram();

        if (datagram.isValid())
        {
            //qDebug() << "[ RECV ] Received a datagram from the remote.";
            if (!_receivedInfoRemote)
            {
                _senderAddr = datagram.senderAddress();
                _senderPort = datagram.senderPort();
                _receivedInfoRemote = true;
            }

            _handleMessage(datagram);

            // Handle the message here
//            const char* data = "HI, NAVDISPLAY IS RESPONDING TO YOUR MESSAGE\n";
//            qint64 datasize = strlen(data);
//            _sock->writeDatagram(data, datasize, _senderAddr, _senderPort);
        }
        else
        {
            qDebug() << "[ ERROR (RECV) ] Error receiving the datagram";
        }
    }
}

void ThirdPartyListener::_sendHeartbeat()
{
    // Currently only send information if the remote sent information before
    // Like we wait for a first message from the remote so we can know it's
    // address.

    if (_receivedInfoRemote)
    {
        //qDebug() << "[ SEND ] Should send a heartbeat";
        const char* data = "HI, I'M THE NAVDISPLAY\n";
        qint64 datasize = strlen(data);

        _sock->writeDatagram(data, datasize, _senderAddr, _senderPort);
    }
}

void ThirdPartyListener::_handleMessage(const QNetworkDatagram& datagram)
{

    // We parse the mavlink message here
    // Since this is kind of a workaround to get the landing point, we are only listening to landing point messages

    QByteArray data = datagram.data();
    int nBytes = static_cast<int>(data.size());

    mavlink_message_t msg;
    mavlink_status_t status;

    for (int i = 0; i < nBytes; ++i)
    {
        // Only returns when the full msg is parsed
        if (mavlink_parse_char(MAVLINK_COMM_0, data[i], &msg, &status))
        {
            switch (msg.msgid)
            {
            case MAVLINK_MSG_ID_HEARTBEAT:
                //qDebug() << "Received a heartbeat from MavNRC";
                //handleHeartbeatMessage(listenedMessage);
                break;
            case MAVLINK_MSG_ID_LANDING_TARGET:
                qDebug() << "Received a landing point from MavNRC";
                _handleLandingPointMessage(msg);
                break;
            default:
                LOG("\nReceived an un-handled MavLink message. Details below:\n");
                LOG("Message informations:\n");
                LOG("sysid :     %u\n", msg.sysid);
                LOG("compid :    %u\n", msg.compid);
                LOG("compatf:    %u\n", msg.compat_flags);
                LOG("incompf:    %u\n", msg.incompat_flags);
                LOG("checksum:   %u\n", msg.checksum);
                LOG("len:        %u\n", msg.len);
                LOG("magic:      %u\n", msg.magic);
                LOG("msgid:      %u\n", msg.msgid);
                LOG("seq:        %u\n", msg.seq);
                break;
            }   // end of switch

        }
    }
}

void ThirdPartyListener::_handleLandingPointMessage(const mavlink_message_t& msg)
{
    mavlink_landing_target_t lp;
    mavlink_msg_landing_target_decode(&msg, &lp);

    _lpLat = lp.x;
    _lpLon = lp.y;
    _lpBearing = lp.z;

    emit newLandingPoint(_lpLat, _lpLon, _lpBearing);
}
