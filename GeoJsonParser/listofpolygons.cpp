#include "listofpolygons.h"

int ListOfPolygons::count() const {
    return static_cast<int>(_polygons.size());
}

QVariant ListOfPolygons::get(int index)
{
    QGeoPolygon poly = _polygons.at(index);

    if (!poly.isValid())
    {
        qDebug() << "Polygon is not valid";
    }

    return QVariant::fromValue(poly);
}

void ListOfPolygons::add(const QGeoPolygon& polygon)
{
    _polygons.push_back(polygon);
    emit countChanged(count());
}

QVariant ListOfPolygons::data(const QModelIndex &index, int role) const
{
    switch (role)
    {
    case NameRole:
        return QVariant();
    case GetRole:
        return QVariant::fromValue(_polygons.at(index.row()));
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> ListOfPolygons::roleNames() const
{
    static QHash<int, QByteArray> mapping
    {
        { NameRole, "name" },
        { GetRole,  "get" }
    };

    return mapping;
}
