#ifndef LISTOFPOLYGONS_H
#define LISTOFPOLYGONS_H

#include <QObject>
#include <QQmlEngine>
#include <vector>
#include <QGeoPolygon>
#include <QAbstractListModel>

// The list of geojson polygons is implemented as a separate class in order to implement the QAbstractListModel
// which allows to expose an actual list to QML. You can't really expose a std::vector or a basic array directly
// to QML so you have to use this type of abstraction to access this list in QML.

class ListOfPolygons : public QAbstractListModel
{
    Q_OBJECT
public:
    ListOfPolygons() = default;

    Q_PROPERTY(int count READ count NOTIFY countChanged)

    int count() const;
    Q_INVOKABLE QVariant get(int index);
    void add(const QGeoPolygon& polygon);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override { Q_UNUSED(parent); return static_cast<int>(_polygons.size()); }
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

    static void declareQml() {
        qmlRegisterInterface<ListOfPolygons>("ListOfPolygons", 1);
    }

signals:
    void countChanged(int newCount);

private:
    enum Roles
    {
        NameRole = Qt::UserRole,
        GetRole
    };

    std::vector<QGeoPolygon> _polygons;
};

Q_DECLARE_METATYPE(ListOfPolygons*)

#endif // LISTOFPOLYGONS_H
