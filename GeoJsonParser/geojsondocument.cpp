#include "geojsondocument.h"

GeoJsonDocument::GeoJsonDocument(const QString& name, QObject *parent)
    : QObject{parent}
    , _name(name)
    , _isVisible(true)
{
}

void GeoJsonDocument::addPolygon(const QGeoPolygon& polygon)
{
    _polygons.add(polygon);
}

void GeoJsonDocument::addPoint(const QGeoCircle& circle)
{
    _points.add(circle);
}

void GeoJsonDocument::addLine(const QGeoPath& path)
{
    _lines.add(path);
}

void GeoJsonDocument::setIsVisible(bool visibility)
{
    if (_isVisible != visibility) {
        _isVisible = visibility;
        emit isVisibleChanged(_isVisible);
    }
}
