#ifndef GEOJSONDOCUMENT_H
#define GEOJSONDOCUMENT_H

#include <QObject>
#include <QQmlEngine>
#include <QGeoPolygon>
#include <QGeoCircle>
#include <QGeoPath>

#include "listoflines.h"
#include "listofpoints.h"
#include "listofpolygons.h"

// A GeoJsonDocument is a parsed version of an actual geojson document. A GeoJson document
// can hold polygons, lines or even points on a map and everything can be stored and retreived
// accordingly in from this object.

class GeoJsonDocument : public QObject
{
    Q_OBJECT
public:
    GeoJsonDocument(const QString& name, QObject* parent = nullptr);
    ~GeoJsonDocument() = default;

    Q_PROPERTY(ListOfPolygons*  polygons    READ polygons   CONSTANT)
    Q_PROPERTY(ListOfLines*     lines       READ lines      CONSTANT)
    Q_PROPERTY(ListOfPoints*    points      READ points     CONSTANT)
    Q_PROPERTY(bool             isVisible   READ isVisible  WRITE   setIsVisible NOTIFY isVisibleChanged)
    Q_PROPERTY(QString          name        READ name       CONSTANT)

    void addPolygon(const QGeoPolygon& polygon);
    void addPoint(const QGeoCircle& circle);
    void addLine(const QGeoPath& path);

    static void declareQml()
    {
        qmlRegisterInterface<GeoJsonDocument>("GeoJsonDocument", 1);
        ListOfLines::declareQml();
        ListOfPoints::declareQml();
        ListOfPolygons::declareQml();
    }

    const QString &name() const { return _name;         };
    ListOfPolygons* polygons()  { return &_polygons;    };
    ListOfLines* lines()        { return &_lines;       };
    ListOfPoints* points()      { return &_points;      };
    bool isVisible() const      { return _isVisible;    };

    void setIsVisible(bool visibility);

signals:
    void isVisibleChanged(bool visibility);

private:
    QString         _name;
    bool            _isVisible;
    ListOfPolygons  _polygons;
    ListOfLines     _lines;
    ListOfPoints    _points;
};

Q_DECLARE_METATYPE(GeoJsonDocument*)

#endif // GEOJSONDOCUMENT_H
