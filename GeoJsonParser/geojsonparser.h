#ifndef GEOJSONPARSER_H
#define GEOJSONPARSER_H

#include <QObject>
#include <QQmlEngine>
#include <QAbstractListModel>

#include "geojsondocument.h"
// Each time you parse a new document, it will be added to the list of current documents
class GeoJsonParser : public QAbstractListModel
{
    Q_OBJECT
public:
    GeoJsonParser();

    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)

    enum Roles
    {
        NameRole = Qt::UserRole,
        LayerName,
        Polygons,
        Points,
        Lines,
        Visibility,
        Document
    };

    Q_INVOKABLE void parseGeoJsonDocument(const QString& path, const QString& docName = "");
    Q_INVOKABLE void parseGeoJsonDocument(const QUrl& url, const QString& docName = "");
    Q_INVOKABLE void removeDocument(int index);
    Q_INVOKABLE void removeDocument(const QString &name);
    Q_INVOKABLE void toggleVisibilityForLayers(bool isVisible);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override { Q_UNUSED(parent); return static_cast<int>(_documents.size()); }
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

    static void declareQml() {
        GeoJsonDocument::declareQml();
    }

signals:
    void countChanged(int newCount);

private:
    void _addDocument(QSharedPointer<GeoJsonDocument> doc);


    std::vector<QSharedPointer<GeoJsonDocument>> _documents;
};

#endif // GEOJSONPARSER_H
