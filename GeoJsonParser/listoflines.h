#ifndef LISTOFLINES_H
#define LISTOFLINES_H

#include <QObject>
#include <QQmlEngine>
#include <QAbstractListModel>
#include <QGeoPath>

// The list of geojson lines is implemented as a separate class in order to implement the QAbstractListModel
// which allows to expose an actual list to QML. You can't really expose a std::vector or a basic array directly
// to QML so you have to use this type of abstraction to access this list in QML.

class ListOfLines : public QAbstractListModel
{
    Q_OBJECT
public:
    ListOfLines() = default;

    Q_PROPERTY(int count READ count NOTIFY countChanged)

    int count() const;
    Q_INVOKABLE QVariant get(int index) const;
    void add(const QGeoPath& polygon);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override { Q_UNUSED(parent); return count(); }
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

    static void declareQml()
    {
        qmlRegisterInterface<ListOfLines>("ListOfLines", 1);
    }

signals:
    void countChanged(int newCount);

private:
    enum Roles
    {
        NameRole = Qt::UserRole,
        GetRole
    };

    std::vector<QGeoPath> _lines;
};

#endif // LISTOFLINES_H
