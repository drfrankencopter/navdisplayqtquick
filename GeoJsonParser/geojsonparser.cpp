#include "geojsonparser.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QUrl>
#include <QGeoPolygon>
#include <QGeoCoordinate>

GeoJsonParser::GeoJsonParser()
{

}

void GeoJsonParser::parseGeoJsonDocument(const QString& path, const QString& docName)
{
    QUrl url(path);
    parseGeoJsonDocument(url, docName);
}

void GeoJsonParser::parseGeoJsonDocument(const QUrl& url, const QString& docName) {

    if (!url.isLocalFile())
    {
        qDebug() << "[ ERROR ] GeoJson file is not a local file. " << url.toDisplayString();
        return;
    }

    QString documentName = docName == "" ? url.fileName() : docName;
    QFile file(url.toLocalFile());
    if (!file.exists())
    {
        qDebug() << "[ ERROR ] GeoJson file doesn't exist. " << url.toDisplayString();
    }


    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "[ ERROR ] Can't open the GeoJson file to parse.";
        return;
    }

    QSharedPointer<GeoJsonDocument> finalDoc(new GeoJsonDocument(documentName));

    QByteArray data = file.readAll();
    QJsonDocument doc = QJsonDocument::fromJson(data);
    QJsonArray features = doc.object()["features"].toArray();
    QJsonArray::const_iterator it = features.constBegin();

    while (it != features.constEnd())
    {
        QJsonObject feature     = it->toObject();
        QJsonObject geometry    = feature["geometry"].toObject();
        QString type            = geometry["type"].toString();
        QJsonArray coords       = geometry["coordinates"].toArray();

        if (type == "Polygon")
        {
            qDebug() << "!!! FOUND POLYGON";

            QJsonArray polyCoords = coords.at(0).toArray(); // List of coordinates is nested inside of a list
            QGeoPolygon poly;
            QList<QGeoCoordinate> parsed_coords;

            for (int i = 0; i < polyCoords.size(); ++i)
            {
                QJsonArray two_coords = polyCoords[i].toArray();
                QGeoCoordinate coordinate(two_coords.at(1).toDouble(), two_coords.at(0).toDouble()); // GEOJSON coord order is lon/lat
                parsed_coords.append(coordinate);
            }

            poly.setPath(parsed_coords);

            if (!poly.isValid())
            {
                qDebug() << "Parsed Polygon is not valid";
            }

            finalDoc->addPolygon(poly);
        }
        else if (type == "LineString")
        {
            qDebug() << "!!! FOUND LINESTRING";

            QJsonArray lineCoords = coords;
            QGeoPath line;
            QList<QGeoCoordinate> parsed_coords;
            for (int i = 0; i < lineCoords.count(); ++i)
            {
                QJsonArray two_coords = lineCoords[i].toArray();
                QGeoCoordinate coordinate(two_coords.at(1).toDouble(), two_coords.at(0).toDouble());
                parsed_coords.append(coordinate);
            }

            line.setPath(parsed_coords);

            if (!line.isValid())
            {
                qDebug() << "Line is not valid";
            }

            finalDoc->addLine(line);
        }
        else if (type == "Point")
        {
            qDebug() << "!!! FOUND POINT";
            QGeoCircle point;
            QGeoCoordinate parsed_coord(coords.at(1).toDouble(), coords.at(0).toDouble());
            point.setCenter(parsed_coord);

            qreal radius = 300;
            auto parsed_radius = feature["properties"].toObject()["radius"];
            if (!parsed_radius.isUndefined() && parsed_radius.isDouble())
            {
                radius = parsed_radius.toDouble();
            }

            point.setRadius(radius);

            if (!point.isValid())
            {
                qDebug() << "Point is not valid";
            }

            finalDoc->addPoint(point);
        }
        else
        {
            qDebug() << "[ ERROR ] Parsing : Unsupported type of geojson feature (" << type <<"). Type feature ignored.";
        }

        ++it;
    }

    // Add the built document holding all the geometries to the list of documents
    _addDocument(finalDoc);

}

QVariant GeoJsonParser::data(const QModelIndex &index, int role) const
{

    if (!index.isValid())
        return QVariant();

    auto data = _documents.at(index.row());

    switch (role)
    {
    case NameRole:
    case LayerName:
        return QVariant(data->name());
    case Polygons:
        return QVariant::fromValue(data->polygons());
    case Lines:
        return QVariant::fromValue(data->lines());
    case Points:
        return QVariant::fromValue(data->points());
    case Document:
        return QVariant::fromValue(data.data());
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> GeoJsonParser::roleNames() const
{
    static QHash<int, QByteArray> mapping
    {
        { NameRole,     "name" },
        { LayerName,    "layerName" },
        { Polygons,     "polygons" },
        { Points,       "points" },
        { Lines,        "lines"},
        { Visibility,   "visibility" },
        { Document,     "document"}
    };

    return mapping;
}

void GeoJsonParser::_addDocument(QSharedPointer<GeoJsonDocument> doc)
{
    int index = _documents.size();
    beginInsertRows(QModelIndex(), index, index);

    _documents.push_back(doc);

    endInsertRows();

    emit countChanged(rowCount());
}

void GeoJsonParser::removeDocument(int index)
{
    beginRemoveRows(QModelIndex(), index, index);

    _documents.erase(_documents.begin() + index);

    endRemoveRows();

    emit countChanged(rowCount());
}

void GeoJsonParser::removeDocument(const QString &nameCompared)
{
    auto pos = std::find_if(std::begin(_documents), std::end(_documents), [nameCompared](QSharedPointer<GeoJsonDocument> doc)
    {
        return doc->name() == nameCompared;
    });

    if (pos == std::end(_documents))
    {
        qDebug() << "Can't remove a document that doesn't exist";
        return;
    }

    int index = std::distance(std::begin(_documents), pos);
    assert(index < static_cast<int>(_documents.size()) && index >= 0);

    beginRemoveRows(QModelIndex(), index, index);
    _documents.erase(pos);
    endRemoveRows();

    emit countChanged(rowCount());
}

void GeoJsonParser::toggleVisibilityForLayers(bool isVisible)
{
    for (auto document : _documents)
    {
        document->setIsVisible(isVisible);
    }
}
