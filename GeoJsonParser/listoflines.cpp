#include "listoflines.h"

int ListOfLines::count() const
{
    return static_cast<int>(_lines.size());
}

QVariant ListOfLines::get(int index) const
{
    if (index < 0 || index >= count())
    {
        qDebug() << "Invalid index for line getter";
        return QVariant();
    }

    return QVariant::fromValue(_lines.at(index));
}

void ListOfLines::add(const QGeoPath &line)
{
    _lines.push_back(line);
    emit countChanged(count());
}

QVariant ListOfLines::data(const QModelIndex &index, int role) const
{
    switch (role)
    {
    case NameRole:
        return QVariant();
    case GetRole:
        return get(index.row());
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> ListOfLines::roleNames() const
{
    static QHash<int, QByteArray> mapping
    {
        { NameRole, "name" },
        { GetRole,  "get" }
    };

    return mapping;
}
