#include "listofpoints.h"

int ListOfPoints::count() const
{
    return static_cast<int>(_points.size());
}

QVariant ListOfPoints::get(int index) const
{
    if (index < 0 || index >= count())
    {
        qDebug() << "Invalid index for line getter";
        return QVariant();
    }

    return QVariant::fromValue(_points.at(index));
}

void ListOfPoints::add(const QGeoCircle &point)
{
    _points.push_back(point);
    emit countChanged(count());
}

QVariant ListOfPoints::data(const QModelIndex &index, int role) const
{
    switch (role)
    {
    case NameRole:
        return QVariant();
    case GetRole:
        return get(index.row());
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> ListOfPoints::roleNames() const
{
    static QHash<int, QByteArray> mapping
    {
        { NameRole, "name" },
        { GetRole,  "get" }
    };

    return mapping;
}
