#include "missionmanager.h"
#include <QDebug>
#include "MissionData/standardmissiondata.h"

#ifdef _WIN32
constexpr TCHAR missionDataSharedMemName[] = TEXT("missionDataMem");
#else
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
const char* missionDataSharedMemName = "missionDataMem";
#endif

MissionManager::MissionManager()
    : _isSharedMemoryConnected(false),
      _listener(new ThirdPartyListener())
{
    // Connect the shared memory
    _isSharedMemoryConnected = _connectToSharedMemory();

    // Launch the rest
    _activeLookThread = new SharedMemoryThread(_mission_data, this);

    _standard       = new StandardMissionData(_mission_data, _activeLookThread);
    _fences         = new FenceMissionData(_mission_data, _activeLookThread);
    _rallyPoints    = new RallyPointMissionData(_mission_data, _activeLookThread);

    // Connect to all mission types to know when to update the visuals
    connect(_standard, &MissionData::newMissionDownloaded, this, &MissionManager::handleMissionDownloaded);
    connect(_fences, &MissionData::newMissionDownloaded, this, &MissionManager::handleMissionDownloaded);
    connect(_rallyPoints, &MissionData::newMissionDownloaded, this, &MissionManager::handleMissionDownloaded);

    connect(_standard, &StandardMissionData::newMissionCurrent, this, &MissionManager::newMissionCurrent);

    connect(_listener, &ThirdPartyListener::newLandingPoint, this, &MissionManager::handleNewLandingPoint);

    _activeLookThread->start();
}

void MissionManager::handleMissionDownloaded(int mission_type)
{

    // We might want a timer to set all the flags to zero
    _missionDownloadFlags[mission_type] = true;

    if (_missionDownloadFlags[0] && _missionDownloadFlags[1] && _missionDownloadFlags[2])
    {
        _missionDownloadFlags[0] = false;
        _missionDownloadFlags[1] = false;
        _missionDownloadFlags[2] = false;

        emit newMissionAvailable(_mission_data);
    }

}

void MissionManager::handleNewMissionCurrent(int current)
{
    emit newMissionCurrent(current);
}

void MissionManager::handleNewLandingPoint(double lat, double lon, float bearing)
{
    emit newLandingPoint(lat, lon, bearing);
}

bool MissionManager::_connectToSharedMemory()
{
#ifdef _WIN32
    _hMapFile = OpenFileMapping(
                    FILE_MAP_READ,
                    FALSE,
                    missionDataSharedMemName);

        if (_hMapFile == NULL)
        {
            //Could not create file mapping object
            qDebug() << "Failed mapping mission file..";
            return false;
        }

        qDebug() << "Mapped mission file..";

        _mission_data = (missionData_t*) MapViewOfFile(_hMapFile,
                                                       FILE_MAP_READ, // read permission
                                                       0,
                                                       0,
                                                       sizeof(missionData_t));

        if (_mission_data == NULL)
        {
            // Could not map view of file
            CloseHandle(_hMapFile);
            qDebug() << "Failed connecting mission file..";
            return false;
        }
#else
    _hMapFile = shm_open(missionDataSharedMemName, O_CREAT | O_RDWR, 0666); // Create file descriptor for shared mem. Create the mem if it doesn't already exist. Set

    if (_hMapFile < 0)
    {
        qDebug() << "Failed mapping vehicle file..";
        return false;
    }

    ftruncate(_hMapFile, sizeof(missionData_t));

    qDebug() << "Mapped vehicle file..";

    _mission_data = (missionData_t *)mmap(NULL, sizeof(missionData_t), PROT_READ | PROT_WRITE, MAP_SHARED, _hMapFile, 0);
    if (_mission_data == MAP_FAILED)
    {
        close(_hMapFile);
        qDebug() << "Failed connecting vehicle file..";
        return false;
    }
#endif

        qDebug() << "Connected to mission file.. size: " << sizeof(*_mission_data);

        return true;
}

MissionManager::~MissionManager()
{
    delete _standard;
    delete _fences;
    delete _rallyPoints;

    _activeLookThread->exit();

    delete _activeLookThread;
    delete _listener;
}
