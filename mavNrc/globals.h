//
//  globals.h
//  mavNrc
//
//  Created by Jeremi Levesque on 2022-02-10.
//  Copyright © 2022 Kris Ellis. All rights reserved.
//

#ifndef globals_h
#define globals_h

#include <stdio.h>
#include <standard/mavlink.h>
#ifdef _WIN32
#include <winsock2.h>
#include <windows.h>
#else
#include <netinet/in.h>
#endif
////
// Macro to print error socket depending on the operating system
//
#ifdef _WIN32
#define SERROR(str, ...)                          \
    printf(str, __VA_ARGS__);                     \
    printf(" (Error : %d)\n", WSAGetLastError()); \

#else
#define SERROR(str, ...) (perror(str))
#endif

#define LOG (printf)

#define MAX_WAYPOINTS 2048
#define MAX_GEOFENCE_PTS 128
#define MAX_RALLY_PTS 128
#define PACKET_MAX_LEN 2041

typedef struct { // To be used in the mission struct
    int itemRequested;
    int numMissionItems;
    int numMissionItemsToDownload;
    mavlink_mission_item_int_t items[MAX_WAYPOINTS];
} missionType_t;

typedef struct {
    
    int currentMissionItem;         // saves current item
    int missionStarted;             // 1 for true
    int missionMonitoringRequired;  // Supervisor sets this to 1 or 0 when active monitoring of the mission progress is required
    
    missionType_t standard;
    missionType_t fences;
    missionType_t rallyPoints;
} myMissionStruct_t;

typedef struct {
    myMissionStruct_t myNewMission;
    myMissionStruct_t myCurrentMission;
    int newMissionFromGcs;                      // Flag to send to all remotes
    int newMissionFromRemote;                   // Flag to send to QGC
    int missionCount;
    
    uint8_t myMavBaseMode;                      // Base mode is requested by GCS
    uint32_t myMavCustomMode;                   //Custom mode configuration conforming to PixHawk modes 65536 = manual, 67371008 = mission, etc (see spreadsheet)
} missionData_t;

missionType_t* getSpecificMissionType(myMissionStruct_t* mission, MAV_MISSION_TYPE missionType);

#endif /* globals_h */
