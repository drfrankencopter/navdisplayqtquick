//
//  globals.c
//  mavNrc
//
//  Created by Jeremi Levesque on 2022-02-10.
//  Copyright © 2022 Kris Ellis. All rights reserved.
//

#include "globals.h"

/*
    getSpecificMissionType
    Description : This function returns a pointer to a missionTypeInfo struct (standard, fences, rallyPoints) in the shared memory
                  based on the provided missionType that we want to work with.

    Author         : Jeremi Levesque
    Date         : 02/07/2022
    Return value: A pointer to the specific mission type information struct in the shared_memory or NULL on failure.
    Inputs        :
        - shm             : A pointer to the shared memory
        - missionType   : The mission type (MAV_MISSION_TYPE) from the mavlink protocol

*/
missionType_t* getSpecificMissionType(myMissionStruct_t* mission, MAV_MISSION_TYPE missionType) {
    missionType_t* missionSpecific;
    switch (missionType)
    {
    case MAV_MISSION_TYPE_MISSION:
        missionSpecific = &mission->standard;
        break;
    case MAV_MISSION_TYPE_FENCE:
        missionSpecific = &mission->fences;
        break;
    case MAV_MISSION_TYPE_RALLY:
        missionSpecific = &mission->rallyPoints;
        break;
    default:
        missionSpecific = NULL;
        break;
    }

    return missionSpecific;
}