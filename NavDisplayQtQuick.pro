QT += quick gui core widgets positioning
QT += quickcontrols2
QT += location
# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        ColorPalettes/darkpalette.cpp \
        ColorPalettes/lightpalette.cpp \
        ColorPalettes/palettebase.cpp \
        ColorPalettes/paletteholder.cpp \
        GeoJsonParser/geojsondocument.cpp \
        GeoJsonParser/geojsonparser.cpp \
        GeoJsonParser/listoflines.cpp \
        GeoJsonParser/listofpoints.cpp \
        GeoJsonParser/listofpolygons.cpp \
        MissionData/fencemissiondata.cpp \
        MissionData/missiondata.cpp \
        MissionData/rallypointmissiondata.cpp \
        MissionData/standardmissiondata.cpp \
        MissionData/waypoint.cpp \
        ThirdPartyController/thirdpartylistener.cpp \
        geojsonformatter.cpp \
        listofvoxels.cpp \
        main.cpp \
        mapboxhelper.cpp \
        mavNrc/globals.c \
        missionhelper.cpp \
        missionmanager.cpp \
        navdisplayglobals.cpp \
        navsettings.cpp \
        presetmanager.cpp \
        sharedmemorythread.cpp \
        unitsconverter.cpp \
        vehicledatamanager.cpp \
        vehiclememorythread.cpp \
        voxelmapreader.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    ColorPalettes/darkpalette.h \
    ColorPalettes/lightpalette.h \
    ColorPalettes/palettebase.h \
    ColorPalettes/paletteholder.h \
    GeoJsonParser/geojsondocument.h \
    GeoJsonParser/geojsonparser.h \
    GeoJsonParser/listoflines.h \
    GeoJsonParser/listofpoints.h \
    GeoJsonParser/listofpolygons.h \
    MissionData/fencemissiondata.h \
    MissionData/missiondata.h \
    MissionData/rallypointmissiondata.h \
    MissionData/standardmissiondata.h \
    MissionData/waypoint.h \
    ThirdPartyController/sockets.h \
    ThirdPartyController/thirdpartylistener.h \
    enums.h \
    geojsonformatter.h \
    listofvoxels.h \
    mapboxhelper.h \
    mavNrc/config.h \
    mavNrc/frl_file_types.h \
    mavNrc/globals.h \
    missionhelper.h \
    missionmanager.h \
    navdisplayglobals.h \
    navsettings.h \
    presetmanager.h \
    sharedmemorythread.h \
    unitsconverter.h \
    vehicledatamanager.h \
    vehiclememorythread.h \
    voxelmapreader.h

DISTFILES +=

INCLUDEPATH += ./mavlink

QSG_INFO=1

linux: {
    LIBS += -lrt
}

