#include <QGuiApplication>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "sharedmemorymanager.h"
#include "geojsonformatter.h"
#include "mapboxhelper.h"
#include "navdisplayglobals.h"
#include "missionmanager.h"
#include "vehicledatamanager.h"
#include "missionhelper.h"
#include "enums.h"
#include "unitsconverter.h"
#include "navsettings.h"
#include "GeoJsonParser/geojsonparser.h"
#include "ColorPalettes/palettebase.h"
#include "ColorPalettes/paletteholder.h"
#include "presetmanager.h"
#include "voxelmapreader.h"
#include "listofvoxels.h"
int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QApplication app(argc, argv);

    // Create the objects in C++ to expose them just under in QML
    // It's important to create the objects before creating the qml engine.
    QScopedPointer<NavSettings> navSettings(new NavSettings);
    QScopedPointer<VehicleDataManager> vehicleManager(new VehicleDataManager);
    QScopedPointer<MissionManager> missionManager(new MissionManager);
    QScopedPointer<NavDisplayGlobals> navDisplay(new NavDisplayGlobals);
    QScopedPointer<MissionHelper> missionHelper(new MissionHelper(missionManager->standard(), vehicleManager.data()));
    QScopedPointer<MapboxHelper> mapboxHelper(new MapboxHelper());
    QScopedPointer<UnitsConverter> unitsConverter(new UnitsConverter(navSettings.data()));
    QScopedPointer<GeoJsonParser> geoJsonParser(new GeoJsonParser());
    QScopedPointer<PaletteHolder> paletteHolder(new PaletteHolder(navSettings.data()));
    QScopedPointer<PresetManager> presetManager(new PresetManager(navSettings.data()));
    QScopedPointer<VoxelMapReader> voxelMapReader(new VoxelMapReader());

    QQmlApplicationEngine engine;
    app.setOrganizationName("National Research Council");
    app.setOrganizationDomain("NRC-CNRC");

    // Expose the settings object to QML
    engine.rootContext()->setContextProperty("navSettings", navSettings.data());

    // Expose other objects to QML
    qmlRegisterType<GeoJsonFormatter>("GeoJsonFormatter", 1, 0, "GeoJsonFormatter");
    qmlRegisterInterface<Waypoint>("Waypoint", 1);
    Enums::declareQml();
    GeoJsonParser::declareQml();
    PaletteBase::declareQml();
    ListOfVoxels::declareQml();

    QGeoPolygon polygon;
    polygon.setPath({{51.11, 17.13},
                     {50.42, 30.54},
                     {58.36, 26.70},
                     {51.11, 17.13}});

    engine.rootContext()->setContextProperty("poly", QVariant::fromValue(polygon));

    engine.rootContext()->setContextProperty("vehicleManager", vehicleManager.data());
    engine.rootContext()->setContextProperty("missionManager", missionManager.data());
    engine.rootContext()->setContextProperty("navDisplay", navDisplay.data());
    engine.rootContext()->setContextProperty("missionHelper", missionHelper.data());
    engine.rootContext()->setContextProperty("mapboxHelper", mapboxHelper.data());
    engine.rootContext()->setContextProperty("units", unitsConverter.data());
    engine.rootContext()->setContextProperty("geoJsonParser", geoJsonParser.data());
    engine.rootContext()->setContextProperty("paletteHolder", paletteHolder.data());
    engine.rootContext()->setContextProperty("presetManager", presetManager.data());
    engine.rootContext()->setContextProperty("voxelMap", voxelMapReader.data());

    // General QML stuff to connect the bridge between C++ and QML
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    int return_code = app.exec();

    return return_code;
}
