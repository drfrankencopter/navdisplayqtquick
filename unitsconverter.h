#ifndef UNITSCONVERTER_H
#define UNITSCONVERTER_H

#include <QObject>
#include <QString>

#include "navsettings.h"

// This class/object is to allow the dynamic conversion of units in the UI. Every distance/speed unit is customizable
// in the settings menu of the app. In QML, everytime you display a distance/speed unit, you should muliply its value
// by the horizontal/vertical factor. Everything is computed in meters & meters/sec in the app, so if the user wants the units to be
// displayed in meters, the factor would be 1 so the value wouldn't change. Or if the user wants the units to be in
// feet for example, then the factor would be the value to convert meters to feet.
//
// The object also provides a string of the units you're displaying. If you're displaying the units of any distance/speed,
// you should use the appropriate functions so the unit string can be changed at runtime as well.

class UnitsConverter : public QObject
{
    Q_OBJECT

public:
    UnitsConverter(NavSettings* navSettings, QObject* parent=nullptr);
    ~UnitsConverter() = default;

    // Factors of the units (multiply those to meters to get the proper value)
    Q_PROPERTY(double       horizontalFactor        READ horizontalFactor       NOTIFY horizontalFactorChanged)
    Q_PROPERTY(double       verticalFactor          READ verticalFactor         NOTIFY verticalFactorChanged)
    Q_PROPERTY(double       horizontalSpeedFactor   READ horizontalSpeedFactor  NOTIFY horizontalSpeedFactorChanged)
    Q_PROPERTY(double       verticalSpeedFactor     READ verticalSpeedFactor    NOTIFY verticalSpeedFactorChanged)

    // Strings of the units
    Q_PROPERTY(QString      horizontalUnits         READ horizontalUnits        NOTIFY horizontalUnitsChanged)
    Q_PROPERTY(QString      verticalUnits           READ verticalUnits          NOTIFY verticalUnitsChanged)
    Q_PROPERTY(QString      horizontalSpeedUnits    READ horizontalSpeedUnits   NOTIFY horizontalSpeedUnitsChanged)
    Q_PROPERTY(QString      verticalSpeedUnits      READ verticalSpeedUnits     NOTIFY verticalSpeedUnitsChanged)

    double  horizontalFactor()      { return _horizontalFactor;         }
    double  verticalFactor()        { return _verticalFactor;           }
    double  horizontalSpeedFactor() { return _horizontalSpeedFactor;    }
    double  verticalSpeedFactor()   { return _verticalSpeedFactor;      }
    QString horizontalUnits()       { return _horizontalUnits;          }
    QString verticalUnits()         { return _verticalUnits;            }
    QString horizontalSpeedUnits()  { return _horizontalSpeedUnits;     }
    QString verticalSpeedUnits()    { return _verticalSpeedUnits;       }

signals:
    void horizontalFactorChanged            (double newFactor);
    void verticalFactorChanged              (double newFactor);
    void horizontalSpeedFactorChanged       (double newFactor);
    void verticalSpeedFactorChanged         (double newFactor);
    void horizontalUnitsChanged             (const QString& newUnits);
    void verticalUnitsChanged               (const QString& newUnits);
    void horizontalSpeedUnitsChanged        (const QString& newUnits);
    void verticalSpeedUnitsChanged          (const QString& newUnits);

public slots:
    void handleHorizontalUnitsChanged       (int newUnitsEnum);
    void handleVerticalUnitsChanged         (int newUnitsEnum);
    void handleHorizontalSpeedUnitsChanged  (int newUnitsEnum);
    void handleVerticalSpeedUnitsChanged    (int newUnitsEnum);

private:
    std::pair<double, QString>  _determineUnits(int units_enum);
    std::pair<double, QString>  _determineSpeedUnits(int units_enum);

    double  _horizontalFactor;
    double  _verticalFactor;
    double  _horizontalSpeedFactor;
    double  _verticalSpeedFactor;

    QString _horizontalUnits;
    QString _verticalUnits;
    QString _horizontalSpeedUnits;
    QString _verticalSpeedUnits;

    NavSettings* _navSettings;
};

#endif // UNITSCONVERTER_H
