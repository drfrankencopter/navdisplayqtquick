import QtQuick 2.0
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import "helper.js" as Helper

MouseArea {
    function addPointToMap(lat, lon) {
        contextPoint.coordinate.latitude = lat
        contextPoint.coordinate.longitude = lon
    }

    id: _root
    acceptedButtons: Qt.RightButton
    anchors.fill: parent

    property bool isInfoVisible: contextMenu.visible

    property int _x: 0
    property int _y: 0

    property var lat
    property var lon
    property var alt
    property var time
    property string speedDefaultIndicator: ""
    property var distance

    property string latFormatted: lat ? (lat.toFixed(7)) : "N/A"
    property string lonFormatted: lon ? (lon.toFixed(7)) : "N/A"
    property string altFormatted: alt ? ((alt * units.verticalFactor).toFixed(0) + " " + units.verticalUnits) : "N/A"
    property string timeFormatted: time ? Helper.formatSecInTime(time) + " " + speedDefaultIndicator : "N/A"
    property string distanceFormatted: distance ? (distance * units.horizontalFactor).toFixed(0) + " " + units.horizontalUnits : "N/A"

    onIsInfoVisibleChanged: {
        alt = null
    }

    onClicked: {
        var c = mainMap.toCoordinate(Qt.point(mouse.x, mouse.y))
        let lat = c.latitude
        let lon = c.longitude

        mapboxHelper.getAltitudeAt(lat, lon)

        // Add point to map
        addPointToMap(lat, lon)

        // Coords to put show on the popup
        _root.lat     = lat
        _root.lon     = lon

        // Pixel coords to position the popup
        let pos = calcWindowPosition(mouse.x, mouse.y, contextMenu.width, contextMenu.height)
        _root._x      = pos.x
        _root._y      = pos.y

        // update the time
        // If speed is < 1 (we're prob hovering or not moving), let's assume a default speed of 5 m/s.
        let current_speed
        if (vehicleManager.groundSpeed() < 1) {
            current_speed = 5.0
            _root.speedDefaultIndicator = "(5 m/s)"
        }
        else {
            current_speed = vehicleManager.groundSpeed()
            _root.speedDefaultIndicator = ""
        }

        _root.time = missionHelper.timeBetweenPoints(vehicleManager.lat(), vehicleManager.lon(), lat, lon, current_speed, current_speed)

        // update the distance
        _root.distance = missionHelper.distanceBetween(vehicleManager.lat(), vehicleManager.lon(), lat, lon)

        contextMenu.open()
    }

    function calcWindowPosition(x, y, width, height) {
        var secure_x = x
        var secure_y = y

        if ((secure_x + width) > navSettings.windowWidth) {
            secure_x = secure_x - width
        }

        if ((secure_y + height) > navSettings.windowHeight) {
            secure_y = secure_y - height
        }

        return { x: secure_x, y: secure_y }
    }

    Connections {
        target: mapboxHelper
        function onGroundAltitudeReadyAtPoint(altitude, isValid) {
            if (isValid)
                alt = altitude
            else
                alt = null
        }
    }

    Popup {
        id: contextMenu
        x: parent._x
        y: parent._y
        width: 200
        height: 150
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        background: Rectangle {
            color: paletteHolder.hudHoverColor
            border.color: "transparent"
        }

        property color textColor: paletteHolder.generalText

        Item {
            width: parent.width
            height: parent.height
            anchors.centerIn: parent

            Text {
                id: popupTitle
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Point info"
                font.pointSize: 10
                color: contextMenu.textColor
            }

            Rectangle {
                width: parent.width * 0.75
                height: 1
                color: contextMenu.textColor
                anchors.top: popupTitle.bottom
                anchors.horizontalCenter: popupTitle.horizontalCenter
            }

            GridLayout {
                columns: 2
                width: parent.width
                height: parent.height - popupTitle.height
                anchors.top: popupTitle.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                Text {
                    text: "Lat:"
                    font.pointSize: 9
                    color: contextMenu.textColor

                }

                Text {
                    text: _root.latFormatted
                    color: contextMenu.textColor
                    Layout.fillWidth: true
                }

                Text {
                    text: "Lon:"
                    font.pointSize: 9
                    color: contextMenu.textColor
                }

                Text {
                    text: _root.lonFormatted
                    Layout.fillWidth: true
                    color: contextMenu.textColor
                }

                Text {
                    text: "Alt:"
                    font.pointSize: 9
                    color: contextMenu.textColor
                }

                Text {
                    text: _root.altFormatted
                    Layout.fillWidth: true
                    color: contextMenu.textColor
                }

                Text {
                    text: "Travel time:"
                    font.pointSize: 9
                    color: contextMenu.textColor
                }

                Text {
                    text: _root.timeFormatted
                    Layout.fillWidth: true
                    color: contextMenu.textColor
                }

                Text {
                    text: "Distance:"
                    font.pointSize: 9
                    color: contextMenu.textColor
                }

                Text {
                    text: _root.distanceFormatted
                    Layout.fillWidth: true
                    color: contextMenu.textColor
                }
            }
        }
    }
}
