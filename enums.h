#ifndef ENUMS_H
#define ENUMS_H
#include <QtGlobal>
#include <QObject>
#include <standard/mavlink.h>
#include <QQmlEngine>

// Enums is a utility class that is used to expose Enums to QML. We can't directly expose Enums to QML
// without encapsulating them in 'static' a Q_OBJECT from which we can access the enums on the QML side.

class Enums : public QObject
{
    Q_OBJECT

public:
    Enums() : QObject() {}

    enum WAYPOINT_TYPE {
        Waypoint                = MAV_CMD_NAV_WAYPOINT,
        Land                    = MAV_CMD_NAV_LAND,
        Takeoff                 = MAV_CMD_NAV_TAKEOFF,
        UndefinedWaypoint       = 0
    };

    enum FENCE_TYPE {
        ReturnPoint             = MAV_CMD_NAV_FENCE_RETURN_POINT,
        CircleInclusion         = MAV_CMD_NAV_FENCE_CIRCLE_INCLUSION,
        CircleExclusion         = MAV_CMD_NAV_FENCE_CIRCLE_EXCLUSION,
        PolygonVertexInclusion  = MAV_CMD_NAV_FENCE_POLYGON_VERTEX_INCLUSION,
        PolygonVertexExclusion  = MAV_CMD_NAV_FENCE_POLYGON_VERTEX_EXCLUSION,
        UndefinedFence          = 0
    };

    enum MISSION_TYPE {
        Standard                = MAV_MISSION_TYPE_MISSION,
        Fence                   = MAV_MISSION_TYPE_FENCE,
        RallyPoint              = MAV_MISSION_TYPE_RALLY,

    };

    enum DIST_UNITS {
        Imperial,
        Metric,
        Nautical
    };

    enum SPEED_UNITS {
        Knots,
        MetersPerSec,
        FtPerMin
    };

    enum HUD_MODE{
        DefaultMode  = 0,
        DeviateMode  = 1
    };

    enum DEVIATE_STATE{
        NoMission=0,
        Viewing =1,
        Adding=2,
        Editing=3
    };

    Q_ENUM(WAYPOINT_TYPE)
    Q_ENUM(FENCE_TYPE)
    Q_ENUM(DIST_UNITS)
    Q_ENUM(SPEED_UNITS)
    Q_ENUM(HUD_MODE)
    Q_ENUM(DEVIATE_STATE)


    static void declareQml() {
        qmlRegisterType<Enums>("NavEnums", 1, 0, "Enums");
    }
};

#endif // ENUMS_H
