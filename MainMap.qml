import QtQuick 2.2
import QtQuick.Window 2.0
import QtQuick.Controls 2.0
import QtLocation 5.15
import QtPositioning 5.6
import QtGraphicalEffects 1.15
import Palettes 1.0

Map {
    function controllerMove(){
        var coords = map.fromCoordinate( QtPositioning.coordinate(vehicleManager.lat(),vehicleManager.lon()),false)
        switch (map.controllerMode){
        case "Heading":{
            var offsetPosition = map.toCoordinate(Qt.point(coords.x,coords.y+aircraftOffset),false)
            updateMapPosition(offsetPosition.latitude, offsetPosition.longitude, vehicleManager.heading())
            break;
        }
        case "Direction":{
            updateMapPosition(vehicleManager.lat(),vehicleManager.lon(),headingDirection)
            break;
        }
        case "Velocity":{//not implemented correctly
            updateMapPosition(vehicleManager.lat(),vehicleManager.lon(),vehicleManager.heading()+Math.atan2(vehicleManager.eastVel(),vehicleManager.northVel()))
            break;
        }
        }
    }

    function updateMapPosition(lat,lng,hdg){
        map.center = QtPositioning.coordinate(lat,lng)
        map.bearing = hdg;
    }

    function controlZoom() {
        var groundSpeed = vehicleManager.groundSpeed()
        var commandedZoomLevel
        // Speed limits in m/s
        if (groundSpeed < 3) {
            commandedZoomLevel = 20.0
        } else if (groundSpeed < 10) {
            commandedZoomLevel = 17.0
        } else if (groundSpeed < 40) {
            commandedZoomLevel = 16.5
        } else if (groundSpeed < 65) {
            commandedZoomLevel = 16.0
        } else if (groundSpeed < 150) {
            commandedZoomLevel = 15.0
        } else {
            commandedZoomLevel = 14.0
        }

        zoomLevelCommanded = commandedZoomLevel
    }

    function controlTilt(){
        var groundSpeed = vehicleManager.groundSpeed()
        var commandedTiltLevel
        // Speed limits in m/s
        if (groundSpeed < 3) {
            commandedTiltLevel = 0
        } else if (groundSpeed < 10) {
            commandedTiltLevel = 10
        } else if (groundSpeed < 65) {
            commandedTiltLevel = 20.0
        } else if (groundSpeed < 150) {
            commandedTiltLevel = 30.0
        } else {
            commandedTiltLevel = 0.0
        }

        tiltCommanded = commandedTiltLevel
    }
    function setMapType(stylename){
        for(var i=0;i<map.supportedMapTypes.length;i++){
            if(map.supportedMapTypes[i].name===stylename){
                map.activeMapType = map.supportedMapTypes[i]
            }
        }

        if (navSettings.supportAutomaticPaletteChange) {
            if (mainMap.activeMapType.night) {
                navSettings.colorPalette = Palette.Light
            }
            else {
                navSettings.colorPalette = Palette.Dark
            }
        }
    }

    plugin:Plugin {
        id: mapboxglPlugin
        name: "mapboxgl"

        PluginParameter{
            name: "mapboxgl.access_token"
            value: "pk.eyJ1IjoiY3ZsYWQiLCJhIjoiY2t5YnU5ZWFpMDBteTJvcGF5dGJyMXczeCJ9.yuupYYFrEW7-mNY7j213Wg"//this should be cached somewhere; needed for other mapbox stuff
        }
        PluginParameter{
            name: "mapboxgl.mapping.additional_style_urls"
            value: "mapbox://styles/cvlad/ckyke0j502hf915kwph2xwkp7"
        }
    }

    id: map
    anchors.fill: parent
    center: QtPositioning.coordinate(vehicleManager.lat(), vehicleManager.lon())
    zoomLevel: 17
    z: 10

    copyrightsVisible: false
    gesture.enabled: true
    gesture.acceptedGestures: MapGestureArea.PanGesture | MapGestureArea.FlickGesture | MapGestureArea.PinchGesture | MapGestureArea.RotationGesture | MapGestureArea.TiltGesture
    gesture.flickDeceleration: 3000

    property bool controllerActive: navSettings.controllerActive
    property bool controllerPaused: false
    property string controllerMode : navSettings.controllerMode
    property bool missionActive: navSettings.missionActive
    property real aircraftOffset: navSettings.aircraftOffset
    property real headingDirection: 0
    property bool _automaticZoomEnabled: navSettings.automaticZoomEnabled
    property bool _automaticTiltEnabled: navSettings.automaticTiltEnabled
    property real zoomLevelCommanded: 17.0
    property real tiltCommanded: 0
    property bool _showZoomButtons: navSettings.showZoomButtons
    property var offsetPosition

    Component.onCompleted: map.setMapType(navSettings.mapboxStyle)

    NumberAnimation {
        id: zoomLevelAnimation
        target: map
        property: "zoomLevel"
        from: map.zoomLevel
        to: map.zoomLevelCommanded
        duration: 3000
        easing.type: Easing.OutQuart
    }

    gesture.onPanStarted: {
        controllerPaused=true
    }
    NumberAnimation {
        id: tiltLevelAnimation
        target: map
        property: "tilt"
        from: map.tilt
        to: map.tiltCommanded
        duration: 3000
        easing.type: Easing.OutQuart
    }
    Connections{
        target: timer
        function onTriggered(){
            if(map.controllerActive && !map.controllerPaused){
                map.controllerMove()

                if (_automaticZoomEnabled) {
                    controlZoom()

                    if (zoomLevelCommanded != zoomLevel) {
                        zoomLevelAnimation.start()
                    }
                }
                if(_automaticTiltEnabled){
                    controlTilt()
                    if (tiltCommanded != map.tilt) {
                        tiltLevelAnimation.start()
                    }
                }
            }
        }
    }
    onCenterChanged: {
        if (map.controllerActive && !map.controllerPaused) {
            if (map.controllerMode === "Heading") {
                var coords = map.fromCoordinate( QtPositioning.coordinate(vehicleManager.lat(),vehicleManager.lon()),false)
                var offsetPosition = map.toCoordinate(Qt.point(coords.x,coords.y+aircraftOffset),false)
                updateMapPosition(offsetPosition.latitude, offsetPosition.longitude, vehicleManager.heading())
            }
            else {
                updateMapPosition(vehicleManager.lat(), vehicleManager.lon(), headingDirection)
            }
        }
    }
}

