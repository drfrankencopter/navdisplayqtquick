#include "geojsonformatter.h"

#include <QFile>
#include <QJsonDocument>


GeoJsonFormatter::GeoJsonFormatter()
{



}

void GeoJsonFormatter::addLayer(QString path,QString name)
{
    QFile file(QUrl(path).toLocalFile());
    if(!file.open(QIODevice::ReadOnly| QFile::Text)){
        qDebug()<<"Cannot open file" +file.errorString();
        //QMessageBox::warning(this,"Error", "Cannot open file :"+file.errorString());//maybe change to qml
        return;
    }
    QTextStream in(&file);
    QString jsonString =in.readAll();
    QJsonDocument doc =QJsonDocument::fromJson(jsonString.toLatin1());
    if(doc.isNull()){
        qDebug()<<"Invalid Json";
        //QMessageBox::warning(this,"Error", "File "+file.fileName()+" is not valid JSON");
        return;
    }
    m_layers.append(LayerData{path,jsonString,name});
}

QString GeoJsonFormatter::layerData(QString path)
{
    for(auto layer : m_layers){
        if(layer.path==path){
            return layer.json;
        }
    }
    return "";
}

QString GeoJsonFormatter::layerName(QString path)
{
    for(auto layer : m_layers){
        if(layer.path==path){
            return layer.name;
        }
    }
    return "";
}
