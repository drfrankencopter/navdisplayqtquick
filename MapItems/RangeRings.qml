import QtQuick 2.0
import QtLocation 5.15

MapItemGroup {
    id: _root

    property var coords
    property var color
    property int lineWidth

    property var _coords: coords
    property var _color: color ? color : "white"
    property int _step: navSettings.rangeRingStep
    property int _lineWidth: lineWidth ? lineWidth : 1

    visible: navSettings.showRangeRings

    MapCircle {
        id: smallCircle
        center: _coords
        radius: _root._step
        border.width: _root._lineWidth
        border.color: _root._color
        color: Qt.rgba(0, 0, 0, 0)
    }

    MapCircle {
        id: mediumCircle
        center: _coords
        radius: _root._step * 2
        border.width: _root._lineWidth
        border.color: _root._color
        color: Qt.rgba(0, 0, 0, 0)
    }

    MapCircle {
        id: largeCircle
        center: _coords
        radius: _root._step * 3
        border.width: _root._lineWidth
        border.color: _root._color
        color: Qt.rgba(0, 0, 0, 0)
    }

    MapCircle {
        id: xLargeCircle
        center: _coords
        radius: _root._step * 4
        border.width: _root._lineWidth
        border.color: _root._color
        color: Qt.rgba(0, 0, 0, 0)
    }
}
