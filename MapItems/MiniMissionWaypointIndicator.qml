import QtQuick 2.9
import QtLocation 5.15
import QtQml 2.15
import NavEnums 1.0

MapQuickItem {
    id: root
    anchorPoint.x: circle.width/2
    anchorPoint.y: circle.height/2
    coordinate {
        latitude: latitude;
        longitude: longitude;
    }
    property var waypointNumber:number
    property var circleColor: "yellow"
    property var waypointRadius: acceptanceRadius
    property var waypointSpeed: speed

    onXChanged: {
        mapOverlay.moveMiniMissionWaypoint(waypointNumber-1,coordinate)
    }
    onYChanged: {
        mapOverlay.moveMiniMissionWaypoint(waypointNumber-1,coordinate)
    }
    Drag.active:dragArea.drag.active

    sourceItem: Rectangle {
        id: circle
        width: 25
        height: width
        radius: width*0.5
        z:10
        border.width: 0
        color: circleColor
        Text {
            anchors.centerIn: parent
            color: "black"
            text: root.waypointNumber
        }
    }
    MouseArea{
        id:dragArea
        anchors.fill: parent
        drag.target: parent
        preventStealing: false
        onClicked: {
            mapOverlay.miniMissionSelectionChanged(waypointNumber-1)
        }
    }

}



