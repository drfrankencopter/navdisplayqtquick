import QtQuick 2.0
import QtLocation 5.15

MapPolygon {

    // TODO, we should probably compute the path of the doghouse polygon in C++
    // and expose the path to QML after that.
    id: _root

    property var _dogHousePath: []

    color: "transparent"
    border.width: 3
    border.color: "magenta"
    path: _dogHousePath

    Connections {
        target: missionManager
        function onNewLandingPoint(lat, lon, heading) {
            _root._dogHousePath = calcDogHousePoints(lat, lon, heading, 0.002/*km*/)
        }

        function calcDogHousePoints(lat, lon, h, scale) {
            var newPoints = []
            // Point 0 added
            //newPoints.push({ latitude: lat, longitude: lon })

            // Dog house representation of the following points being drawn.
            //                          P1 (0, 1)
            //                         / \
            //                       /     \        sqrt(0.6^2 + 1^2)
            //                    /           \
            //                 /                 \
            //  P2 (-1, 0.4)  |                   | P5 (1, 0.4)
            //                |                   |
            //           1.4  |                   | 1.4
            //                |                   |
            //  P3 (-1, -1)   |___________________| P4 (1, -1)
            //
            //

            // Point 1
            var p1 = coordsWithDistanceAndBearing(lat, lon, scale, h)

            // Point interm
            var pi = coordsWithDistanceAndBearing(lat, lon, scale, Math.abs(h-180))

            // Point 3
            var bearing_3 = roundBeadingTo360Bounds(h - 90)
            var p3 = coordsWithDistanceAndBearing(pi.latitude, pi.longitude, scale, bearing_3)

            // Point 2
            var p2 = coordsWithDistanceAndBearing(p3.latitude, p3.longitude, scale + scale*0.4, h)

            // Point 4
            var bearing_4 = roundBeadingTo360Bounds(h + 90)
            var p4 = coordsWithDistanceAndBearing(pi.latitude, pi.longitude, scale, bearing_4)

            // Point 5
            var p5 = coordsWithDistanceAndBearing(p4.latitude, p4.longitude, scale + scale*0.4, h)

            // Add points in order of drawing
            newPoints.push(p1)
            newPoints.push(p2)
            newPoints.push(p3)
            newPoints.push(p4)
            newPoints.push(p5)

            return newPoints
        }

        function coordsWithDistanceAndBearing(lati, loni, d, bearing) {
               var brng = bearing * (Math.PI / 180.0)   // Should be in radians
               var R = 6378.1 // Radius of the earth

               var lat1 = lati * (Math.PI / 180.0)
               var lon1 = loni * (Math.PI / 180.0)

               // Reference for the following calculations : https://stackoverflow.com/questions/7222382/get-lat-long-given-current-point-distance-and-bearing
               var lat2 = Math.asin( Math.sin(lat1)*Math.cos(d/R) + Math.cos(lat1)*Math.sin(d/R)*Math.cos(brng));
               var lon2 = lon1 + Math.atan2(Math.sin(brng)*Math.sin(d/R)*Math.cos(lat1), Math.cos(d/R)-Math.sin(lat1)*Math.sin(lat2));

               lat2 = lat2 / (Math.PI / 180.0)
               lon2 = lon2 / (Math.PI / 180.0)
               return { latitude: lat2, longitude: lon2 }
        }

        function roundBeadingTo360Bounds(heading) {
           if (heading > 0) {
               return heading%360
           }
           else {
               var tmp = heading
               while (tmp < 0) {
                   tmp += 360
               }
               return tmp
           }
        }
    }
}
