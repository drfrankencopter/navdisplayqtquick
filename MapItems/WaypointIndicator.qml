import QtQuick 2.15
import QtLocation 5.15
import QtQml 2.15
import NavEnums 1.0

MapQuickItem {
    id: _itemRoot
    anchorPoint.x: circle.width/2
    anchorPoint.y: circle.height/2
    coordinate {
        latitude: latitude
        longitude: longitude
        altitude: altitude
    }

    property var waypointType: type
    property var waypointNumber: number
    property var waypointLabelText: waypointType === Enums.Land ? "L" : (waypointType === Enums.Takeoff ? "T" : waypointNumber)

    property var currentWaypoint: missionManager.standard.getCurrentWaypoint()
    property var circleColor: waypointNumber >= currentWaypoint ? navSettings.missionLineColor : navSettings.oldMissionLineColor
    property var textColor: waypointNumber >= currentWaypoint ? "black" : "white"


    Connections {
        target: missionManager

        function onNewMissionCurrent(current) {
            currentWaypoint = missionManager.standard.getCurrentWaypoint()
        }
    }


    sourceItem: Rectangle {
        id: circle
        width: 25
        height: width
        radius: width*0.5
        border.width: 0
        color: circleColor
        Text {
            anchors.centerIn: parent
            color: "black"
            text: _itemRoot.waypointLabelText
        }
    }
}


