import QtQuick 2.0
import QtLocation 5.15

MapCircle {
    radius: acceptanceRadius
    color: "transparent";
    border.width: 2
    border.color: "yellow"
    center {
        latitude: latitude;
        longitude: longitude;
    }
}
