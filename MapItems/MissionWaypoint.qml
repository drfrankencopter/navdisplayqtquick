import QtLocation 5.15;
import QtQuick 2.15
MapCircle{
    border.width: 100
    border.color: acceptanceColor
    property real latitude: 0
    property real longitude: 0

    center {
        latitude: latitude
        longitude: longitude
    }

    property var currentWaypoint: missionManager.standard.getCurrentWaypoint()
    property var acceptanceColor: navSettings.radiusOnCurrentOnly ? "transparent" : (number >= currentWaypoint ? "magenta" : "gray")

    Connections {
        target: missionManager

        function onNewMissionCurrent(current) {
            currentWaypoint = missionManager.standard.getCurrentWaypoint()
        }
    }
}
