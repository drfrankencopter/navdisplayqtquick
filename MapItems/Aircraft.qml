import QtQuick 2.0
import QtLocation 5.15
import QtGraphicalEffects 1.15

//mapitem version of the aircraftIcon
MapQuickItem{
    id:aircraft
    anchorPoint.x: img.width/2
    anchorPoint.y: img.height/2
    zoomLevel: mainMap.zoomLevel < 19 ? 0 : 19
    sourceItem:Item {
        Image {
            id: img
            source: "qrc:/imgs/imgs/helicopter_icon.svg"
            width: 60 //these need to be based on something
            height: 60
        }

        ColorOverlay{
            anchors.fill: img
            source: img
            color: navSettings.aircraftColor
        }
    }
}
