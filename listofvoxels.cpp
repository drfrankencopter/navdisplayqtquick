#include "listofvoxels.h"

int ListOfVoxels::count() const {
    qDebug() << "size : " << rowCount();
    return static_cast<int>(_voxels.size());
}

QVariant ListOfVoxels::get(int index)
{
    QGeoPolygon poly = _voxels.at(index);

    if (!poly.isValid())
    {
        qDebug() << "Polygon is not valid";
    }

    return QVariant::fromValue(poly);
}

void ListOfVoxels::add(const QGeoPolygon& voxel)
{

    beginInsertRows(QModelIndex(), _voxels.size(), _voxels.size());
    _voxels.push_back(voxel);
    endInsertRows();
}

void ListOfVoxels::add(const ListOfVoxels& other)
{
    beginInsertRows(QModelIndex(), 0, other._voxels.size() - 1);
    _voxels = other._voxels; // We just copy the other list of voxels
    endInsertRows();
}

void ListOfVoxels::clearAll()
{
    beginRemoveRows(QModelIndex(), 0, _voxels.size() - 1);
    _voxels.clear();
    endRemoveRows();
}

QVariant ListOfVoxels::data(const QModelIndex &index, int role) const
{
    switch (role)
    {
    case NameRole:
        return QVariant();
    case GetRole:
        return QVariant::fromValue(_voxels.at(index.row()));
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> ListOfVoxels::roleNames() const
{
    static QHash<int, QByteArray> mapping
    {
        { NameRole, "name" },
        { GetRole,  "get" }
    };

    return mapping;
}
