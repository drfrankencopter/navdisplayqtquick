import QtQuick 2.0
import QtQuick.Window 2.0
import GeoJsonFormatter 1.0
Window {

    width: navSettings.windowWidth
    height: navSettings.windowHeight
    visible: true
    id: root
    title: "NavDisplay"

    property int timerRate: 60 //Rate of many system checks.

    //default app colors
    property color menuColor: Qt.rgba(0.9,0.9,0.9,1)
    property color textColor: Qt.rgba(0.1,0.1,0.1,1)
    property color secondaryColor: Qt.rgba(0.8,0.8,0.8,1)

    onWidthChanged: function () {
        navSettings.windowWidth = root.width
    }

    onHeightChanged: function () {
        navSettings.windowHeight = root.height
    }

    Component.onCompleted: {
            x = Screen.width / 2 - width / 2
            y = Screen.height / 2 - height / 2
    }

    //Global Components

    //
    Connections {
        target: vehicleManager
        function onMinimizeSwitchChanged(isMinimized) {
            if (isMinimized) {
                root.showMinimized()
            }
            else {
                root.showNormal()
            }
        }
    }
    //global timer. Used by various components
    Timer{
        id:timer
        interval: 1000 / timerRate
        running: true
        repeat: true
        triggeredOnStart: true
        signal fullSecond
        signal fullMinute
        property int ticks: 0
        property int secondsTick: 0
        onTriggered: {
            ++ticks
            if(ticks % timerRate === 0){
                timer.fullSecond()
                ++secondsTick
                if (secondsTick % 60 === 0) {
                    secondsTick = 1
                    timer.fullMinute()
                }
            }
        }
    }

    GeoJsonFormatter{
        id: formatter
    }

    //Map Components
    MainMap{
        id:mainMap
        anchors.fill:parent
        z:1
    }

    MapOverlay{
        id:mapOverlay
        anchors.fill:parent
        z:5
    }

    MapHud{
        id:mapHud
        anchors.fill:parent
        z:10
    }
    MenuScreen{
        id:menuScreen
        visible:false
        z:20
    }
}
