#ifndef GEOJSONFORMATTER_H
#define GEOJSONFORMATTER_H

#include <QJsonObject>
#include <QString>
#include <QObject>

class GeoJsonFormatter: public QObject
{
    Q_OBJECT
public:
    GeoJsonFormatter();

    struct LayerData{//Save this type
        QString path;
        QString json;
        QString name;
    };

    Q_INVOKABLE void addLayer(QString path,QString name);
    Q_INVOKABLE QString layerData(QString path);
    Q_INVOKABLE QString layerName(QString path);

private:

    QVector<LayerData> m_layers;
signals:


};

#endif // GEOJSONFORMATTER_H
