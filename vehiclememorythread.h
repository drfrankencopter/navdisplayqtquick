#ifndef VEHICLEMEMORYTHREAD_H
#define VEHICLEMEMORYTHREAD_H
#include <QThread>
#include "mavNrc/frl_file_types.h"

class VehicleMemoryThread : public QThread
{
    Q_OBJECT

public:
    VehicleMemoryThread(shared_data_t* shm, QObject *parent = nullptr);

signals:
    void    engageChanged(bool newEngageStatus);
    void    engageNumberChanged(unsigned int newEngageNumber);
    void    minimizeSwitchChanged(bool isMinimized);
protected:
    void    run();

private:
    shared_data_t* _shm;

    unsigned int     _engageNumber;
    bool    _isEngaged;
    bool    _isMinimized;
};

#endif // VEHICLEMEMORYTHREAD_H
