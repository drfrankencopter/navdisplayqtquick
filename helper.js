
function formatDistance(meters)
{
    var dist = Math.round(meters)
    if (dist > 1000 ){
        if (dist > 100000){
            dist = Math.round(dist / 1000)
        }
        else{
            dist = Math.round(dist / 100)
            dist = dist / 10
        }
        dist = dist + " km"
    }
    else{
        dist = dist + " m"
    }
    return dist
}

Number.prototype.toDeg = function(){
    return this*180/Math.PI
}
Number.prototype.toRad = function(){
    return this*Math.PI/180
}

function rotateCoord(lon,lat,bearing,distance){

    var φ1 = lat.toRad(),
    λ1 = lon.toRad(),
    brng = bearing.toRad(),
    d=distance,
    R=6371e3;

    const φ2 = Math.asin( Math.sin(φ1)*Math.cos(d/R) + Math.cos(φ1)*Math.sin(d/R)*Math.cos(brng) );
    const λ2 = λ1 + Math.atan2(Math.sin(brng)*Math.sin(d/R)*Math.cos(φ1),Math.cos(d/R)-Math.sin(φ1)*Math.sin(φ2));
    return{lng:φ2.toDeg(),lat:λ2.toDeg()}
}


function distance(lat1,lon1,lat2,lon2){
    const R = 6371e3; // metres
    const φ1 = lat1.toDeg(); // φ, λ in radians
    const φ2 = lat2.toDeg();
    const Δφ = (lat2-lat1).toDeg();
    const Δλ = (lon2-lon1).toDeg();

    const a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
              Math.cos(φ1) * Math.cos(φ2) *
              Math.sin(Δλ/2) * Math.sin(Δλ/2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    return R * c; // in metres
}

function formatBearing(bearing){
    var b = Math.round(bearing)
    return (b+360)%360
}

function midPoint(lat1, lon1, lat2, lon2) {
    var dLon = (lon2 - lon1).toRad();

    //convert to radians
    lat1 = lat1.toRad();
    lat2 = lat2.toRad();
    lon1 = lon1.toRad();

    var Bx = Math.cos(lat2) * Math.cos(dLon);
    var By = Math.cos(lat2) * Math.sin(dLon);
    var lat3 = Math.atan2(Math.sin(lat1) + Math.sin(lat2), Math.sqrt((Math.cos(lat1) + Bx) * (Math.cos(lat1) + Bx) + By * By));
    var lon3 = lon1 + Math.atan2(By, Math.cos(lat1) + Bx);

    //print out in degrees
    return { latitude: lat3.toDeg(), longitude: lon3.toDeg() }
}

function coordinateDistanceHeading(lat1, lon1, distance, heading) {
    let R = 6378.1 //Radius of the Earth
    let brng = heading * (Math.PI / 180) //Bearing is 90 degrees converted to radians.
    let d = distance / 1000 //Distance in km

    var lat1r = lat1 * (Math.PI / 180)
    var lon1r = lon1 * (Math.PI / 180)

    var lat2 = Math.asin( Math.sin(lat1r)*Math.cos(d/R) +
                         Math.cos(lat1r)*Math.sin(d/R)*Math.cos(brng))

    var lon2 = lon1r + Math.atan2(Math.sin(brng)*Math.sin(d/R)*Math.cos(lat1r),
                                 Math.cos(d/R)-Math.sin(lat1r)*Math.sin(lat2))

    lat2 = lat2 * (180 / Math.PI)
    lon2 = lon2 * (180 / Math.PI)

    return { latitude: lat2, longitude: lon2 }
}

function formatSecInTime(time_in_sec) {

    if (time_in_sec < 0) {
        return "N/A"
    }

    let hours = Math.floor(time_in_sec / 3600)
    var remainder = time_in_sec % 3600
    let minutes = Math.floor(remainder / 60)
    remainder = remainder % 60
    let seconds = remainder

    // Numbers formatting
    let hoursFormatted      = hours > 9     ? hours : "0" + hours
    let minutesFormatted    = minutes > 9   ? minutes : "0" + minutes
    let secondsFormatted    = seconds > 9   ? seconds : "0" + seconds

    return hoursFormatted + ":" + minutesFormatted + ":" + secondsFormatted
}
