#include "voxelmapreader.h"

#include <QFile>
#include <QDebug>
#include <QStringList>
#include <QtMath>

VoxelMapReader::VoxelMapReader(QObject *parent)
{
    Q_UNUSED(parent);
}

bool VoxelMapReader::readVoxelMap(QString path)
{
    QVector<QPair<double, double>> voxelMap;

    QFile inputFile(path);
    if (inputFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile);
       while (!in.atEnd())
       {
          QString line = in.readLine();
          QStringList list = line.split(",");
          double latitude  = list[0].toDouble();
          double longitude = list[1].toDouble();

          QGeoPolygon square;

          QGeoCoordinate p2 = _coordDistanceHeading(latitude, longitude, 10, 90);
          QGeoCoordinate p3 = _coordDistanceHeading(p2.latitude(), p2.longitude(), 10, 0);
          QGeoCoordinate p4 = _coordDistanceHeading(latitude, longitude, 10, 0);

          square.addCoordinate({ latitude, longitude }); // First point (bottom left)
          square.addCoordinate(p2); // (bottom right)
          square.addCoordinate(p3); // (top right)
          square.addCoordinate(p4); // (top left)

          _voxelsDb.add(square);
          if (_shouldShow()) {
              _voxels.add(square);
          }
       }

       qDebug() << "Finished reading voxel input file" << path << ". Current nb of voxels : " << _voxels.count();
       inputFile.close();
       return true;
    }
    else {
        qDebug() << "Could not read voxel input file " << path;
        return false;
    }
}

void VoxelMapReader::checkZoomLevel(double newZoomLevel)
{
    double previousZoomLevel = currentZoomLevel;
    double boundary = 15.4;


    if (newZoomLevel < boundary)
    {
        if (previousZoomLevel >= boundary)
        {
            qDebug() << "Removing the voxels";
            // Update if just crossed it
            _voxels.clearAll();
        }
    }
    else if (newZoomLevel >= boundary)
    {
        if (previousZoomLevel < boundary)
        {
            // Update if just crossed it
            qDebug() << "Adding the voxels";
            _voxels.add(_voxelsDb);
        }
    }

    currentZoomLevel = newZoomLevel;
}

void VoxelMapReader::checkPosition(double lat1, double lon1, double lat2, double lon2)
{
    // We want to remove voxels out of the field of view
    // We want to add voxels that are in the field of view
}

QGeoCoordinate VoxelMapReader::_coordDistanceHeading(double lat1, double lon1, double distance, double heading) const
{
    double R = 6378.1;                      // Radius of the Earth
    double brng = heading * (M_PI / 180.0); // Bearing is 90 degrees converted to radians.
    double d = distance / 1000;             // Distance in km

    double lat1r = lat1 * (M_PI / 180.0);
    double lon1r = lon1 * (M_PI / 180.0);

    double lat2 = qAsin( qSin(lat1r) * qCos(d/R) + qCos(lat1r) * qSin(d/R) * qCos(brng));

    double lon2 = lon1r + qAtan2(qSin(brng) * qSin(d/R) * qCos(lat1r), qCos(d/R) - qSin(lat1r) * qSin(lat2));

    lat2 = lat2 * (180 / M_PI);
    lon2 = lon2 * (180 / M_PI);

    return { lat2, lon2 };
}

bool VoxelMapReader::_shouldShow() const
{
    return currentZoomLevel > 15.4;
}
