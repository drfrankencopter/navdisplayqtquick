#ifndef MISSIONMANAGER_H
#define MISSIONMANAGER_H

#include <QObject>

#include "MissionData/standardmissiondata.h"
#include "MissionData/fencemissiondata.h"
#include "MissionData/rallypointmissiondata.h"
#include "ThirdPartyController/thirdpartylistener.h"

// MissionManager is pretty self-explanatory in its name in the sense that is manages the entire mission.
// Every mission type data is stored in here and handles whenever there's a new mission of any mission type.
// Once the entire mission is received, we signal that there's a new mission available. This signal is mostly
// used to we can refresh and update the UI right when there's a new mission. One really important point to note
// is that the MissionManager ONLY emits this newMission signal when all the mission types were fully received.
// This means that if it never receives the Geofences and/or the rally points of the new mission, no signal will
// be emitted and the UI will not be refreshed.
// The mission manager also start the activeLookThread that actively looks for changes in the shared memory file (
// checks if the mission number changed, etc.)
//
// TODO : We might want to figure out better ways of handling what to do in case we don't receive every mission type.

class MissionManager : public QObject
{
    Q_OBJECT

public:
    MissionManager();
    ~MissionManager();

    Q_PROPERTY(StandardMissionData*     standard    READ standard       CONSTANT)
    Q_PROPERTY(FenceMissionData*        fences      READ fences         CONSTANT)
    Q_PROPERTY(RallyPointMissionData*   rallyPoints READ rallyPoints    CONSTANT)

    StandardMissionData* standard()         { return _standard; }
    FenceMissionData* fences()              { return _fences; }
    RallyPointMissionData* rallyPoints()    { return _rallyPoints; }

signals:
    void newMissionAvailable(missionData_t* mission_data);
    void newMissionCurrent(int current);
    void newLandingPoint(double lat, double lon, float bearing);

public slots:
    void handleMissionDownloaded(int mission_type);
    void handleNewMissionCurrent(int current);
    void handleNewLandingPoint(double lat, double lon, float bearing);

private:
    bool _connectToSharedMemory();

#ifdef _WIN32
    HANDLE _hMapFile;
#else
    int _hMapFile;
#endif
    bool _isSharedMemoryConnected;

    missionData_t*          _mission_data;
    SharedMemoryThread*     _activeLookThread;

    ThirdPartyListener*     _listener;

    StandardMissionData*    _standard;
    FenceMissionData*       _fences;
    RallyPointMissionData*  _rallyPoints;

    bool _missionDownloadFlags[3] = {/* standard: */false, /* fence: */false, /* rally: */false};
};

#endif // MISSIONMANAGER_H
