#ifndef MAPBOXHELPER_H
#define MAPBOXHELPER_H

#include <QNetworkAccessManager>
#include <QObject>
#include <QPair>

// The MapboxHelper uses the Mapbox API to find to get specific information on specific points
// in the terrain elevation map provided by Mapbox.

class MapboxHelper : public QObject
{
    Q_OBJECT
public:
    MapboxHelper() = default;
    Q_INVOKABLE void getAltitudeAt(double lat,double lon,double distance); //currently distance is just used to link back to graph
    Q_INVOKABLE void getAltitudeAt(double lat,double lon);

signals:
    void groundAltitudeReady(double distance,double alt, bool isValid);
    void groundAltitudeReadyAtPoint(double alt, bool isValid);

public slots:
    void handleGroundAltitude();

private:
    QPair<double, bool> _parseForAltitude(const QJsonDocument& doc);

    QNetworkAccessManager m_manager;
};

#endif // MAPBOXHELPER_H
