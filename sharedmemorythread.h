#ifndef SHAREDMEMORYTHREAD_H
#define SHAREDMEMORYTHREAD_H

#include <QThread>
#include <QPointer>
#include "mavNrc/globals.h"

// This is a QThread which simply actively checks at a specific interval of time if some things changed
// in the shared memory file. If some things change like the mission number for example, it will emit a
// signal accordingly.

class SharedMemoryThread : public QThread
{
    Q_OBJECT

public:
    SharedMemoryThread(missionData_t* shm, QObject *parent = nullptr);

signals:
    void missionNumberChanged(missionData_t* mission_data);
    void missionCurrentChanged(int newMissionCurrent);
protected:
    void run();

private:
    int _missionNumber;
    int _missionCurrent;
    missionData_t* _mission_data;
};

#endif // SHAREDMEMORYTHREAD_H
