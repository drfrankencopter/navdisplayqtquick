#ifndef NAVDISPLAYGLOBALS_H
#define NAVDISPLAYGLOBALS_H

#include <QObject>

// TODO : This class might be deleted because we ended up not really using it.
// It was supposed to be a kind of global object holding the information on the Z property of
// each type of objects in the UI. We could then manage the order of the UI items all in one place,
// but we ended up using the z property rarely. If the z property is used in future developments,
// this could be a great way of ordering most of the items in the UI in one place.

class NavDisplayGlobals : public QObject
{
    Q_OBJECT
public:
    NavDisplayGlobals();

    Q_PROPERTY(qreal        mapInitZoom             READ mapInitZoom                CONSTANT)
    Q_PROPERTY(bool         defaultControllerActive READ defaultControllerActive    CONSTANT)
    Q_PROPERTY(bool         defaultWaypointsActive  READ defaultWaypointsActive     CONSTANT)

    Q_PROPERTY(qreal        zOrderTopMost           READ zOrderTopMost              CONSTANT)
    Q_PROPERTY(qreal        zOrderWidgets           READ zOrderWidgets              CONSTANT)
    Q_PROPERTY(qreal        zOrderVehicles          READ zOrderVehicles             CONSTANT)
    Q_PROPERTY(qreal        zOrderMapItems          READ zOrderMapItems             CONSTANT)
    Q_PROPERTY(qreal        zOrderTrajectoryLines   READ zOrderTrajectoryLines      CONSTANT)
    Q_PROPERTY(qreal        zOrderLowest            READ zOrderLowest               CONSTANT)

    // We could also add a section for default colors
    qreal   zOrderTopMost           () { return 1000; }
    qreal   zOrderWidgets           () { return 100; }
    qreal   zOrderVehicles          () { return 50; }
    qreal   zOrderMapItems          () { return 49; }
    qreal   zOrderTrajectoryLines   () { return 49; }
    qreal   zOrderLowest            () { return 0; }

    qreal   mapInitZoom             () { return _mapInitZoom; }
    bool    defaultControllerActive () { return _defaultControllerActive; }
    bool    defaultWaypointsActive  () { return _defaultWaypointsActive; }

private:
    qreal   _mapInitZoom = 17.0;
    bool    _defaultControllerActive = true;
    bool    _defaultWaypointsActive = true;


};

#endif // NAVDISPLAYGLOBALS_H
