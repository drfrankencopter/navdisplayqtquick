# Navigation Display

> Date: 2022-08-24
>
> Authors: Trent Powell, Jeremi Levesque, Kris Ellis

## Description

The navigation display is a navigation display that shows geographic and mapping data to be used by the pilot when supervising autonomy for the CVLAD project. The integrity of the application is built around Qt with the QML Engine to support multiple operating systems and platforms. We developed this interface to be able to use MapBox's GL vectorized maps instead of rasters. QGroundControl was initially supposed to act as a navigation display, but since it offers lots of limitations related to the underlying map provider, the effort needed for developement was minimized by developping a separate application.

One of the objectives was to be able to render GeoJson data as layers over the map. MapBox GL is a map plugin that's able to support lots of different types of layers. However, on the QML side there's a enormous lack of documentation around using the layers so we now print the GeoJson ourselves instead of relying on the Mapbox plugin for this purpose. The MapBox GL Plugin also enables us to rotate the map while having the labels in a correct orientation which wasn't possible with the raster maps of QGroundControl.

Another great objective of this project was to enable the pilot to create a mission mid-flight. This would particularily be useful to respond to Air Traffic Control requests, other obstacles, etc. Currently, this feature is half-implemented in the UI only and its development was put aside so the team could take the time to rethink a design to deviate from the mission temporarily. The feature is only really implemented in the UI and doesn't communicate with the mission manager in any way.

## How to first build

### Requirements
1. **Install Qt**
    1. Download Qt and its packages from the [Online installer](https://www.qt.io/download)
    2. Install the following packages from **Qt 5.12.2** (the version is important):
        * Qt Charts
        * *On Windows*: MinGW 64 bit
        * *On MacOS*: macOS
        * *On Linux*: GCC 64 bit
    3. You should now have Qt Creator installed on your machine with the Qt dependencies

2. **Install OpenSSL**
    
    OpenSSL is a **requirement** because of the MapBox GL API. Every request/response to/from MapBox is encrypted and OpenSSL enables to make use of these messages. If you don't have OpenSSL installed on you machine, the underlying map of the application will not be loaded and a looping message will be printed in Debug.

    * *MacOS*: OpenSSL should already be installed on your machine
    * *Linux*: No idea, might have to install it depending on what the OS provides by default
    * *Windows* :
        * [Install Chocolatey](https://docs.chocolatey.org/en-us/choco/setup)
        * After installing Chocolatey on your Windows Machine, run the following command to **install OpenSSL**:
        ``` powershell
        choco install OpenSSL
        ```

### How to run/build

1. **Create the shared memory file**
    
    Without this step, you'll be able to compile the project, but the application will crash systematically each time you'll try to run the app. The app is a primary navigation display for the mission data. This means that it needs to know about the mission data. The current implementation requires you to have the mission shared memory file created. To do that, you can:

    * *On your local machine:* **Launch MavNRC** since it creates and populates the mission data shared memory
    * *When MavNRC is on another machine:* Run the bossConsoleHelper because it should also create and populate the mission shared memory file mapping.

    Maybe it could be a future development to allow the app to run even if there are no currently created shared memory mapped file. A simple way to do that would be to create the shared memory mapped file on launch if it doesn't exist, but we would have to determine the behaviors of uninitialized mission memory.

2. **Run the app**
    * *When you have the source code:* Launch Qt Creator and open the project file NavDisplayQtQuick.pro. You might have to configure the project settings on the first launch. If so, just select the pre-generated kit, but **make sure to select MinGW as the compiler (or GCC compiler if you're on Unix systems)**. You'll then be able to run the app with the green triangle on the bottom left.

    * *When you just have the executable:* Just launch the executable :). It should be located in the *staging* folder.

3. Missions will be shown on the display once you upload the mission in the connected instance of QGroundControl. **If the missions do not show upon uploading, it is very very important to make sure that the QGC from which you're uploading the new mission has all the parameters correctly uploaded in MavNRC.** Some problems might occur related to the parameter protocol when starting either QGC or MavNRC. If you can't add geofences/rallypoints in QGC, it means that there was a problems with the parameters on startup. I would suggest restarting QGC & MavNRC in a different order to make sure all the parameters are uploaded properly and that you can access the geofences/rallypoints menus in plan view. 