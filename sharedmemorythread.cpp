#include <QDebug>
#include "sharedmemorythread.h"

SharedMemoryThread::SharedMemoryThread(missionData_t* shm, QObject *parent)
    : QThread{parent},
      _mission_data(shm)
{
    _missionNumber = -1;
}

void SharedMemoryThread::run()
{
    while (true)
    {
        usleep(10000);

        // Check if new mission was uploaded
        if (_mission_data->missionCount != _missionNumber)
        {
            _missionNumber = _mission_data->missionCount;
            emit missionNumberChanged(_mission_data);
        }

        if (_mission_data->myCurrentMission.currentMissionItem != _missionCurrent)
        {
            _missionCurrent = _mission_data->myCurrentMission.currentMissionItem;
            emit missionCurrentChanged(_missionCurrent);
        }

    }
}
