#ifndef MISSIONHELPER_H
#define MISSIONHELPER_H
#include <QObject>
#include "MissionData/standardmissiondata.h"
#include "vehicledatamanager.h"

class MissionHelper : public QObject
{
    Q_OBJECT
public:
    MissionHelper(StandardMissionData* standard, VehicleDataManager* vehicle);

    // All helper functions
    Q_INVOKABLE int timeToWaypoint(int waypointNo) const;       // in seconds
    Q_INVOKABLE int timeLeftForMission() const;                 // in seconds
    Q_INVOKABLE float speedAtWaypoint(int waypointNo) const;    // in m/s
    Q_INVOKABLE float totalWaypointDistance(int waypoint) const;
    Q_INVOKABLE float relativeWaypointDistance(double lat,double lon,int waypoint) const;
    Q_INVOKABLE float nextWaypointDistance(int waypoint) const;
    Q_INVOKABLE float previousWaypointDistance(int waypoint) const;

    // Convenience
    Q_INVOKABLE int timeToNextWaypoint() const;
    Q_INVOKABLE int timeBetweenWaypoints(int start, int end) const;
    Q_INVOKABLE int getPreviousWaypointOf(int start_waypoint) const;
    Q_INVOKABLE float headingToNextWaypoint() const;
    Q_INVOKABLE float headingBetweenWaypoints(int start, int end) const;
    Q_INVOKABLE int getFirstWaypoint() const;
    Q_INVOKABLE int getNextWaypointOf(int start_waypoint) const;
    Q_INVOKABLE float distanceBetweenWaypoints(int start, int end) const;
    Q_INVOKABLE int timeFromCurrentToWaypoint(int waypointNo) const;
    Q_INVOKABLE float climbRateFromWaypointToNext(int waypointNo) const;
    Q_INVOKABLE float climbFromWaypointToNext(int waypointNo) const;
    Q_INVOKABLE int timeBetweenPoints(double lat1, double lon1, double lat2, double lon2, float speed_at_start = 0.0, float speed_at_end = 0.0, float acceptanceRadius = 0.0) const;
    Q_INVOKABLE float distanceBetween(double lat1, double lon1, double lat2, double lon2) const;
private:
    bool _isAWaypoint(int waypointNo) const;

    int _getNextWaypoint();
    float _getHeadingBetweenPoints(double lat1, double lon1, double lat2, double lon2) const;


    StandardMissionData*    _standard;
    VehicleDataManager*     _vehicle;
};

#endif // MISSIONHELPER_H
