import QtQuick 2.0
import QtLocation 5.15
import QtGraphicalEffects 1.15
import QtPositioning 5.6
import QtQuick.Window 2.0
import QtQuick.Controls 2.0
import QtQuick 2.2
import QtQuick.Layouts 1.15
import NavEnums 1.0
import QtQml.Models 2.15

import "helper.js" as Helper
import "MapItems"

Map{
    id:mapOverlay
    function addWaypoints(){
        for (var i = 0; i < missionManager.standard.actualWaypointCount ; ++i){
            var w = missionManager.standard.get(i)

            _missionLine.addCoordinate(QtPositioning.coordinate(w.lat, w.lon))
            missionWaypoints.append({latitude: w.lat, longitude: w.lon, altitude: w.alt, number: w.seq+1, acceptanceRadius: w.acceptanceRadius, type: w.command })
            computeAltitudeArrows()
        }
    }
    function setupMiniMission(){
        var current =missionManager.standard.get_seq(missionManager.standard.getCurrentWaypoint())
        miniMissionLine.addCoordinate(QtPositioning.coordinate(current.lat,current.lon))
        var currentIndex = missionManager.standard.seqToIndex(missionManager.standard.getCurrentWaypoint());
        if(currentIndex+1<missionManager.standard.actualWaypointCount()){
            var next = missionManager.standard.get(currentIndex+1)
            miniMissionLine.addCoordinate(QtPositioning.coordinate(next.lat,next.lon))
        }
    }
    function syncMiniMissionLine(){
        for(var i=0;i<miniMissionWaypoints.count;i++){
            var coords = QtPositioning.coordinate(miniMissionWaypoints.get(i).latitude,miniMissionWaypoints.get(i).longitude)
            miniMissionLine.replaceCoordinate(i+1,coords)
        }
    }

    function endMiniMission(){
        mapHud.deviateState=Enums.NoMission
        miniMissionLine.setPath(QtPositioning.path())

        console.log(miniMissionLine.pathLength())
        miniMissionWaypoints.clear()
    }
    function moveMiniMissionStart(index){
        var waypoint =missionManager.standard.get_seq(index)
        if(waypoint===null)return
        var coords = QtPositioning.coordinate(waypoint.lat,waypoint.lon)
        miniMissionLine.replaceCoordinate(0,coords)
    }
    function moveMiniMissionEnd(index){
        var waypoint =missionManager.standard.get_seq(index)
        if(waypoint===null)return
        var coords = QtPositioning.coordinate(waypoint.lat,waypoint.lon)
        miniMissionLine.replaceCoordinate(miniMissionLine.path.length-1,coords)
    }
    function moveMiniMissionWaypoint(index,coords){
        mapOverlay.miniMissionWaypoints.get(index).longitude = coords.longitude
        mapOverlay.miniMissionWaypoints.get(index).latitude = coords.latitude
        miniMissionLine.replaceCoordinate(index+1,coords)
        miniMissionSelectionChanged(index)

    }

    function addMiniMissionWaypoint(coord){
        miniMissionWaypoints.append({latitude: coord.latitude, longitude: coord.longitude,altitude:0,speed:5,acceptanceRadius:100, number:miniMissionWaypoints.count+1})
        miniMissionLine.insertCoordinate(miniMissionLine.path.length-1,coord)
        //miniMissionLine.addCoordinate(coord)
        miniMissionSelectionChanged(miniMissionWaypoints.count-1)
    }
    function removeMiniMissionItem(index){
        if(miniMissionWaypoints.count<2)return
        miniMissionLine.removeCoordinate(index+1)
        miniMissionWaypoints.remove(index,1)
        for(var i=0;i<miniMissionWaypoints.count;i++){
            miniMissionWaypoints.get(i).number=i+1
        }
    }

    function computeAltitudeArrows() {
        if (missionWaypoints.count > 1) {

            // Only when there's more than 1 waypoint (we want to check between waypoints)
            var prev_obj = missionWaypoints.get(missionWaypoints.count - 2)
            var curr_obj = missionWaypoints.get(missionWaypoints.count - 1)
            var alt_diff = curr_obj.altitude - prev_obj.altitude

            if (Math.abs(alt_diff) < 0.01) { // no change
                return
            }

            var currentRelativeAltitude = 0.0
            var currentRelativeDistance = 0.0
            // For each meter of distance traveled, we climb this number of meters
            var climbRate = alt_diff / missionHelper.distanceBetweenWaypoints(prev_obj.number - 1, curr_obj.number - 1)

            // Determine comparison function & step if we're climbing or descending
            var step = 25
            var reachedAlt = function (alt, step, diff) {return (alt + step) < diff }
            if (alt_diff < 0) {
                step = -step
                reachedAlt = function (alt, step, diff) {return (alt + step) > diff }
            }

            var coordinates

            if (Math.abs(alt_diff) < Math.abs(step)) {
                coordinates = Helper.midPoint(prev_obj.latitude, prev_obj.longitude, curr_obj.latitude, curr_obj.longitude)
                // Under the step line, but still climbing/desc. We still want to add only one arrow in the middle in this case
                missionArrows.append({ latitude: coordinates.latitude, longitude: coordinates.longitude, isClimb: (alt_diff > 0), number: curr_obj.number })
            }
            else {
                while (reachedAlt(currentRelativeAltitude, step, alt_diff)) {

                    currentRelativeDistance += step / climbRate

                    // Draw the arrow at the coordinates at currentRelativeDistance distance from the initial waypoint
                    // First get the heading between the two point
                    var heading = missionHelper.headingBetweenWaypoints(prev_obj.number - 1, curr_obj.number - 1)

                    // Then get the coordinate at the distance to the calculated heading.
                    coordinates = Helper.coordinateDistanceHeading(prev_obj.latitude, prev_obj.longitude, currentRelativeDistance, heading)

                    missionArrows.append({ latitude: coordinates.latitude, longitude: coordinates.longitude, isClimb: (alt_diff > 0), number: curr_obj.number })

                    currentRelativeAltitude += step
                }
            }

        }
    }


    function addGeofences() {
        for (var i = 0; i < missionManager.fences.fenceCount() && i >= 0;) {
            var qmlString = ""
            var coordinates = missionManager.fences.getCoordinates(i)
            var qmlObject

            switch(missionManager.fences.getFenceType(i)) {
            case Enums.CircleInclusion:
                //console.log("Got circle inclusion")
                qmlString ='import QtLocation 5.15; MapCircle{
                            visible: mapOverlay.areGeoFencesVisible;z:5;center{latitude:%1; longitude:%2;}
                            border.width:2;color:"transparent";border.color:"red";
                            radius:%3}';
                qmlString = qmlString.arg(coordinates.latitude)
                qmlString = qmlString.arg(coordinates.longitude)
                qmlString = qmlString.arg(missionManager.fences.getParam(i, 1))
                break;
            case Enums.CircleExclusion:
                //console.log("Got circle exclusion")
                qmlString ='import QtLocation 5.15; MapCircle{
                            visible: mapOverlay.areGeoFencesVisible;z:5;center{latitude:%1; longitude:%2;}
                            border.width:2;color:Qt.rgba(1,0,0,0.3);border.color:"red";
                            radius:%3}';
                qmlString = qmlString.arg(coordinates.latitude)
                qmlString = qmlString.arg(coordinates.longitude)
                qmlString = qmlString.arg(missionManager.fences.getParam(i, 1))
                break;

            case Enums.PolygonVertexInclusion:
                //console.log("Got poly inclusion")
                qmlString = 'import QtLocation 5.15; MapPolygon{
                             visible: mapOverlay.areGeoFencesVisible;z:5;path: [%1];color:Qt.rgba(1,0,0,0); border.color:"red";
                             border.width:2}'

                qmlString = qmlString.arg(getPolygonFormattedPath(i))
                break;

            case Enums.PolygonVertexExclusion:
                //console.log("Got poly exclusion")
                qmlString = 'import QtLocation 5.15; MapPolygon{
                             visible: mapOverlay.areGeoFencesVisible;z:5;path: [%1];color:Qt.rgba(1,0,0,0.3); border.color:"red";
                             border.width:2}'

                qmlString = qmlString.arg(getPolygonFormattedPath(i))
                break;
            case Enums.ReturnPoint:
                //console.log("Got return point")
                qmlString ='import QtLocation 5.15; import QtQuick.Controls 2.0;  MapCircle{
                            visible: mapOverlay.areGeoFencesVisible;z:5;center{latitude:%1; longitude:%2;}
                            color:"cyan";
                            radius:5;
                            }';
                // WE ALSO NEED TO ADD A LABEL CENTERED ON IT SAYING "B"
                qmlString = qmlString.arg(coordinates.latitude)
                qmlString = qmlString.arg(coordinates.longitude)
                break;
            default:
                break;
            }

            qmlObject = Qt.createQmlObject(qmlString, mapOverlay)
            missionFences.push(qmlObject)
            mapOverlay.addMapItem(qmlObject)

            // Calculate the start index of the next fence.
            // Needed espacially for polygons because a polygon fence
            // is composed of multiple items in the list.
            i = missionManager.fences.getNextFenceIndex(i)
            if (i < 0) {
                i = missionManager.fences.fenceCount()
            }
        }
    }

    function getPolygonFormattedPath(i) {
        var polyPath = missionManager.fences.getPolygonPath(i)

        // To workaround the qgeocoordinate toString, we format the QGeoCoordinate by hand
        var formattedPolyPath = []
        for (var j = 0; j < polyPath.length; ++j) {
            formattedPolyPath.push('{latitude: %1, longitude: %2}'.arg(polyPath[j].latitude).arg(polyPath[j].longitude))
        }
        return formattedPolyPath
    }

    function addRallyPoints() {
        //console.log("Should have the rallypoints")
        var numRallyPoints = missionManager.rallyPoints.rallyPointCount()
        for (var i = 0; i < numRallyPoints; ++i) {
            var coord = missionManager.rallyPoints.getCoordinates(i)
            missionRallyPoints.append({ latitude: coord.latitude, longitude: coord.longitude })
        }

    }

    function removeWaypoints(){
        var start = missionWaypoints.count-1;
        for (var i = 0; i < missionWaypoints.count; ++i) {
            _missionLine.removeCoordinate(start--)
        }
        missionArrows.clear()
        missionWaypoints.clear()
        missionWaypointsNew.clear()
    }

    function removeGeofences() {
        missionFences.forEach(fence => mapOverlay.removeMapItem(fence))
    }

    function removeRallyPoints() {
        missionRallyPoints.clear()
    }

    function refreshMissionItems() {
        if (missionActive) {
            removeWaypoints()
            removeGeofences()
            removeRallyPoints()
            addWaypoints()
            addGeofences()
            addRallyPoints()

            mapHud.generateAltitudeGraph()
        }
    }

    function removeMissionItems() {
        removeWaypoints()
        removeGeofences()
        removeRallyPoints()
    }

    function getVelocityVector() {
        var ve              = vehicleManager.eastVel() * 0.3048
        var vn              = vehicleManager.northVel() * 0.3048

        var groundSpeed     = vehicleManager.groundSpeed()
        var groundSpeedKts  = groundSpeed * 1.94384
        var numberOfFeet    = groundSpeedKts * 10.0
        var d               = numberOfFeet * 0.3048 / 1000.0 // In kilometers

        var heading         = vehicleManager.heading() * (Math.PI / 180.0) // Should be in radians
        var speedDirection  = Math.atan2(ve, vn)
        var R               = 6378.1 // Radius of the earth

        var lat1            = vehicleManager.lat() * (Math.PI / 180.0)
        var lon1            = vehicleManager.lon() * (Math.PI / 180.0)
        // Reference for the following calculations : https://stackoverflow.com/questions/7222382/get-lat-long-given-current-point-distance-and-bearing
        var lat2 = Math.asin( Math.sin(lat1)*Math.cos(d/R) + Math.cos(lat1)*Math.sin(d/R)*Math.cos(speedDirection));
        var lon2 = lon1 + Math.atan2(Math.sin(speedDirection)*Math.sin(d/R)*Math.cos(lat1), Math.cos(d/R)-Math.sin(lat1)*Math.sin(lat2));

        lat2 = lat2 / (Math.PI / 180.0)
        lon2 = lon2 / (Math.PI / 180.0)
        return [{ latitude: vehicleManager.lat(), longitude: vehicleManager.lon() }, { latitude: lat2, longitude: lon2 }]
    }


    anchors.fill: parent
    plugin:Plugin{name: "itemsoverlay"}
    gesture.enabled: false
    center:mainMap.center
    color:"transparent"
    zoomLevel: mainMap.zoomLevel
    bearing: mainMap.bearing
    minimumFieldOfView: mainMap.minimumFieldOfView
    maximumFieldOfView: mainMap.maximumFieldOfView
    minimumTilt: mainMap.minimumTilt
    maximumTilt: mainMap.maximumTilt
    minimumZoomLevel: mainMap.minimumZoomLevel
    maximumZoomLevel: mainMap.maximumZoomLevel
    tilt: mainMap.tilt
    fieldOfView: mainMap.fieldOfView
    z: mainMap.z + 1


    // Additionnal code to enable SSAA
    layer.enabled: true
    layer.smooth: true
    property int w: width
    property int h: height
    property int pr: Screen.devicePixelRatio
    layer.textureSize: Qt.size(w*2*pr, h*2*pr)

    //Other properties
    property color aircraftColor: navSettings.aircraftColor
    property int defaultWaypointRadius: navSettings.defaultWaypointRadius
    property var missionLine: null
    property var missionFences: []
    property bool _currentMissionLineVisible: navSettings.currentMissionLineVisible
    property bool missionActive: navDisplay.defaultWaypointsActive
    property alias missionWaypoints: missionWaypoints
    property alias missionRallyPoints: missionRallyPoints
    property alias miniMissionWaypoints: miniMissionWaypoints
    property var oldMissionLinePath: []
    property bool isTrajectoryLineVisible: navSettings.isTrajectoryLineVisible && allLayersAreVisible
    property bool areGeoFencesVisible: allLayersAreVisible
    property bool allLayersAreVisible: true
    property real voxelOpacity: 1.0

    property color missionLineColor: navSettings.missionLineColor

    signal miniMissionSelectionChanged(int index)

    Component.onCompleted: {
        voxelMap.checkZoomLevel(mapOverlay.zoomLevel)
    }

    ListModel {
        id: missionWaypoints
    }

    ListModel {
        id: miniMissionWaypoints
    }

    ListModel {
        id: missionWaypointsNew
    }

    ListModel {
        id: missionRallyPoints
    }

    ListModel {
        id: missionArrows
    }

    Layers {
        _map: mapOverlay
        z: 1
    }

    //Items
    MapPolyline {
        id: velocityLine
        z: 10
        line.width: 3
        line.color: "white"
        visible: true // Might want to add a settings to toggle visibility of the vel line
        Connections{
            target: timer
            function onTriggered(){
                velocityLine.path = mapOverlay.getVelocityVector()
            }
        }
    }

    // Mission line
    MapPolyline {
        id: _missionLine
        z: 10
        line.width: 3
        line.color: mapOverlay.missionLineColor
        path: []
    }
    // Mini Mission line
    MapPolyline {
        id: miniMissionLine
        line.width: 3
        line.color: "yellow"
        path: []
    }

    // Current Leg Line
    MapPolyline {
        id: _currentMissionLine
        z: 10
        line.width: 3
        line.color: navSettings.currentMissionLineColor
        visible: mapOverlay._currentMissionLineVisible
        path: []
    }

    // Old Leg Line
    MapPolyline {
        id: _oldMissionLine
        z: 10
        line.width: 3
        line.color: navSettings.oldMissionLineColor
        visible: true
        path: oldMissionLinePath
    }

    // Add the climb/desc arrows on the map
    MapItemView {
        z: 10
        model: missionArrows
        delegate: MapQuickItem {
            id: _itemRoot
            property var sourceImage: isClimb === true ? "qrc:/imgs/imgs/small_up_arrow.png" : "qrc:/imgs/imgs/small_down_arrow.png"
            property var _number: number
            anchorPoint.x: img.width/2
            anchorPoint.y: img.height/2
            coordinate {
                latitude: latitude;
                longitude: longitude;
            }
            sourceItem: Image {
                id: img
                width: 35
                height: width
                source: sourceImage

                ColorOverlay {
                    source: parent
                    anchors.fill: parent
                    color: paletteHolder.climbArrow
                }
            }

            Connections{
                target: mainMap
                function onZoomLevelChanged(){
                    _itemRoot.visible = mapHud.calculateScale() < 1000
                }
            }
        }
    }

    // Previous Trajectory Line
    MapPolyline {
        id: _trajectoryLine
        z: 10
        line.width: 2
        line.color: "white"
        visible: mapOverlay.isTrajectoryLineVisible
        path: []

        Connections {
            target: timer
            function onFullSecond() {
                _trajectoryLine.addCoordinate(QtPositioning.coordinate(vehicleManager.lat(), vehicleManager.lon(), vehicleManager.alt()))
            }
        }
    }

    // Waypoint radius
    MapItemView {
        z: 10
        model: missionWaypoints
        delegate: MapCircle {
            radius: acceptanceRadius === 0 ? defaultWaypointRadius : acceptanceRadius
            color: "transparent";
            border.width: 2
            border.color: acceptanceColor
            center {
                latitude: latitude;
                longitude: longitude;
            }

            property var currentWaypoint: missionManager.standard.getCurrentWaypoint()
            property var acceptanceColor: !(number >= currentWaypoint+1) || (navSettings.radiusOnCurrentOnly && !(currentWaypoint+1 === number)) ? "transparent" : (type === Enums.Land ? navSettings.currentMissionLineColor : mapOverlay.missionLineColor)

            Connections {
                target: missionManager

                function onNewMissionCurrent(current) {
                    currentWaypoint = missionManager.standard.getCurrentWaypoint()
                }
            }
        }
    }

    // Waypoints
    MapItemView {
        z: 10
        model: missionWaypoints
        delegate: WaypointIndicator { }
    }

    MapItemView {
        anchors.fill: parent
        model: miniMissionWaypoints
        delegate: MiniMissionWaypointIndicator { }
    }
    // mini waypoint radius
    MapItemView {
        z: 10
        model: miniMissionWaypoints
        delegate: MiniMissionWaypointCircle{ }
    }

    // Rally Points
    MapItemView {
        z: 10
        model: missionRallyPoints
        delegate: MapQuickItem {
            anchorPoint.x: circle.width/2
            anchorPoint.y: circle.height/2
            coordinate {
                latitude: latitude;
                longitude: longitude;
            }

            sourceItem: Rectangle {
                id: circle
                width: 15
                height: width
                radius: width*0.5
                border.width: 0
                color: "cyan"
                Text {
                    anchors.centerIn: parent
                    color: "black"
                    text: "R"
                }
            }
        }
    }

    MapItemView {
        model: voxelMap.voxels
        delegate: MapPolygon {
            opacity: mapOverlay.voxelOpacity
            color: Qt.rgba(142/255, 150/255, 68/255, 1)
            geoShape: model.get
            border.width: 0
        }
        Connections {
            target: mapOverlay
            function onZoomLevelChanged(zl) {
                voxelMap.checkZoomLevel(zl)
                handlePerspectiveChange()
            }

            function onCenterChanged() {
                handlePerspectiveChange()
            }

            function handlePerspectiveChange() {
                var topLeft = mainMap.toCoordinate(Qt.point(0, 0))
                var botRight = mainMap.toCoordinate(Qt.point(navSettings.windowWidth, navSettings.windowHeight))

                voxelMap.checkPosition(topLeft.latitude, topLeft.longitude, botRight.latitude, botRight.longitude)
            }
        }
    }

    // Point appearing on right click
    // This is tightly linked to MapInfoPoint
    MapQuickItem {
        id: contextPoint
        z: 10
        anchorPoint.x: pointCircle.width / 2
        anchorPoint.y: pointCircle.height / 2
        coordinate {
            latitude: lat;
            longitude: lon;
        }
        visible: mapInfoPoint.isInfoVisible

        property real lat: 0
        property real lon: 0

        sourceItem: Rectangle {
            id: pointCircle
            width: 15
            height: width
            radius: width * 0.5
            border.width: 0
            color: "orange"

            Text {
                anchors.centerIn: parent
                color: "black"
                text: "P"
            }
        }
    }

    property alias aircraft: aircraft
    Aircraft{
        id:aircraft
        z: 1000
        function updatePosition(){
            aircraft.coordinate = QtPositioning.coordinate(vehicleManager.lat(),vehicleManager.lon())
            aircraft.rotation = vehicleManager.heading() - mainMap.bearing
        }
        Connections{
            target: timer
            function onTriggered(){aircraft.updatePosition()}
        }

    }

    RangeRings {
        coords: aircraft.coordinate
        color: paletteHolder.rangeRings
        lineWidth: 1
        z: 11
    }

    MapInfoPoint {
        id: mapInfoPoint
        z: 999
    }


    Connections {
        target: missionManager
        function onNewMissionAvailable() {

            mapOverlay.refreshMissionItems()

        }

        function onNewMissionCurrent(currentNumber) {

            console.log("New Current")

            var previous = missionHelper.getPreviousWaypointOf(currentNumber)

            if (previous >= 0) {
                mapOverlay.oldMissionLinePath = getOldMissionLinePath(previous);
                // Remove the current leg line if there's one
                // If index is invalid, does nothing (so if there's no current line, there's no problem)
                _currentMissionLine.removeCoordinate(1)
                _currentMissionLine.removeCoordinate(0)

                // We need to add the current leg line
                var prev_coord = missionManager.standard.getCoordinates(previous)
                var curr_coord = missionManager.standard.getCoordinates(currentNumber)
                _currentMissionLine.addCoordinate(prev_coord)
                _currentMissionLine.addCoordinate(curr_coord)
            }
            else {
                // Remove the current leg line if there's one
                _currentMissionLine.removeCoordinate(1)
                _currentMissionLine.removeCoordinate(0)
            }
        }

        function getOldMissionLinePath(last) {
            var path = []
            var first = missionHelper.getFirstWaypoint()

            while (first <= last) {
                path.push(missionManager.standard.getCoordinates(first))
                first = missionHelper.getNextWaypointOf(first)
            }

            return path
        }
    }
}
