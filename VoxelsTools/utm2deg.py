from lib2to3.pytree import convert
import utm
from pathlib import Path
import pathlib

OUTPUT_DIR = Path("output")
files_to_parse = [ "FRL_DSM_objects_10_10_5.csv", "FRL_DSM_ground_10_10_1.csv", "FRL_DSM_ground_20_20_1.csv", "FRL_DSM_objects_20_20_10.csv" ]

zone_number = utm.latlon_to_zone_number(45.421532, -75.697189)  # Ottawa reference lat/lons
zone_letter = utm.latitude_to_zone_letter(45.421532)            #

for filename in files_to_parse:
    output_lines = []
    output_filename = "Parsed_" + filename
    output_file = open(OUTPUT_DIR / output_filename, "w")
    file = open(filename, "r")
    lines = file.readlines()

    for line in lines:
        cols = line.replace("\n", "").split(",") # Strip the newline char and comma
        
        assert(len(cols) == 3)

        lat, lon = utm.to_latlon(float(cols[0]), float(cols[1]), zone_number, zone_letter)

        convertedLine = str(lat) + "," + str(lon) + "," + cols[2] + "\n"
        output_lines.append(convertedLine)
    


    output_file.writelines(output_lines)

                