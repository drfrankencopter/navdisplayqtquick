#include "missionhelper.h"
#include "enums.h"
#include <QDebug>

//#define DEBUG_HELPER

static constexpr double CONSTANTS_RADIUS_OF_EARTH = 6371000; // meters (m)
static constexpr double EPSILON = 0.001;
static constexpr double DEFAULT_SPEED = 5.0; // m/s

template<typename Type>
Type wrap(Type x, Type low, Type high) {
    // already in range
    if (low <= x && x < high) {
        return x;
    }

    const Type range = high - low;
    const Type inv_range = Type(1) / range; // should evaluate at compile time, multiplies below at runtime
    const Type num_wraps = (Type) floor((x - low) * inv_range);
    return x - range * num_wraps;
}

/**
 * Wrap value in range [-π, π)
 */
template<typename Type>
Type wrap_pi(Type x)
{
    return wrap(x, Type(-M_PI), Type(M_PI));
}

MissionHelper::MissionHelper(StandardMissionData* standard, VehicleDataManager* vehicle)
    : _standard(standard),
      _vehicle(vehicle)
{
}

int MissionHelper::timeToWaypoint(int waypointNo) const {

#ifdef DEBUG_HELPER
    qDebug() << "timeToWaypoint " << waypointNo;
#endif
    if (_standard->getMissionNumber() < 1) {
        return 0;
    }

    if (!_isAWaypoint(waypointNo)) {
        qDebug() << "Can't compute time to a non-waypoint item " << waypointNo;
        return -1;
    }

    double lat1 = _vehicle->lat();
    double lon1 = _vehicle->lon();
    double lat2 = _standard->waypointLat(waypointNo);
    double lon2 = _standard->waypointLon(waypointNo);

    double speed_at_start = _vehicle->groundSpeed();
    double speed_at_end   = speedAtWaypoint(waypointNo);

    float acceptanceRadius = _standard->waypointAcceptanceRadius(waypointNo);

    return timeBetweenPoints(lat1, lon1, lat2, lon2, speed_at_start, speed_at_end, acceptanceRadius);
}

int MissionHelper::timeLeftForMission() const {
#ifdef DEBUG_HELPER
    qDebug() << "timeLeftForMission ";
#endif
    if (_standard->getMissionNumber() < 1) {
        return 0;
    }

    int total_time = 0;
    total_time += timeToNextWaypoint();

    int prec = _standard->getCurrentWaypoint();
    int next = getNextWaypointOf(prec);

    while (next > 0) {
        total_time += timeBetweenWaypoints(prec, next);
        prec = next;
        next = getNextWaypointOf(next);
    }

    return total_time;
}

float MissionHelper::speedAtWaypoint(int waypointNo) const {
#ifdef DEBUG_HELPER
    qDebug() << "speedAtWaypoint " << waypointNo;
#endif
    if (_standard->getMissionNumber() < 1) {
        return -1.0;
    }

    if ((waypointNo < 0) || (waypointNo > _standard->waypointCount() - 1)) {
        qDebug() << "speedAtMissionItemNo received request for mission item outside of mission bounds";
        return -1.0;
    }

    // Verify it's a waypoint (not a speed change, or altitude change, etc.)
    if (!_isAWaypoint(waypointNo)) {
        qDebug() << "Can't compute speed at mission item "<<waypointNo<<" because it's not a waypoint\n";
        return -1.0;
    }

    float commandedSpeed = DEFAULT_SPEED;	// Start by assuming the default speed defined in NrcDefaults
                                            // in case we don't find any defined speed.

    for (int i = 0; i < waypointNo; ++i) {
        switch (_standard->waypointType(i)) {
        case MAV_CMD_NAV_TAKEOFF:
            // According to our NRC ICD if param 1 is set, then it defines the speed to get there
            if (_standard->getParam(i, 1) > 0) {
                commandedSpeed = _standard->getParam(i, 1);
            }
            break;
        case MAV_CMD_DO_CHANGE_SPEED:
            //Primary means of changing speed
            if (_standard->getParam(i, 2) != -1) { // According to the ICS -1 means no speed change
                commandedSpeed = _standard->getParam(i, 2);
            }
            break;
        case MAV_CMD_DO_ORBIT:
            commandedSpeed = _standard->getParam(i, 2); // Commanded tangential speed
            break;
        case MAV_CMD_DO_REPOSITION:
            if (_standard->getParam(i, 1) >= 0) {
                commandedSpeed = _standard->getParam(i, 1);
            }
            break;
        // No default required as these are all the known commands that adjust speed
        }
    }

    return commandedSpeed;
}

float MissionHelper::totalWaypointDistance(int waypoint) const
{
#ifdef DEBUG_HELPER
    qDebug() << "totalWaypointDistance " << waypoint;
#endif
    float distance =0;
    for(int i =1; i<=waypoint;i++){
        auto prev = _standard->get(i-1);
        auto current = _standard->get(i);
        distance+=distanceBetween(current->lat(),current->lon(),prev->lat(),prev->lon());
    }
    return distance;
}

float MissionHelper::relativeWaypointDistance(double lat, double lon, int waypoint) const
{
#ifdef DEBUG_HELPER
    qDebug() << "relativeWaypointDistance " << waypoint;
#endif
    return distanceBetween(lat,lon,_standard->waypointLat(waypoint),_standard->waypointLon(waypoint));
}

float MissionHelper::nextWaypointDistance(int waypoint) const
{
#ifdef DEBUG_HELPER
    qDebug() << "nextWaypointDistance " << waypoint;
#endif
    int next = getNextWaypointOf(waypoint);
    return distanceBetween(_standard->waypointLat(waypoint),_standard->waypointLon(waypoint),_standard->waypointLat(next),_standard->waypointLon(next));
}

float MissionHelper::previousWaypointDistance(int waypoint) const
{
#ifdef DEBUG_HELPER
    qDebug() << "previousWaypointDistance";
#endif
    int prev = getPreviousWaypointOf(waypoint);
    return distanceBetween(_standard->waypointLat(waypoint),_standard->waypointLon(waypoint),_standard->waypointLat(prev),_standard->waypointLon(prev));
}



int MissionHelper::timeToNextWaypoint() const {
#ifdef DEBUG_HELPER
    qDebug() << "timeToNextWaypoint ";
#endif
    int current = _standard->getCurrentWaypoint();
    if (current < 0) {
        return std::numeric_limits<int>::quiet_NaN();
    }

    return timeToWaypoint(current);
}

bool MissionHelper::_isAWaypoint(int waypointNo) const {
#ifdef DEBUG_HELPER
    qDebug() << "isAWaypoint " << waypointNo;
#endif
    return _standard->waypointType(waypointNo) == Enums::WAYPOINT_TYPE::Waypoint
            || _standard->waypointType(waypointNo) == Enums::WAYPOINT_TYPE::Takeoff
            || _standard->waypointType(waypointNo) == Enums::WAYPOINT_TYPE::Land;
}

float MissionHelper::distanceBetween(double lat1, double lon1, double lat2, double lon2) const {
#ifdef DEBUG_HELPER
    qDebug() << "_distanceBetween ";
#endif
    const double lat_now_rad = (M_PI / 180.0)*(lat1);
    const double lat_next_rad = (M_PI / 180.0)*(lat2);

    const double d_lat = lat_next_rad - lat_now_rad;
    const double d_lon = (M_PI / 180.0)*(lon2) -(M_PI / 180.0)*(lon1);

    const double a = sin(d_lat / 2.0) * sin(d_lat / 2.0) + sin(d_lon / 2.0) * sin(d_lon / 2.0) * cos(lat_now_rad) * cos(lat_next_rad);

    const double c = atan2(sqrt(a), sqrt(1.0 - a));

    float dist = (CONSTANTS_RADIUS_OF_EARTH * 2.0 * c);

    return static_cast<float>(dist); // Meters
}

int MissionHelper::getPreviousWaypointOf(int start_waypoint) const {

#ifdef DEBUG_HELPER
    qDebug() << "getPreviousWaypointOf " << start_waypoint;
#endif

    int previousWaypoint = start_waypoint;

    while (--previousWaypoint >= 0 && !_isAWaypoint(previousWaypoint))
    {
    }

    if (previousWaypoint < 0 || !_isAWaypoint(previousWaypoint)) {
        // There's no previous point
        return -1;
    }

    assert(previousWaypoint < static_cast<int>(_standard->waypointCount()));
    assert(previousWaypoint >= 0);
    return previousWaypoint;
}

float MissionHelper::headingToNextWaypoint() const {
#ifdef DEBUG_HELPER
    qDebug() << "headingToNextWaypoint";
#endif
    double lat1 = _vehicle->lat();
    double lon1 = _vehicle->lon();

    int current = _standard->getCurrentWaypoint();
    QGeoCoordinate currentCoords = _standard->getCoordinates(current);
    double lat2 = currentCoords.latitude();
    double lon2 = currentCoords.longitude();

    return _getHeadingBetweenPoints(lat1, lon1, lat2, lon2);
}

float MissionHelper::headingBetweenWaypoints(int start, int end) const {

#ifdef DEBUG_HELPER
    qDebug() << "headingBetweenWaypoints ";
#endif

    if (!_isAWaypoint(start)) {
        qDebug() << "Can't compute heading to a non-waypoint item " << start;
        return -1;
    }

    if (!_isAWaypoint(end)) {
        qDebug() << "Can't compute heading to a non-waypoint item " << end;
        return -1;
    }

    double lat1 = _standard->waypointLat(start);
    double lon1 = _standard->waypointLon(start);
    double lat2 = _standard->waypointLat(end);
    double lon2 = _standard->waypointLon(end);

    return _getHeadingBetweenPoints(lat1, lon1, lat2, lon2);
}

float MissionHelper::_getHeadingBetweenPoints(double lat1, double lon1, double lat2, double lon2) const {

#ifdef DEBUG_HELPER
    qDebug() << "_getHeadingBetweenPoints";
#endif

    const double lat_now_rad = (M_PI / 180.0)*(lat1);
    const double lat_next_rad = (M_PI / 180.0)*(lat2);
    const double cos_lat_next = cos(lat_next_rad);
    const double d_lon = (M_PI / 180.0)*(lon2 - lon1);

    /* conscious mix of double and float trig function to maximize speed and efficiency */
    const float y = static_cast<float>(sin(d_lon) * cos_lat_next);
    const float x = static_cast<float>(cos(lat_now_rad) * sin(lat_next_rad) - sin(lat_now_rad) * cos_lat_next * cos(d_lon));
    double brng_rad = wrap_pi(atan2f(y, x));

    // Conversion stuff to degrees

    float brng_deg = brng_rad * (180.0 / M_PI);

    if (brng_deg < 0) {
        brng_deg = 360 - fabs(brng_deg);
    }

    return brng_deg;
}

int MissionHelper::getFirstWaypoint() const {

#ifdef DEBUG_HELPER
    qDebug() << "getFirstWaypoint";
#endif

    int i = -1;

    while (!_isAWaypoint(++i))
    {}

    return i;
}

int MissionHelper::_getNextWaypoint() {
#ifdef DEBUG_HELPER
    qDebug() << "_getNextWaypoint";
#endif
    int currentWaypoint = _standard->getCurrentWaypoint();
    return getNextWaypointOf(currentWaypoint);
}

int MissionHelper::getNextWaypointOf(int start_waypoint) const {
#ifdef DEBUG_HELPER
    qDebug() << "getNextWaypointOf " << start_waypoint;
#endif
    int nextWaypoint = start_waypoint;

    if (nextWaypoint < 0) {
        return -1;
    }

    while (++nextWaypoint < static_cast<int>(_standard->waypointCount()) && !_isAWaypoint(nextWaypoint))
    {
    }

    if (nextWaypoint >= static_cast<int>(_standard->waypointCount()))
        return -1;

    assert(static_cast<int>(nextWaypoint) < static_cast<int>(_standard->waypointCount()));
    return nextWaypoint;
}

float MissionHelper::distanceBetweenWaypoints(int start, int end) const {
#ifdef DEBUG_HELPER
    qDebug() << "distanceBetweenWaypoints " << start << " and " << end;
#endif
    if (!_isAWaypoint(start)) {
        qDebug() << "Can't compute distance to a non-waypoint item " << start;
        return -1;
    }

    if (!_isAWaypoint(end)) {
        qDebug() << "Can't compute distance to a non-waypoint item " << end;
        return -1;
    }

    double lat1 = _standard->waypointLat(start);
    double lon1 = _standard->waypointLon(start);
    double lat2 = _standard->waypointLat(end);
    double lon2 = _standard->waypointLon(end);

    return distanceBetween(lat1, lon1, lat2, lon2);
}

int MissionHelper::timeFromCurrentToWaypoint(int waypointNo) const {
#ifdef DEBUG_HELPER
    qDebug() << "timeFromCurrentToWaypoint " << waypointNo;
#endif
    if (!_isAWaypoint(waypointNo)) {
        qDebug() << "Can't compute timeFromCurrentToWaypoint to a non-waypoint item " << waypointNo;
        return -1;
    }

    int total_time = 0.0;
    int prec = _standard->getCurrentWaypoint();
    int next = getNextWaypointOf(prec);
    while (next > 0 && next < _standard->waypointCount() && next < waypointNo) {
        total_time += timeBetweenWaypoints(prec, next);
        prec = next;
        next = getNextWaypointOf(prec);
    }
    assert(total_time >= 0);
    return total_time;
}

float MissionHelper::climbRateFromWaypointToNext(int waypointNo) const {

#ifdef DEBUG_HELPER
    qDebug() << "climbRateFromWaypointToNext " << waypointNo;
#endif

    if (!_isAWaypoint(waypointNo)) {
        qDebug() << "Can't compute climbRateFromWaypointToNext to a non-waypoint item " << waypointNo;
        return -1;
    }

    int next = getNextWaypointOf(waypointNo);
    if (next < 0) {
        return -1;
    }

    int alt_diff = _standard->waypointAlt(next) - _standard->waypointAlt(waypointNo);
    int timeBetween = timeBetweenWaypoints(waypointNo, next);
    if (timeBetween <= 0) {
        return 0;
    }

    return alt_diff / timeBetween; // m/s of climb
}

float MissionHelper::climbFromWaypointToNext(int waypointNo) const
{
    if (!_isAWaypoint(waypointNo)) {
        qDebug() << "Can't compute climbRateFromWaypointToNext to a non-waypoint item " << waypointNo;
        return -1;
    }

    int next = getNextWaypointOf(waypointNo);
    if (next < 0) {
        return -1;
    }

    int alt_diff = _standard->waypointAlt(next) - _standard->waypointAlt(waypointNo);

    return alt_diff;
}

int MissionHelper::timeBetweenWaypoints(int start, int end) const {

#ifdef DEBUG_HELPER
    qDebug() << "timeBetweenWaypoints " << start << " and " << end;
#endif

    if (_standard->getMissionNumber() < 1) {
        return 0;
    }

    // Make sure it's a waypoint
    if (!_isAWaypoint(start)) {
        qDebug() << "Can't compute time to a non-waypoint item " << start;
        return -1;
    }

    if (!_isAWaypoint(end)) {
        qDebug() << "Can't compute time to a non-waypoint item " << end;
        return -1;
    }

    double lat1 = _standard->waypointLat(start);
    double lon1 = _standard->waypointLon(start);
    double lat2 = _standard->waypointLat(end);
    double lon2 = _standard->waypointLon(end);

    double speed_at_start = speedAtWaypoint(start);
    double speed_at_end   = speedAtWaypoint(end);

    float acceptanceRadius = _standard->waypointAcceptanceRadius(end);

    return timeBetweenPoints(lat1, lon1, lat2, lon2, speed_at_start, speed_at_end, acceptanceRadius);
}

int MissionHelper::timeBetweenPoints(double lat1, double lon1, double lat2, double lon2, float speed_at_start, float speed_at_end, float acceptanceRadius) const {

#ifdef DEBUG_HELPER
    qDebug() << "timeBetweenPoints";
#endif

    // This function is a copy-pasta of the timeToNextWaypoint.
    // TODO : Please refactor so we reuse the same code
    float distance_between = distanceBetween(lat1, lon1, lat2, lon2);
    float distance_in_meters = distance_between - acceptanceRadius;

    if (distance_in_meters <= 0) {
        // We are already at the waypoint
        return 0.0;
    }

    float delta_time_in_sec;
    if (fabs(speed_at_end - speed_at_start) < EPSILON) {
        // Speed is constant
        // time = s / v
        double speed_avg = (speed_at_end + speed_at_start) / 2.0;
        delta_time_in_sec = distance_in_meters / speed_avg;
    }
    else {
        // We are accelerating or decelerating
        // We assume the acceleration is constant between the two waypoints
        // double a = (v^2 - u^2) / 2 * s
        float acceleration_in_m_s = ((speed_at_end * speed_at_end) - (speed_at_start * speed_at_start)) / (2.0 * distance_in_meters);
        delta_time_in_sec = (speed_at_end - speed_at_start) / acceleration_in_m_s;
    }

    if (delta_time_in_sec < 0)
        delta_time_in_sec = 0.0;



    assert(delta_time_in_sec >= 0);
    return delta_time_in_sec;
}

