Last updated Mar 21 2022

Items not necessarily in order of importance

[Done]1. Handle mission items that don't have lat/lon values
    switch (specificMission->items[mission_data->myCurrentMission.currentMissionItem].command)
        and look for:
    case MAV_CMD_NAV_WAYPOINT:
    case MAV_CMD_NAV_TAKEOFF:
    case MAV_CMD_NAV_LAND:
[Done]2. Waypoint radius should be based on the appropriate parameter in the mission items
    we use a certain parameter to define the Waypoint radius in meters...but have a default as a fallback
    #define DEFAULT_ACCEPTANCE_RADIUS_HORZ 30.0
    float horzAcceptDist = DEFAULT_ACCEPTANCE_RADIUS_HORZ;
    For MAV_CMD_NAV_WAYPOINT and MAV_CMD_NAV_TAKEOFF it's param2
    if (specificMission->items[mission_data->myCurrentMission.currentMissionItem].param2>0.0) {
        horzAcceptDist = specificMission->items[mission_data->myCurrentMission.currentMissionItem].param2;
    }
    for MAV_CMD_NAV_LAND its param3
    if (specificMission->items[mission_data->myCurrentMission.currentMissionItem].param3 > 0.0) {
        horzAcceptDist = specificMission->items[mission_data->myCurrentMission.currentMissionItem].param3;
    }
[Done]3.The helicopter location should be in the bottom 1/3rd of the display so it displays more of what's ahead of you than what is behind
[Done]4. Mission lines should be in Magenta
5. Geofences should be handled/displayed red fill for inclusions and red line for exclusions
[DOne]6. Helicopter icon should have colour options (at a minimum white would be nice for consistency)
Ask Kris 7. Helicopter Icon should have some minimum scale...i.e. it should never display itself as smaller than the actual helicopter.
    And ideally should look like a Bell 412. QGC and Nav Dsiplay should use the same SVG here.
[Done]8. UI menus are very broken...at least on KE's system. A workable menu structure should be established.
[Done]9. Investigate accuracy of positioning on map. Where is the reference point for the Helo Icon?
[Done]10. May need to custom colour scale legend based on map loaded. E.g. On dark map its almost impossible to read the blue scale
11. Will need text display areas for data like time/dist to next WP etc
[Kinda Done]12. A heading Arc should be added. I have some OpenGl code from BOSS if it's possible to include that as an overlay
[Fix]13. Altitude display
14. Serialization (QSettings)
15. Change sprite colors
16. Change compass to bar compass
17. Auto zoom. Zoom based on distance away from closest waypoint.
18.Show current altitude