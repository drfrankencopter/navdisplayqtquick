#include <QObject>
#include "mavNrc/frl_file_types.h"
#include "vehiclememorythread.h"

#ifdef _WIN32
#include <windows.h>
#else

#endif
#ifndef VEHICLEDATAMANAGER_H
#define VEHICLEDATAMANAGER_H

// The Vehicle Data Manager is a thread that connects to the shared memory of the vehicle's state and data.
// This way we can display the vehicle's position, heading, current speed, etc.

class VehicleDataManager : public QObject
{
    Q_OBJECT
public:
    VehicleDataManager();

    Q_INVOKABLE double  lat()           const;
    Q_INVOKABLE double  lon()           const;
    Q_INVOKABLE float   alt()           const;
    Q_INVOKABLE float   heading()       const;
    Q_INVOKABLE float   eastVel()       const;
    Q_INVOKABLE float   northVel()      const;
    Q_INVOKABLE float   verticalVel()   const;
    Q_INVOKABLE float   groundSpeed()   const;
    Q_INVOKABLE bool    engageStatus()  const;
    Q_INVOKABLE int     engageNumber()  const;

public slots:
    void handleEngageChanged(bool engageStatus);
    void handleEngageNumberChanged(unsigned int engageNumber);
    void handleMinimizeSwitchChanged(bool isMinimized);
signals:
    void engageStatusChanged(bool engageStatus);
    void engageNumberChanged(unsigned int engageNumber);
    void minimizeSwitchChanged(bool isMinimized);
private:
    bool _connectToSharedMemory();

#ifdef _WIN32
    HANDLE _hMapFile;
#else
    int _hMapFile;
#endif
    bool _isSharedMemoryConnected;
    shared_data_t* _vehicle_data;
    VehicleMemoryThread* _memThread;
};

#endif // VEHICLEDATAMANAGER_H
