#ifndef VOXELMAPREADER_H
#define VOXELMAPREADER_H

#include <QObject>
#include <QGeoPolygon>
#include <QGeoCoordinate>
#include "listofvoxels.h"

class VoxelMapReader: public QObject
{
    Q_OBJECT
public:
    VoxelMapReader(QObject *parent = nullptr);

    Q_PROPERTY(ListOfVoxels* voxels READ voxels CONSTANT)

    Q_INVOKABLE bool readVoxelMap(QString path);
    Q_INVOKABLE void checkZoomLevel(double zoomLevel);
    Q_INVOKABLE void checkPosition(double lat1, double lon1, double lat2, double lon2);

    ListOfVoxels* voxels() { return &_voxels; }

private:
    QGeoCoordinate  _coordDistanceHeading(double lat1, double lon1, double distance, double heading) const;
    bool            _shouldShow() const;

    double          currentZoomLevel;
    ListOfVoxels    _voxels;
    ListOfVoxels    _voxelsDb;
};

#endif // VOXELMAPREADER_H
