import QtQuick 2.0
import QtQuick.Controls 2.2
import "../Components"
Item {
    CheckBox{
        id: waypointsEnabled
        checked: mainMap.missionActive
        anchors{
            top: parent.top
            horizontalCenter: parent.horizontalCenter
        }
        onCheckStateChanged: function(){
            if(checkState === Qt.Checked){
                mainMap.missionActive = true
                mapOverlay.refreshMissionItems()
                missionSubmenu.openAnim.start()
            }
            else if(checkState === Qt.Unchecked){
                mapOverlay.removeMissionItems()
                mainMap.missionActive = false
                missionSubmenu.closeAnim.start()
            }

            navSettings.missionActive = mainMap.missionActive
        }
        text: "Enable Waypoints"
        font{
            pointSize: 12
            bold:true
        }
    }

    MenuSubmenu{
        id:missionSubmenu
        color:menuColor
        menuHeight: parent.height-waypointsEnabled.height-anchors.margins*2
        anchors.top: waypointsEnabled.bottom
        MenuSlider{
            id:radiusSlider
            anchors{
                top:parent.top
                right:parent.right
                left:parent.left
            }
            height:50
            slider{
                from: 10
                value: mapOverlay.defaultWaypointRadius
                to: 1000
                onMoved: {
                    mapOverlay.defaultWaypointRadius = radiusSlider.slider.value
                    navSettings.defaultWaypointRadius = mapOverlay.defaultWaypointRadius
                }
            }
            showValue: true
            text:"Default Radius(m):"

        }
        MenuCheckBox{
            id: currentMissionLineCheckbox
            anchors{
                top:radiusSlider.bottom
                right:parent.right
                left:parent.left
            }
            height:50
            checked: mapOverlay._currentMissionLineVisible
            text: "Enable Mission Line:"
            onCheckStateChanged: {
                if(checkState === Qt.Checked){
                    mapOverlay._currentMissionLineVisible = true
                }
                else if(checkState === Qt.Unchecked){
                    mapOverlay._currentMissionLineVisible = false
                }

                navSettings.currentMissionLineVisible = mapOverlay._currentMissionLineVisible
            }
        }
        MenuCheckBox{
            id: altitudegraphCheckBox
            anchors{
                top:currentMissionLineCheckbox.bottom
                right:parent.right
                left:parent.left
            }
            height:50
            checked: mapHud.altitudeGraphEnabled
            text: "Enable Altitude Graph:"
            onCheckStateChanged: {
                if(checkState === Qt.Checked){
                    mapHud.altitudeGraphEnabled = true
                }
                else if(checkState === Qt.Unchecked){
                    mapHud.altitudeGraphEnabled = false
                }
            }
        }
        MenuCheckBox{
            id: telemetryCheckBox
            anchors{
                top:altitudegraphCheckBox.bottom
                right:parent.right
                left:parent.left
            }
            height:50
            checked: mapHud.telemetryEnabled
            text: "Enable Telementry:"
            onCheckStateChanged: {
                if(checkState === Qt.Checked){
                    mapHud.telemetryEnabled = true
                }
                else if(checkState === Qt.Unchecked){
                    mapHud.telemetryEnabled = false
                }
            }
        }
        MenuCheckBox{
            id: compassCheckBox
            anchors{
                top:telemetryCheckBox.bottom
                right:parent.right
                left:parent.left
            }
            height:50
            checked: mapHud.compassEnabled
            text: "Enable Compass:"
            onCheckStateChanged: {
                if(checkState === Qt.Checked){
                    mapHud.compassEnabled = true
                }
                else if(checkState === Qt.Unchecked){
                    mapHud.compassEnabled = false
                }
            }
        }

        MenuCheckBox{
            id: radiusOnCurrentCheckBox
            anchors{
                top:compassCheckBox.bottom
                right: parent.right
                left: parent.left
            }
            height:50
            checked: navSettings.radiusOnCurrentOnly
            text: "Accept. radius on current item only:"
            onCheckStateChanged: {
                if(checkState === Qt.Checked){
                    navSettings.radiusOnCurrentOnly = true
                }
                else if(checkState === Qt.Unchecked){
                    navSettings.radiusOnCurrentOnly = false
                }

            }
        }
    }
}
