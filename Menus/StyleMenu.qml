import QtQuick 2.2
import QtQml.Models 2.1
import QtQuick.Dialogs 1.3
import QtQuick.Controls 2.2
import QtLocation 5.15
import "../Components"
Item {
    Rectangle{
        id: mapStyle
        color:Qt.rgba(0.7,0.7,0.7,0.8)
        radius: 5
        anchors{
            top:parent.top
            margins:2
            horizontalCenter: parent.horizontalCenter
        }
        width:300
        height:40

        Label{
            text:"Style Type"
            anchors{
                horizontalCenter: parent.horizontalCenter
                top:parent.top
            }
        }
        ListModel{
            id:styleListModel
        }

        ComboBox{
            id:mapStyleComboBox

            model: {
                var mapTypes = new Array;
                for(var i =0;i<mainMap.supportedMapTypes.length;i++){
                    mapTypes.push(mainMap.supportedMapTypes[i].name)
                }
                return mapTypes

            }
            anchors.fill: parent
            anchors.margins: 2
            font.pointSize: 10
            flat:true
            onActivated: {
                navSettings.mapboxStyle = mapStyleComboBox.model[index]
            }

            onCurrentTextChanged: {
                mainMap.setMapType(mapStyleComboBox.currentText)
            }
            Component.onCompleted: {
                mapStyleComboBox.currentIndex = mapStyleComboBox.find(navSettings.mapboxStyle)
            }

        }
    }

    ListView{
        id: listView
        focus: true
        clip: true
        width: 4 * parent.width / 5

        property int maxHeight : (parent.height - mapStyle.height - newLayerButton.height - 50)

        height: contentHeight < maxHeight ? contentHeight : maxHeight
        anchors{
            top: mapStyle.bottom
            horizontalCenter: parent.horizontalCenter
            margins: 5
        }

        model: geoJsonParser
        delegate: Rectangle {
            property var i: index
            property string layerName: name
            property var doc: document

            color: menuColor
            radius:3
            height:50
            width: listView.width
            anchors{
                margins:3
            }

            Label {
                text: parent.layerName
                color: "black"
                font.pointSize: 20
                anchors.verticalCenter: parent.verticalCenter
                anchors.margins: 5
            }

            CheckBox {
                text: "Hide"
                checked: false
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: editButton.left
                onCheckStateChanged: {
                    if (checked) {
                        parent.doc.isVisible = false
                    }
                    else {
                        parent.doc.isVisible = true
                    }
                }
            }

            NavButton {
                id: editButton
                buttonText: "Remove"
                normalColor: Qt.rgba(0.7, 0.7, 0.7, 0.8)
                hoverColor: Qt.rgba(0.7, 0.7, 0.7, 1)
                anchors{
                    verticalCenter: parent.verticalCenter
                    right: parent.right
                    margins: 2
                }
                onPressed: {
                    geoJsonParser.removeDocument(model.layerName)
                }
            }
        }
    }

    NavButton {
        id: newLayerButton
        buttonText: "New Layer"
        normalColor: Qt.rgba(0.7, 0.7, 0.7, 0.8)
        hoverColor: Qt.rgba(0.7, 0.7, 0.7, 1)
        anchors{
            top: listView.bottom
            horizontalCenter: parent.horizontalCenter
            margins: 5
        }
        onPressed: () => {
            newLayerDialog.open()
        }
    }

    Dialog{
        id: newLayerDialog
        modal:true
        anchors.centerIn: parent
        height: 250
        width: 300
        FileDialog{
            id:newLayerPathDialog
            title:"Select a File"
            onRejected: {
                console.log("error")
            }
        }
        Rectangle{
            id: nameInput
            color:menuColor
            radius: 5
            anchors{
                top:parent.top
                right:parent.right
                left:parent.left
                margins:2
            }
            height:parent.height/5

            property string nameText: nameInputText.text
            Label{
                text:"Name"
                anchors{
                    horizontalCenter: parent.horizontalCenter
                    top:parent.top
                }
            }

            TextEdit{
                id:nameInputText
                anchors.fill: parent
                anchors.margins: 2
                font.pointSize: 10
                verticalAlignment: TextEdit.AlignVCenter
            }
        }
        Rectangle{
            id: pathInput
            color:menuColor
            radius: 5
            anchors{
                top:nameInput.bottom
                right:parent.right
                left:parent.left
                margins:2
            }
            height:parent.height/5
            property string pathText: pathInputText.fileUrl

            Label{
                text:"Path"
                anchors{
                    horizontalCenter: parent.horizontalCenter
                    top:parent.top
                }
            }
            TextEdit{
                id: pathInputText
                anchors.fill: parent
                onActiveFocusChanged:  {
                    if(pathInputText.activeFocus){
                        newLayerPathDialog.open()
                    }
                }
                text: fileUrl.toString()
                property string fileUrl: newLayerPathDialog.fileUrl
                font.pointSize: 10
                anchors.margins: 2
                verticalAlignment: TextEdit.AlignVCenter
                clip:true
            }
        }

        Rectangle{
            id: sourceType
            color:menuColor
            radius: 5
            anchors{
                top:pathInput.bottom
                right:parent.right
                left:parent.left
                margins:2
            }
            height:parent.height/5
            property string sourceTypeText: sourceTypeComboBox.currentText
            Label{
                text:"Source Type"
                anchors{
                    horizontalCenter: parent.horizontalCenter
                    top:parent.top
                }
            }
            ComboBox{
                id:sourceTypeComboBox
                model: ["geojson","vector","raster","raster-dem","image"]
                width: 100
                anchors.fill: parent
                anchors.margins: 2
                font.pointSize: 10
                flat:true

            }
        }
        Rectangle{
            id: layerType
            color:menuColor
            radius: 5
            anchors{
                top:sourceType.bottom
                right:parent.right
                left:parent.left
                margins:2
            }
            height:parent.height/5
            property string layerTypeText: layerTypeComboBox.currentText
            Label{
                text:"Layer Type"
                anchors{
                    horizontalCenter: parent.horizontalCenter
                    top:parent.top
                }
            }
            ComboBox{
                id:layerTypeComboBox
                model: ["fill", "line", "symbol", "circle", "heatmap", "fill-extrusion", "raster", "hillshade", "background", "sky"]
                width: 100
                anchors.fill: parent
                anchors.margins: 2
                flat:true
                font.pointSize: 10

            }
        }

        NavButton {
            id: addConfirmButton
            width: parent.width
            height: 30
            anchors.top: layerType.bottom
            radius: 0
            buttonText: "Add"
            normalColor: Qt.rgba(0.7, 0.7, 0.7, 0.8)
            hoverColor: Qt.rgba(0.7, 0.7, 0.7, 1)
            onPressed: {
               geoJsonParser.parseGeoJsonDocument(pathInput.pathText, nameInput.nameText)
               newLayerDialog.accepted()
               newLayerDialog.close()
            }
        }
    }
}
