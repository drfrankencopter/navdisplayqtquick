import QtQuick 2.2
import QtQml.Models 2.1
import Qt.labs.platform 1.1
import QtQuick.Controls 2.2
import QtLocation 5.15
import QtQuick.Layouts 1.14
import "../Components"
Item {
    CheckBox{
        id: controllerEnabled
        anchors{
            horizontalCenter: parent.horizontalCenter
        }

        checked: mainMap.controllerActive
        onCheckStateChanged: function(){
            if(checkState === Qt.Checked){
                mainMap.controllerActive =true;
                mainMap.controllerPaused=false;
                controllerSubmenu.openAnim.start()
            }
            else if(checkState === Qt.Unchecked){
                mainMap.controllerActive =false;
                mainMap.controllerPaused=true;
                controllerSubmenu.closeAnim.start()
            }

            // Save the new value in the settings
            navSettings.controllerActive = mainMap.controllerActive
        }
        text: "Enable Controller"
        font{
            pointSize: 12
            bold:true
        }
    }


    MenuSubmenu{
        id:controllerSubmenu
        color:menuColor
        menuHeight: parent.height-controllerEnabled.height-anchors.margins*2
        anchors.top: controllerEnabled.bottom
        MenuCheckBox{
            id: automaticZoomingCheckBox
            anchors{
                top:parent.top
                right:parent.right
                left:parent.left
            }
            height:50
            checked: mainMap._automaticZoomEnabled
            onCheckStateChanged: {
                if(checkState === Qt.Checked){
                    mainMap._automaticZoomEnabled = true;
                }
                else if(checkState === Qt.Unchecked){
                    mainMap.tilt=0
                    mainMap._automaticZoomEnabled = false;
                }

                // Save the new value in the settings
                navSettings.automaticZoomEnabled = mainMap._automaticZoomEnabled
            }
            text:"Automatic Zooming:"
        }
        MenuCheckBox{
            id: automaticTiltCheckBox
            anchors{
                top:automaticZoomingCheckBox.bottom
                right:parent.right
                left:parent.left
            }
            height:50
            checked: mainMap._automaticTiltEnabled
            onCheckStateChanged: {
                if(checkState === Qt.Checked){
                    mainMap._automaticTiltEnabled = true;
                }
                else if(checkState === Qt.Unchecked){
                    mainMap._automaticTiltEnabled = false;
                    mainMap.tilt=0
                }

                navSettings.automaticTiltEnabled = mainMap._automaticTiltEnabled
            }
            text:"Automatic Tilting:"
        }
        MenuCheckBox{
            id: automaticGraphZoomCheckBox
            anchors{
                top:automaticTiltCheckBox.bottom
                right:parent.right
                left:parent.left
            }
            height:50
            checked: mapHud._graphZoomEnabled
            onCheckStateChanged: {
                if(checkState === Qt.Checked){
                    mapHud._graphZoomEnabled = true;
                }
                else if(checkState === Qt.Unchecked){
                    mapHud._graphZoomEnabled = false;
                    //mapHud.mapAltitudeGraph.chart.zoomReset()
                }
            }
            text:"Automatic Graph Zoom:"
        }

        MenuComboBox{
            id:followTypeComboBox
            anchors{
                top:automaticGraphZoomCheckBox.bottom
                right:parent.right
                left:parent.left
            }
            height:50
            text:"Follow Type:"
            model: ["Heading", "Direction","Velocity"]
            onCurrentTextChanged: {
                mainMap.controllerMode = combobox.currentText
                // Save the new value in the settings
                navSettings.controllerMode = mainMap.controllerMode
            }
        }
        MenuSlider{
            id:headingDirectionSlider
            anchors{
                top:followTypeComboBox.bottom
                right:parent.right
                left:parent.left
            }
            height:50
            visible:followTypeComboBox.combobox.currentText==="Direction"
            showValue: true
            slider.onMoved: {
                mainMap.headingDirection = slider.value
            }
            slider.from:0
            slider.value: mainMap.headingDirection
            slider.to:360
            text:"Direction:"
        }

        MenuSlider{
            id:offsetSlider
            anchors{
                top:headingDirectionSlider.visible? headingDirectionSlider.bottom:followTypeComboBox.bottom
                right:parent.right
                left:parent.left
            }
            height: 50
            slider.from: -200
            slider.value: mainMap.aircraftOffset
            slider.to: 200
            slider.onMoved: {
                if (!mapHud.missionLogVisible) {
                    mainMap.aircraftOffset=slider.value
                }
                navSettings.aircraftOffset = mainMap.aircraftOffset
            }
            text:"Aircraft Offset:"
        }
    }
}
