import QtQuick 2.2
import QtQml.Models 2.1
import QtQuick.Dialogs 1.3
import QtQuick.Controls 2.2
import QtLocation 5.15
import QtGraphicalEffects 1.15
import "../Components"
import NavEnums 1.0
import Palettes 1.0
Item {

    id: _root

    function determineCurrentIndex(list, value) {
        var i;
        for (i = 0; i < list.count; ++i) {
            if (list.get(i).value === value) {
                return i
            }
        }

        return 0
    }


    MenuComboBox{
        id: presetComboBox

        function addPreset(name){
            presetList.append({"text":name})
            presetComboBox.combobox.currentIndex = presetComboBox.combobox.indexOfValue(name)
        }
        function loadPresets(){
            var presets = presetManager.getPresets()
            console.log(presets)
            for(var i=0;i<presets.length;i++){
                presetList.append({"text":presets[i]})
            }
        }
        Component.onCompleted: loadPresets()
        combobox.textRole: "text"
        anchors{
            top:parent.top
            right:parent.right
            left:parent.left
        }
        height:50
        model:ListModel {
            id: presetList
        }
        text: "Preset:"
        onCurrentTextChanged: {
            presetManager.changePreset(combobox.currentText);
        }
        Connections{
            target: presetManager
            function onCurrentPresetChanged(name) {
                if(presetList.get(presetComboBox.combobox.currentIndex)!==name){
                    presetComboBox.combobox.currentIndex = presetComboBox.combobox.indexOfValue(name)
                }
            }
        }
    }
    NavButton {
        id: newpresetButton
        anchors{
            verticalCenter: presetComboBox.verticalCenter
            right: presetComboBox.right
            margins: 2
        }
        buttonText: "+"
        onPressed: {
            newPresetDialog.open()
        }
    }

    MenuColor{
        id:aircraftColorSelection
        anchors{
            top:presetComboBox.bottom
            right:parent.right
            left:parent.left
        }
        height:50
        color:navSettings.aircraftColor
        text:"Aircraft Colour:"
        onColorChanged: {
            presetManager.setPresetElement("aircraftColor", dialog.color)
        }
        dialog.onColorChanged: {
            navSettings.aircraftColor = dialog.color
        }

        Connections{
            target: presetManager
            function onCurrentPresetChanged(name) {
                if(presetManager.findPresetElement("aircraftColor")===null){
                    presetManager.setPresetElement("aircraftColor",navSettings.aircraftColor)
                    return
                }
                navSettings.aircraftColor = presetManager.findPresetElement("aircraftColor")
                console.log(aircraftColorSelection.color)
            }
        }
    }
    MenuColor{
        id:missionLineColorSelection
        anchors{
            top:aircraftColorSelection.bottom
            right:parent.right
            left:parent.left
        }
        height:50
        color:navSettings.missionLineColor
        text:"Mission Line Colour:"
        dialog.onColorChanged: {
            navSettings.missionLineColor = dialog.color
        }
    }
    MenuColor{
        id:currentMissionLineColorSelection
        anchors{
            top:missionLineColorSelection.bottom
            right:parent.right
            left:parent.left
        }
        height: 50
        color: navSettings.currentMissionLineColor
        text:"Current Mission Line Colour:"
        dialog.onColorChanged: {
            navSettings.currentMissionLineColor = dialog.color
        }
    }
    MenuColor{
        id:oldMissionLineColorSelection
        anchors{
            top:currentMissionLineColorSelection.bottom
            right:parent.right
            left:parent.left
        }
        height:50
        color:navSettings.oldMissionLineColor
        text:"Expired Mission Line Colour:"
        dialog.onColorChanged: {
            navSettings.oldMissionLineColor = dialog.color
        }
    }
    MenuCheckBox{
        id: showZoomButtonsCheckBox
        anchors{
            top: oldMissionLineColorSelection.bottom
            right:parent.right
            left:parent.left
        }
        checked: mainMap._showZoomButtons
        onCheckStateChanged: function(){
            if(checkState === Qt.Checked){
                mainMap._showZoomButtons = true;
            }
            else if(checkState === Qt.Unchecked){
                mainMap._showZoomButtons = false;
            }

            navSettings.showZoomButtons = mainMap._showZoomButtons
        }
        text: "Show Zoom Buttons"
    }

    MenuCheckBox {
        id: showRangeRings
        anchors{
            top: showZoomButtonsCheckBox.bottom
            right:parent.right
            left:parent.left
        }
        checked: navSettings.showRangeRings
        onCheckStateChanged: {
            var isChecked = checkState === Qt.Checked;
            navSettings.showRangeRings = isChecked
        }
        text: "Show Range Rings"
    }
    MenuCheckBox {
        id: showTrajectoryLine
        anchors{
            top: showRangeRings.bottom
            right:parent.right
            left:parent.left
        }
        checked:  mapOverlay.isTrajectoryLineVisible
        onCheckStateChanged: function(){
            if(checkState === Qt.Checked){
                mapOverlay.isTrajectoryLineVisible = true
            }
            else if(checkState === Qt.Unchecked){
                mapOverlay.isTrajectoryLineVisible = false
            }

            navSettings.isTrajectoryLineVisible = mapOverlay.isTrajectoryLineVisible
        }
        text: "Show trajectory line"
    }

    MenuComboBox {
        id: horizontalUnits
        width: 200
        anchors{
            top: showTrajectoryLine.bottom
            right:parent.right
            left:parent.left
        }
        combobox.textRole: "text"
        combobox.currentIndex: _root.determineCurrentIndex(horizontalUnitsList, navSettings.horizontalUnits)
        model: ListModel {
            id: horizontalUnitsList
            ListElement { text: "Feet (ft)";            value: Enums.Imperial  }
            ListElement { text: "Meters (m)";           value: Enums.Metric    }
            ListElement { text: "Nautical Miles (NM)";  value: Enums.Nautical  }
        }
        text:"Horizontal Units"
        combobox.onCurrentIndexChanged: {
            // Save it in the settings
            if (navSettings.horizontalUnits !== horizontalUnitsList.get(combobox.currentIndex).value)
                navSettings.horizontalUnits = horizontalUnitsList.get(combobox.currentIndex).value
        }
    }

    MenuComboBox {
        id: verticalUnits
        width: 200
        anchors{
            top: horizontalUnits.bottom
            right:parent.right
            left:parent.left
        }
        combobox.textRole: "text"
        combobox.currentIndex: _root.determineCurrentIndex(verticalUnitsList, navSettings.verticalUnits)
        model: ListModel {
            id: verticalUnitsList
            ListElement { text: "Feet (ft)";            value: Enums.Imperial  }
            ListElement { text: "Meters (m)";           value: Enums.Metric    }
            ListElement { text: "Nautical Miles (NM)";  value: Enums.Nautical  }
        }
        text: "Vertical Units"
        combobox.onCurrentIndexChanged: {
            // Save it in the settings
            if (navSettings.verticalUnits !== verticalUnitsList.get(combobox.currentIndex).value)
                navSettings.verticalUnits = verticalUnitsList.get(combobox.currentIndex).value
        }
    }

    MenuComboBox {
        id: horizontalSpeedUnits
        width: 200
        anchors{
            top: verticalUnits.bottom
            right:parent.right
            left:parent.left
        }
        combobox.textRole: "text"
        text: "Horizontal Speed Units"
        combobox.currentIndex: _root.determineCurrentIndex(horizontalSpeedUnitsList, navSettings.horizontalSpeedUnits)
        model: ListModel {
            id: horizontalSpeedUnitsList
            ListElement { text: "Feet per min (ft/min)"; value: Enums.FtPerMin       }
            ListElement { text: "Meters per sec (m/s)";  value: Enums.MetersPerSec   }
            ListElement { text: "Knots (kt)";            value: Enums.Knots          }
        }

        combobox.onCurrentIndexChanged: {
            // Save it in the settings
            if (navSettings.horizontalSpeedUnits !== horizontalSpeedUnitsList.get(combobox.currentIndex).value)
                navSettings.horizontalSpeedUnits = horizontalSpeedUnitsList.get(combobox.currentIndex).value
        }
    }

    MenuComboBox {
        id: verticalSpeedUnits
        width: 200
        anchors{
            top: horizontalSpeedUnits.bottom
            right:parent.right
            left:parent.left
        }
        combobox.textRole: "text"
        text: "Vertical Speed Units"
        combobox.currentIndex: _root.determineCurrentIndex(verticalSpeedUnitsList, navSettings.verticalSpeedUnits)
        model: ListModel {
            id: verticalSpeedUnitsList
            ListElement { text: "Feet per min (ft/min)"; value: Enums.FtPerMin       }
            ListElement { text: "Meters per sec (m/s)";  value: Enums.MetersPerSec   }
            ListElement { text: "Knots (kt)";            value: Enums.Knots          }
        }

        combobox.onCurrentIndexChanged: {
            // Save it in the settings
            if (navSettings.verticalSpeedUnits !== verticalSpeedUnitsList.get(combobox.currentIndex).value)
                navSettings.verticalSpeedUnits = verticalSpeedUnitsList.get(combobox.currentIndex).value
        }
    }

    MenuCheckBox{
        id: toggleAutoPaletteChange
        anchors{
            top: verticalSpeedUnits.bottom
            right:parent.right
            left:parent.left
        }
        checked: navSettings.supportAutomaticPaletteChange
        onCheckStateChanged: function() {
            var checkedState = checkState === Qt.Checked
            navSettings.supportAutomaticPaletteChange = checkedState

            // Reload the palette if needed
            if (mainMap.activeMapType.night) {
                navSettings.colorPalette = Palette.Light
            }
            else {
                navSettings.colorPalette = Palette.Dark
            }
        }
        text: "Change palette based on map style"
    }

    MenuComboBox {
        id: colorPalette
        width: 200
        anchors{
            top: toggleAutoPaletteChange.bottom
            right:parent.right
            left:parent.left
        }
        combobox.textRole: "text"
        text: "Color Palette"
        visible: !navSettings.supportAutomaticPaletteChange
        combobox.currentIndex: _root.determineCurrentIndex(colorPaletteOptions, navSettings.colorPalette)
        model: ListModel {
            id: colorPaletteOptions
            ListElement { text: "Light";    value: Palette.Light    }
            ListElement { text: "Dark";     value: Palette.Dark     }
        }

        combobox.onCurrentIndexChanged: {
            let val = colorPaletteOptions.get(combobox.currentIndex).value
            if (navSettings.colorPalette !== val) {
                navSettings.colorPalette = val
                console.log("I changed the palette to " + colorPaletteOptions.get(combobox.currentIndex).text)
            }
        }
    }

    Dialog{
        id: newPresetDialog
        height: 100
        width: 100
        anchors.centerIn: parent
        Rectangle{
            id: nameInput
            color:menuColor
            radius: 5
            anchors{
                top:parent.top
                right:parent.right
                left:parent.left
                margins:2
            }
            height:parent.height/2

            property string nameText: nameInputText.text
            Label{
                text:"Name"
                anchors{
                    horizontalCenter: parent.horizontalCenter
                    top:parent.top
                }
            }

            TextEdit{
                id:nameInputText
                anchors.fill: parent
                anchors.margins: 2
                font.pointSize: 10
                verticalAlignment: TextEdit.AlignVCenter
            }
        }
        NavButton {
            id: addConfirmButton
            width: parent.width
            height: 20
            anchors{
                bottom:parent.bottom
                horizontalCenter:parent.horizontalCenter
                margins:2
            }

            buttonText: "Add"
            onPressed: {
               presetManager.newPreset(nameInputText.text)
               presetComboBox.addPreset(nameInputText.text)
               newPresetDialog.accepted()
               newPresetDialog.close()
            }
        }
    }
}
