import QtQuick 2.0
import QtLocation 5.15
import QtPositioning 5.6
import QtGraphicalEffects 1.15
import QtQuick.Controls 2.5
import NavEnums 1.0
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0
import "helper.js" as Helper
import "Hud"
import "Components"
Item {
    id:mapHud

    function calculateScale()
    {
        var coord1, coord2, dist, text, f
        f = 0
        coord1 = mainMap.toCoordinate(Qt.point(0,mapScale.y))
        coord2 = mainMap.toCoordinate(Qt.point(0+mapScale.image.sourceSize.width,mapScale.y))
        dist = Math.round(coord1.distanceTo(coord2))

        if (dist === 0) {
            // not visible
        } else {
            for (var i = 0; i < scaleLengths.length-1; i++) {
                if (dist < (scaleLengths[i] + scaleLengths[i+1]) / 2 ) {
                    f = scaleLengths[i] / dist
                    dist = scaleLengths[i]
                    break;
                }
            }
            if (f === 0) {
                f = dist / scaleLengths[i]
                dist = scaleLengths[i]
            }
        }

        text = Helper.formatDistance(dist)
        mapScale.image.width = (mapScale.image.sourceSize.width * f) - 2 * mapScale.leftImage.sourceSize.width
        mapScale.text.text = text
        return dist
    }

    function generateAltitudeGraph(){

        mapAltitudeGraph.graph.removePoints(0,mapAltitudeGraph.graph.count)//clear prev graph
        mapAltitudeGraph.groundLine.removePoints(0,mapAltitudeGraph.groundLine.count)
        var i
        var waypointAltitudes = new Array;
        var waypointDistances = new Array;
        _groundAltitudePoints = new Array;

        for(i = 0; i < missionManager.standard.actualWaypointCount ; ++i){
            var lat =missionManager.standard.get(i).lat
            var lon =missionManager.standard.get(i).lon
            var alt = missionManager.standard.get(i).alt
            var distance = missionHelper.totalWaypointDistance(i)

            mapboxHelper.getAltitudeAt(lat,lon,distance)//Get ground altitude

            waypointDistances.push(distance)
            waypointAltitudes.push(alt)

            mapAltitudeGraph.graph.append(distance,alt)
            mapAltitudeGraph.missionPoints.append(distance,alt)
        }

        mapAltitudeGraph.graphHeight= Math.max(...waypointAltitudes)
        mapAltitudeGraph.graphWidth = Math.max(...waypointDistances)
        mapAltitudeGraph.graph.niceNumbers()
    }

    function createGroundLevelAltitude(){
        for(var i = 0; i < _groundAltitudePoints.length; i++){
            mapAltitudeGraph.groundLine.append(_groundAltitudePoints[i].distance,_groundAltitudePoints[i].alt)
        }
    }

    function createGraphCurrentLeg(current){
        if(current===0) return
        mapAltitudeGraph.activeLine.removePoints(0,mapAltitudeGraph.activeLine.count)
        //first point
        mapAltitudeGraph.activeLine.append(missionHelper.totalWaypointDistance(current-1),missionManager.standard.get(current-1).alt)
        //second point
        mapAltitudeGraph.activeLine.append(missionHelper.totalWaypointDistance(current),missionManager.standard.get(current).alt)
    }

    function log(string){
        var msgComponent =Qt.createComponent("qrc:/Hud/LogMessage.qml")
        msgComponent.createObject(logLayout,{txt:string})
    }

    function updateHudMode(){
        switch(hudMode){
        case(Enums.DefaultMode):{
            for(var i =0; i<mapHud.children.length;i++){
                if(mapHud.children[i].modes){
                    if(mapHud.children[i].modes.includes(Enums.DefaultMode)){
                        mapHud.children[i].visible=true
                    }else{
                        mapHud.children[i].visible=false
                    }
                }
            }
            break
        }
        case(Enums.DeviateMode):{
            for(var i =0; i<mapHud.children.length;i++){
                if(mapHud.children[i].modes){
                    if(mapHud.children[i].modes.includes(Enums.DeviateMode)){
                        mapHud.children[i].visible=true
                    }else{
                        mapHud.children[i].visible=false
                    }
                }
            }
            break
        }
        }
    }
    function updateDeviateState(){
        switch(deviateState){
        case(Enums.NoMission):{
            miniMissionSettings.statusText.text="No Active Mini Mission"
            break
        }
        case(Enums.Adding):{
            miniMissionSettings.statusText.text="Adding Waypoints"
            break
        }
        case(Enums.Viewing):{
            miniMissionSettings.statusText.text=mapOverlay.miniMissionWaypoints.count+" Waypoints"
            break
        }
        }
    }



    //Properties
    property variant scaleLengths: [5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000, 2000000]
    property bool altitudeGraphEnabled: true
    property bool telemetryEnabled: true
    property bool compassEnabled: true
    property bool _graphZoomEnabled: false
    property var _groundAltitudePoints
    property int hudMode: Enums.DefaultMode
    property int deviateState: Enums.NoMission
    onHudModeChanged: updateHudMode()
    onDeviateStateChanged: updateDeviateState()

    Component.onCompleted: updateHudMode()
    //HUD
    MapScale{
        id:mapScale
        anchors.bottom: missionLog.visible ? missionLog.top : (mapAltitudeGraph.visible ? mapAltitudeGraph.top : parent.bottom)
        anchors.right: parent.right
        property var modes: [Enums.DeviateMode,Enums.DefaultMode]
        ColorOverlay{
            anchors.fill:mapScale
            source:mapScale
            color: paletteHolder.hudColor
        }
        Connections{
            target: mainMap
            function onZoomLevelChanged(){
                mapHud.calculateScale()
            }
        }

    }

    MapZoomButtons{
        id:mapZoomButtons
        property var modes: [Enums.DeviateMode,Enums.DefaultMode]

        visible: mainMap._showZoomButtons
        anchors {
            left: missionTelemetry.left
            bottom: mapScale.bottom
        }
    }

    MapAltitudeGraph{
        id:mapAltitudeGraph
        property var modes: [Enums.DefaultMode]
        enabled:mapHud.altitudeGraphEnabled
        function autoZoomGraph(){
            mapAltitudeGraph.chart.zoomReset()
            var width = mapAltitudeGraph.chart.plotArea.width/5
            var point = mapAltitudeGraph.aircraft.at(0)
            //use this rect to change zoom parameters
            var rect =Qt.rect(mapAltitudeGraph.chart.mapToPosition(point,mapAltitudeGraph.aircraft).x-(width)/2,
                              mapAltitudeGraph.chart.plotArea.y,
                              width,
                              mapAltitudeGraph.chart.plotArea.height)
            mapAltitudeGraph.chart.zoomIn(rect)
        }
        function moveAircraftOnGraph(){//moves the graph point that represents the aircraft. The x value is the distance to current plus aircraft distance from next waypoint
            var currentIndex=missionManager.standard.seqToIndex(missionManager.standard.getCurrentWaypoint());
            if(currentIndex === 0 || missionManager.standard.actualWaypointCount <= 0) return;
            var current = missionManager.standard.get(currentIndex)
            var prev = missionManager.standard.get(currentIndex-1)
            var distance=missionHelper.totalWaypointDistance(currentIndex-1)+
                    Math.max(0,missionHelper.distanceBetween(current.lat,current.lon,prev.lat,prev.lon)-missionHelper.distanceBetween(vehicleManager.lat(),vehicleManager.lon(),current.lat,current.lon));
            mapAltitudeGraph.moveAircraft(distance,vehicleManager.alt());
        }

        height:parent.height/5
        anchors{
            left:parent.left
            right:parent.right
            bottom:parent.bottom
            margins:2
        }
        visible: mapHud.altitudeGraphEnabled && !mapHud.missionLogVisible
        Connections{
            target: timer
            function onFullSecond(){

                mapAltitudeGraph.moveAircraftOnGraph()
                if(mapHud._graphZoomEnabled){
                    mapAltitudeGraph.autoZoomGraph()
                }

            }
        }
        Connections{
            target:mapboxHelper
            function onGroundAltitudeReady(distance, alt, isValid){
                _groundAltitudePoints.push({distance:distance,alt:alt})
                if( _groundAltitudePoints.length>=mapAltitudeGraph.graph.count){
                    _groundAltitudePoints.sort((a,b)=>{return a.distance-b.distance})
                    mapHud.createGroundLevelAltitude()
                    _groundAltitudePoints= new Array;
                }
            }
        }
        Connections {
            target: missionManager
            function onNewMissionCurrent(currentNumber) {
                mapHud.createGraphCurrentLeg(missionManager.standard.seqToIndex(currentNumber))
            }
        }
    }
    MapCompass{
        id:mapCompass
        property var modes: [Enums.DefaultMode]

        anchors.fill: parent
        compassVertical:0//fix this to be proportional to offset
        visible: mainMap.controllerActive && mapHud.compassEnabled

        Connections{
            target: timer
            function onTriggered(){mapCompass.bearingAngle = mainMap.bearing;}
        }
        ColorOverlay{
            anchors.fill:mapCompass
            source:mapCompass
            color: paletteHolder.hudColor
        }
    }

    MissionTelemetry {
        id: missionTelemetry
        property var modes: [Enums.DefaultMode]

        visible: mapHud.telemetryEnabled
        z: mapOverlay.z+1
        anchors{
            right: parent.right
            rightMargin: 10
            bottom: mapScale.top
            bottomMargin: 10
        }
        opacity:0.8
    }

    MenuButton{
        id: menuIcon
        anchors{
            top: parent.top
            left: parent.left
            margins:5
        }
        width: 40
        height: 40
        opacity: 0.5
        visible:!menuScreen.visible
        mouseArea.onClicked: menuScreen.visible=true
        image.source: "qrc:/imgs/imgs/setting_icon.png"
    }


    RecenterButton{
        id: recenterButton
        height: 60
        width: height
        anchors.left: parent.left
        anchors.bottom: mapScale.bottom
        anchors.leftMargin: 10
        anchors.bottomMargin: 10
        color: paletteHolder.hudColor
        visible: mainMap.controllerPaused
        radius:8
        opacity:0.8
    }

    DynamicButtons{
        id:dynamicButtons
        property var modes: [Enums.DefaultMode]
        function cycleHeading(){// maybe make this cleaner
            switch (mainMap.controllerMode){
            case "Heading":{
                mainMap.controllerMode="Direction"
                mapHud.log("Changing Controller to Direction Up")
                break;
            }
            case "Direction":{
                mainMap.controllerMode="Velocity"
                mapHud.log("Changing Controller to Velocity Up")
                break;
            }
            case "Velocity":{//not implemented correctly
                mainMap.controllerMode="Heading"
                mapHud.log("Changing Controller to Heading Up")
                break;
            }
            }
        }

        anchors{
            top:menuIcon.bottom
            left: menuIcon.left
            margins:5
        }

        Component.onCompleted: {
            dynamicButtons.addButton("Mission Log", "qrc:/imgs/imgs/log.png", () => { missionLog.visible = !missionLog.visible })
            dynamicButtons.addButton("Toggle Layers", "qrc:/imgs/imgs/layer.png", () => {
                                         mapOverlay.allLayersAreVisible = !mapOverlay.allLayersAreVisible
                                         mapHud.log("Layers visibility set to " + mapOverlay.allLayersAreVisible)
                                     })
            dynamicButtons.addButton("Swap Heading","qrc:/imgs/imgs/waypoint.png",cycleHeading)
            dynamicButtons.addButton("Toggle Range Rings", "qrc:/imgs/imgs/circle.png", () => { navSettings.showRangeRings = !navSettings.showRangeRings })
            dynamicButtons.addButton("Transparency Slider", "qrc:/imgs/imgs/opacity_icon.png", () => { opacitySliderBox.visible = !opacitySliderBox.visible })
        }

        height:300
        width:parent.width/2

    }
    MouseArea{
        id:addMiniWaypointMouseArea
        property var modes: [Enums.DeviateMode]

        anchors.fill:parent
        enabled:deviateState===Enums.Adding

        onClicked: {
            var coord = mainMap.toCoordinate(Qt.point(mouse.x, mouse.y))
            mapOverlay.addMiniMissionWaypoint(coord)
        }
    }


    MiniMissionWaypointSettings{
        id:miniMissionWaypointSettings

        property var modes: [Enums.DeviateMode]

        anchors.fill: parent
        Connections{
            target:mapOverlay
            function onMiniMissionSelectionChanged(index){
                var element = mapOverlay.miniMissionWaypoints.get(index)
                miniMissionWaypointSettings.waypointNumber=element.number
                miniMissionWaypointSettings.latValue=Math.round(element.latitude*1e6)/1e6
                miniMissionWaypointSettings.lonValue=Math.round(element.longitude*1e6)/1e6
                miniMissionWaypointSettings.altValue=Math.round(element.altitude*1e6)/1e6
                 miniMissionWaypointSettings.radiusValue=element.acceptanceRadius
            }
        }
        Connections {
            target: missionManager
            function onNewMissionCurrent(currentNumber) {
                if(deviateState===Enums.NoMission){
                    miniMissionSettings.newCurrent()
                }
            }
        }
    }
    MiniMissionSettings{
        id:miniMissionSettings
        property var modes: [Enums.DeviateMode]
        anchors.fill:parent

    }

    Rectangle{
        radius:20
        anchors{
            right:parent.right
            top:parent.top
            margins:5
        }
        color:Qt.rgba(0,0,0,0.5)
        width:modeRow.children.length*60
        height:50
        RowLayout{
            id:modeRow
            anchors.margins: 5
            spacing:5
            layoutDirection: Qt.RightToLeft
            anchors.fill: parent
            property var chosenButton: defaultButton
            Rectangle{
                id:defaultButton
                Layout.minimumWidth: 50
                Layout.fillHeight: true
                radius:10
                color:parent.chosenButton==defaultButton?Qt.rgba(0,0,0,0.8):Qt.rgba(0,0,0,0.5)
                Text{
                    anchors.centerIn: parent
                    text:"Default"
                    color:"white"
                }
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        modeRow.chosenButton=defaultButton
                        mapHud.hudMode=Enums.DefaultMode
                    }
                }
            }
            Rectangle{
                id:deviateButton
                Layout.minimumWidth: 50
                Layout.fillHeight: true
                radius:10
                color:parent.chosenButton==deviateButton?Qt.rgba(0,0,0,0.8):Qt.rgba(0,0,0,0.5)
                Text{
                    anchors.centerIn: parent
                    text:"Deviate"
                    color:"white"
                }
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        modeRow.chosenButton=deviateButton
                        mapHud.hudMode=Enums.DeviateMode
                    }
                }
            }
        }

    }



    MissionLog {
        id: missionLog
        visible: false
        onVisibleChanged: {
            if (visible == false)
                mainMap.aircraftOffset = navSettings.aircraftOffset
            else
                mainMap.aircraftOffset = 25
        }
    }

    ColumnLayout{
        id:logLayout
        anchors{
            horizontalCenter: parent.horizontalCenter
            bottom: parent.verticalCenter
            bottomMargin: parent.height/3
        }
        width:parent.width*0.7
        height:50*children.length
        spacing:10
        layoutDirection: Qt.RightToLeft
    }

    Text {
        id: currentZoomLevel
        text: "ZL: " + mapOverlay.zoomLevel.toFixed(2)
        font.bold: true
        font.pointSize: 15
        color: paletteHolder.rangeRings
        anchors.left: missionTelemetry.left
        anchors.bottom: missionTelemetry.top
        anchors.bottomMargin: 10
    }

    Text {
        id: rangeRingsScale
        text: "RR: " + (navSettings.rangeRingStep * units.horizontalFactor).toFixed(0) + units.horizontalUnits
        font.bold: true
        font.pointSize: 15
        color: paletteHolder.rangeRings
        anchors.right: missionTelemetry.right
        anchors.bottom: missionTelemetry.top
        anchors.bottomMargin: 10
        visible: navSettings.showRangeRings
    }
    NavButton{
        buttonText: "Load Voxels"
        textColor: paletteHolder.generalText
        normalColor: paletteHolder.hudColor
        hoverColor: paletteHolder.hudHoverColor
//        onPressed: voxelMap.readVoxelMap("/Users/jeremilevesque/Documents/nrc/voxelsdata/output/Parsed_FRL_DSM_objects_10_10_5.csv")
        onPressed: voxelFilePicker.open()

        anchors{
            top:parent.top
            horizontalCenter: parent.horizontalCenter
        }
    }

    FileDialog {
        id: voxelFilePicker
        title: "Choose a voxel data file (csv with lat/lon)"
        onAccepted: {
            var path = voxelFilePicker.fileUrl.toString()
            path = path.replace(/^(file:\/{3})/,"")
            var cleanPath = decodeURIComponent(path)
            var success = voxelMap.readVoxelMap(cleanPath)
            if (success) {
                mapHud.log("Voxels successfully loaded in the backend")
            }
            else {
                mapHud.log("There was a problem loading the voxels")
            }
        }
    }

    Rectangle {
        id: opacitySliderBox
        width: 550
        height: 75
        color: Qt.rgba(0.1,0.1,0.1,0.6)
        anchors.bottom: mapAltitudeGraph.visible ? mapAltitudeGraph.top : parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottomMargin: 25
        visible: false

        Slider {
            id: opacitySlider
            from: 0
            value: 1
            to: 1

            width: 500

            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottomMargin: 10

            background: Rectangle {
                x: opacitySlider.leftPadding
                y: opacitySlider.topPadding + opacitySlider.availableHeight / 2 - height / 2
                implicitWidth: 200
                implicitHeight: 4
                width: opacitySlider.availableWidth
                height: implicitHeight
                radius: 3
                color: "gray"

                Rectangle {
                    width: opacitySlider.visualPosition * parent.width
                    height: parent.height
                    color: "#51cf7d"
                    radius: 3
                }
            }

            handle: Rectangle {
                x: opacitySlider.leftPadding + opacitySlider.visualPosition * (opacitySlider.availableWidth - width)
                y: opacitySlider.topPadding + opacitySlider.availableHeight / 2 - height / 2
                implicitWidth: 26
                implicitHeight: 26
                radius: 13
                color: opacitySlider.pressed ? "#51cf7d" : "#f6f6f6"
                border.color: opacitySlider.pressed ? "#f6f6f6" : "#bdbebf"
                border.width: 2

                Text {
                    id: opacityPercentageText
                    text: (opacitySlider.visualPosition * 100).toFixed(0)
                    anchors.centerIn: parent
                    color: "black"
                }
            }

            onValueChanged: {
                console.log(value)
                mapOverlay.voxelOpacity = value
            }

            Label{
                text: "Voxel Opacity Slider"
                color: paletteHolder.generalText
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    bottom: parent.top
                }
                font {
                    pointSize: 12
                }
            }
        }

        Text {
            text: "x"
            font.pointSize: 20
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.rightMargin: 10
            color: paletteHolder.generalText

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    opacitySliderBox.visible = false
                }

                onEntered: {
                    cursorShape = Qt.PointingHandCursor
                }

                onExited: {
                    cursorShape = Qt.ArrowCursor
                }
            }
        }
    }
}
