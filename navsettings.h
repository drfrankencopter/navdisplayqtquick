#ifndef NAVSETTINGS_H
#define NAVSETTINGS_H

#include <QObject>
#include <QSettings>
#include <QString>
#include <QVariant>
#include <QColor>
#include <QRgb>
#include "enums.h"
#include "ColorPalettes/palettebase.h"

// This define is to ease the declaration of new settings. This allows to create a new Q_PROPERTY, with an associated
// setter function, an associated getter function and a signal attached to the Q_PROPERTY. The define allows to define
// everything that I listed but in only a single line by specifying the "type" of the setting you're creating, the "name"
// of the setting and its default value on creation.
// Whenever you overwrite the value of a setting anywhere, the signal will automatically fire and everyone listening for
// that signal will be notified (espacially useful to update the UI automatically).
// If you're not familiar with this advanced syntax of preprocessor definitions, I would suggest the following link:
// https://stackoverflow.com/questions/13923425/c-preprocessor-concatenation-with-variable

#define DECLARE_SETTING(type, settingName, def)                                                                     \
    Q_SIGNALS:                                                                                                      \
    void settingName ## Changed(type newSetting);                                                                   \
    public:                                                                                                         \
    Q_PROPERTY(type settingName READ settingName WRITE set ## settingName NOTIFY settingName ## Changed)            \
    type settingName() { return get ## type ## setting(#settingName, def); }                                        \
    void set ## settingName(type& val) {                                                                            \
        if (settingName() != val) {                                                                                 \
            setValue(#settingName, val);                                                                            \
            emit settingName ## Changed(settingName());                                                             \
        }                                                                                                           \
    }                                                                                                               \
    // To improve, we might want to check if the value is different
    // If the value is the same, don't do anything

// The NavSettings interfaces the QSettings object from Qt. You can read more about this object in the Qt Documentation,
// but it essentially allows us to store the app settings permanently on the disk. The settings are stored somewhere on
// the local system and we can reference them and use the saved values on startup. We can also edit/rewrite the settings
// values during runtime (usually done in the UI).

class NavSettings : public QObject
{
    Q_OBJECT

public:
    NavSettings();
    ~NavSettings();

    DECLARE_SETTING(int, windowHeight, 400)
    DECLARE_SETTING(int, windowWidth, 600)
    DECLARE_SETTING(bool, controllerActive, true)
    DECLARE_SETTING(QString, controllerMode, "Heading Up")
    DECLARE_SETTING(bool, missionActive, true)
    DECLARE_SETTING(float, aircraftOffset, 0)
    DECLARE_SETTING(bool, isTrajectoryLineVisible, true)
    DECLARE_SETTING(bool, automaticZoomEnabled, false)
    DECLARE_SETTING(bool, automaticTiltEnabled, false)
    DECLARE_SETTING(bool, currentMissionLineVisible, true)
    DECLARE_SETTING(int, defaultWaypointRadius, 300)
    DECLARE_SETTING(QColor, aircraftColor, QColor(0,0,0,255))
    DECLARE_SETTING(QColor, hudColor, QColor(0,0,0,125))
    DECLARE_SETTING(QColor, missionLineColor, QColor("white"))
    DECLARE_SETTING(QColor, currentMissionLineColor, QColor("magenta"))
    DECLARE_SETTING(QColor, oldMissionLineColor, QColor("grey"))
    DECLARE_SETTING(bool, showZoomButtons, true)
    DECLARE_SETTING(int, verticalUnits, Enums::DIST_UNITS::Metric)
    DECLARE_SETTING(int, horizontalUnits, Enums::DIST_UNITS::Metric)
    DECLARE_SETTING(int, horizontalSpeedUnits, Enums::SPEED_UNITS::Knots)
    DECLARE_SETTING(int, verticalSpeedUnits, Enums::SPEED_UNITS::FtPerMin)
    DECLARE_SETTING(bool, radiusOnCurrentOnly, false)
    DECLARE_SETTING(bool, showRangeRings, false)
    DECLARE_SETTING(int, rangeRingStep, 50)
    DECLARE_SETTING(int, colorPalette, PaletteBase::PaletteType::Light)
    DECLARE_SETTING(QString, presets, "{}")
    DECLARE_SETTING(QString, currentPreset, "default")
    DECLARE_SETTING(QString, mapboxStyle, "mapbox://styles/mapbox/light-v9")
    DECLARE_SETTING(bool, supportAutomaticPaletteChange, true)

private:
    // The following functions are all used inside the DECLARE_SETTING macro.
    int         getintsetting       (const QString& key, const QVariant& def) const;
    bool        getboolsetting      (const QString& key, const QVariant& def) const;
    float       getfloatsetting     (const QString& key, const QVariant& def) const;
    double      getdoublesetting    (const QString& key, const QVariant& def) const;
    QString     getQStringsetting   (const QString& key, const QVariant& def) const;
    QColor      getQColorsetting    (const QString& key, const QVariant& def) const;

    void        setValue            (const QString& key, const QVariant& value);

    QSettings* settings;
};

#endif // NAVSETTINGS_H
