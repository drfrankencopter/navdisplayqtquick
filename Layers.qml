import QtQuick 2.0
import QtLocation 5.15
import QtQml.Models 2.15

Instantiator {
    id: _root

    property var map
    property int z

    property var _map: map
    property int _z: z

    model: geoJsonParser
    delegate: MapItemGroup {
        // Properties coming from the C++ model
        property var _doc: document
        property var _polygons: polygons
        property var _lines: lines
        property var _points: points

        visible: _doc.isVisible && _map.allLayersAreVisible // Allows the layers to be toggleable

        MapItemView {
            z: _root._z
            model: _polygons
            delegate: MapPolygon {
                geoShape: get
                color: Qt.rgba(0, 0, 1, 1)
                border.width: 3
                border.color: "red"
            }
        }

        MapItemView {
            z: _root._z
            model: _lines
            delegate: MapPolyline {

                property var geoPath: get

                line.color: "cyan"
                line.width: 5
                Component.onCompleted: {
                    setPath(geoPath)
                }
            }
        }

        MapItemView {
            z: _root._z
            model: _points
            delegate: MapCircle {

                property var circle: get

                radius: circle.radius
                center: circle.center
                color: Qt.rgba(1, 0, 0, 1)
            }
        }
    }

    onObjectAdded: _map.addMapItemGroup(object)
    onObjectRemoved: _map.removeMapItemGroup(object)
}
