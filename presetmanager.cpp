#include "presetmanager.h"
#include "QJsonDocument"

#include <QDebug>
#include <QJsonObject>
PresetManager::PresetManager(NavSettings* settings, QObject *parent)
    : QObject{parent}
    , _settings(settings)
{
    changePreset(settings->currentPreset());
}

void PresetManager::changePreset(QString key)
{
    QString presets = _settings->presets();
    QJsonDocument doc = QJsonDocument::fromJson(presets.toUtf8());
    if (doc.isNull())
    {
        qDebug() << "Invalid Json";
        return;
    }
    QJsonObject presetsJson = doc.object();
    if(presetsJson.contains(key))
    {
        _currentPreset = key;
        emit currentPresetChanged(key);
    }

    _settings->currentPreset() = key;
}

void PresetManager::newPreset(QString key)
{
    QString presets = _settings->presets();
    QJsonDocument doc = QJsonDocument::fromJson(presets.toUtf8());
    if (doc.isNull())
    {
        qDebug() << "Invalid Json";
        return;
    }
    QJsonObject presetsJson = doc.object();
    if(presetsJson.contains(key))
    {
        return;
    }
    presetsJson.insert(key, QJsonValue(QJsonObject()));
    QString newPreset = QString(QJsonDocument(presetsJson).toJson(QJsonDocument::Compact));
    _settings->setpresets(newPreset);
    _settings->currentPreset() = key;

    emit currentPresetChanged(key);
}

void PresetManager::removePreset(QString key)
{
    QString presets = _settings->presets();
    QJsonDocument doc = QJsonDocument::fromJson(presets.toUtf8());
    if (doc.isNull())
    {
        qDebug() << "Invalid Json";
        return;
    }
    QJsonObject presetsJson = doc.object();
    presetsJson.remove(key);
    QString newPreset =QString(QJsonDocument(presetsJson).toJson(QJsonDocument::Compact));
    _settings->setpresets(newPreset);
}

void PresetManager::setPresetElement(QString key, QVariant value)
{
    QString presets = _settings->presets();
    QJsonDocument doc = QJsonDocument::fromJson(presets.toUtf8());
    if (doc.isNull())
    {
        qDebug() << "Invalid Json";
        return;
    }
    QJsonObject presetsJson = doc.object();
    auto current = presetsJson.value(_currentPreset).toObject();
    current.insert(key,QJsonValue::fromVariant(value));
    presetsJson.insert(_currentPreset, current);
    QString newPreset =QString(QJsonDocument(presetsJson).toJson(QJsonDocument::Compact));
    _settings->setpresets(newPreset);
}

QVariant PresetManager::findPresetElement(QString key)
{
    QString presets = _settings->presets();
    QJsonDocument doc = QJsonDocument::fromJson(presets.toUtf8());
    if (doc.isNull())
    {
        qDebug() << "Invalid Json";
        return QVariant();
    }

    QJsonObject presetsJson = doc.object();
    auto value = presetsJson.value(_currentPreset).toObject()[key];

    return value.toVariant();
}

QList<QString> PresetManager::getPresets()
{
    QString presets = _settings->presets();
    QJsonDocument doc = QJsonDocument::fromJson(presets.toUtf8());
    if (doc.isNull())
    {
        qDebug() << "Invalid Json";
        return QList<QString>();
    }

    QJsonObject presetsJson = doc.object();
    return presetsJson.toVariantMap().keys();
}
