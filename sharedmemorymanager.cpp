#include "sharedmemorymanager.h"
#include "winsock.h"
#include <QDebug>
HANDLE hMapFile;
HANDLE hMapFileMission;
TCHAR sharedMemName[] = TEXT("asraSharedMem");
TCHAR missionDataSharedMemName[] = TEXT("missionDataMem");
#define BUF_SIZE sizeof(shared_data_t)

SharedMemoryManager::SharedMemoryManager()
{
    bool isFullyConnected = connect();
    qDebug() << "FULLY CONNECTED ="   << isFullyConnected << " ptr:" << m_missionData;
    _lookUpThread = new SharedMemoryThread(m_missionData, this);
    _lookUpThread->start();

    QObject::connect(_lookUpThread, &SharedMemoryThread::missionNumberChanged, this, &SharedMemoryManager::missionNumberChanged);
    QObject::connect(_lookUpThread, &SharedMemoryThread::currentMissionItemChanged,this,&SharedMemoryManager::currentMissionItemChanged);
}

SharedMemoryManager::~SharedMemoryManager()
{
    _lookUpThread->exit(0);
    CloseHandle(hMapFile);
    CloseHandle(hMapFileMission);
}

void SharedMemoryManager::missionNumberChanged() {
    emit newMissionAvailable();
}

bool SharedMemoryManager::connected()
{
    return m_controllerConnected;
}

bool SharedMemoryManager::connect()
{
    hMapFile = OpenFileMapping(
                FILE_MAP_READ,
                FALSE,
                sharedMemName);

    if (hMapFile == NULL)
    {
        //Could not create file mapping object
        qDebug()<<"failed mapping mission file..";

        return 0;
    }
    qDebug()<<"mapped main file..";
    m_sharedMemoryData = (shared_data_t *) MapViewOfFile(hMapFile,
                                                         FILE_MAP_READ, // read permission
                                                         0,
                                                         0,
                                                         BUF_SIZE);

    if (m_sharedMemoryData == NULL)
    {
        // Could not map view of file
        CloseHandle(hMapFile);
        qDebug()<<"failed connecting mission file..";
        return 0;
    }
    qDebug()<<"connected to main file..";
    m_controllerConnected =true;
    //mission data

    hMapFileMission = OpenFileMapping(
                FILE_MAP_READ,
                FALSE,
                missionDataSharedMemName);

    if (hMapFileMission == NULL)
    {
        //Could not create file mapping object
        qDebug()<<"failed mapping mission file..";
        return 0;
    }
    qDebug()<<"mapped mission file..";
    m_missionData = (missionData_t*) MapViewOfFile(hMapFileMission,
                                                   FILE_MAP_READ, // read permission
                                                   0,
                                                   0,
                                                   sizeof(missionData_t));

    if (m_missionData == NULL)
    {
        // Could not map view of file
        CloseHandle(hMapFileMission);
        qDebug()<<"failed connecting mission file..";
        return 0;
    }
    qDebug()<<"connected to mission file.. size: " << sizeof(*m_missionData);
    m_missionConnected =true;

    return 1;

}

void SharedMemoryManager::printItemDetails(const mavlink_mission_item_int_t& i) const {
    qDebug() << "Param1: " << i.param1;
    qDebug() << "Param2: " << i.param2;
    qDebug() << "Param3: " << i.param3;
    qDebug() << "Param4: " << i.param4;
    qDebug() << "x: " << i.x;
    qDebug() << "y: " << i.y;
    qDebug() << "z: " << i.z;
}

shared_data_t SharedMemoryManager::data()
{
    return *m_sharedMemoryData;
}

double SharedMemoryManager::lat()
{
    if(!m_controllerConnected)return 0;
    return m_sharedMemoryData->hg1700.lat;
}

double SharedMemoryManager::lng()
{
    if(!m_controllerConnected)return 0;
    return m_sharedMemoryData->hg1700.lng;
}

double SharedMemoryManager::hdg()
{
    if(!m_controllerConnected)return 0;
    return m_sharedMemoryData->hg1700.hdg;
}

double SharedMemoryManager::eastVel() {
    return m_sharedMemoryData->hg1700.ve;
}

double SharedMemoryManager::northVel() {
    return m_sharedMemoryData->hg1700.vn;
}

int SharedMemoryManager::waypointCount()
{
    if(!m_missionConnected)return 0;
    return m_missionData->myCurrentMission.standard.numMissionItems;

}

QString SharedMemoryManager::waypointType(int index)
{
    switch(m_missionData->myCurrentMission.standard.items[index].command){
        case(MAV_CMD_NAV_WAYPOINT):{
            return "Waypoint";
        }
        case(MAV_CMD_NAV_TAKEOFF):{
            return "Takeoff";
        }
        case(MAV_CMD_NAV_LAND):{
            return "Land";
        }
        default:{
            return "Unknown";
        }
    }
}

double SharedMemoryManager::waypointLat(int index)
{
    if(!m_missionConnected)return 0;

    if(index<waypointCount()){
        return m_missionData->myCurrentMission.standard.items[index].x*0.0000001;
    }
    return 0;
}

double SharedMemoryManager::waypointLon(int index)
{
    if(!m_missionConnected)return 0;

    if(index<waypointCount()){
        return m_missionData->myCurrentMission.standard.items[index].y*0.0000001;
    }
    return 0;
}

double SharedMemoryManager::waypointAlt(int index)
{
    if(!m_missionConnected)return 0;

    if(index<waypointCount()){
        return m_missionData->myCurrentMission.standard.items[index].z;
    }
    return 0;
}

double SharedMemoryManager::waypointParam(int index,int para)
{
    if(!m_missionConnected)return 0;
    if(index<waypointCount()){
        switch(para){
        case 1: return m_missionData->myCurrentMission.standard.items[index].param1;
        case 2: return m_missionData->myCurrentMission.standard.items[index].param2;
        case 3: return m_missionData->myCurrentMission.standard.items[index].param3;
        }
    }
    return 0;
}

int SharedMemoryManager::fenceCount() {
    return m_missionData->myCurrentMission.fences.numMissionItems;
}

QString SharedMemoryManager::fenceTypeString(int index) {
    switch(m_missionData->myCurrentMission.fences.items[index].command) {
    case MAV_CMD_NAV_FENCE_CIRCLE_INCLUSION:
        return "Circle_Inclusion";
    case MAV_CMD_NAV_FENCE_CIRCLE_EXCLUSION:
        return "Circle_Exclusion";
    case MAV_CMD_NAV_FENCE_POLYGON_VERTEX_INCLUSION:
        return "Polygon_Inclusion";
    case MAV_CMD_NAV_FENCE_POLYGON_VERTEX_EXCLUSION:
        return "Polygon_Exclusion";
    case MAV_CMD_NAV_FENCE_RETURN_POINT:
        return "Return_Point";
    default:
        return "Undefined";
    }
}

int SharedMemoryManager::fenceType(int index) const {
    return m_missionData->myCurrentMission.fences.items[index].command;
}

float SharedMemoryManager::getFenceParam(int index, int param_no) {
    switch(param_no) {
    case 1:
        return m_missionData->myCurrentMission.fences.items[index].param1;
    case 2:
        return m_missionData->myCurrentMission.fences.items[index].param2;
    case 3:
        return m_missionData->myCurrentMission.fences.items[index].param3;
    case 4:
        return m_missionData->myCurrentMission.fences.items[index].param4;
    default:
        return -1.0;
    }
}

QGeoCoordinate SharedMemoryManager::getFenceCoordinates(int index) {
    auto* item = &m_missionData->myCurrentMission.fences.items[index];
    double lat = item->x * 1E-7;
    double lon = item->y * 1E-7;
    return {lat, lon, item->z};
}

QList<QVariant> SharedMemoryManager::getPolygonPath(int index_start) {
    QList<QVariant> path;
    int vertex_count = getFenceParam(index_start, 1);

    for (int i = 0; i < vertex_count; ++i) {
        QGeoCoordinate vertex_coords = getFenceCoordinates(index_start + i);
        path.append(QVariant::fromValue(QGeoCoordinate(vertex_coords.latitude(), vertex_coords.longitude())));
    }

    return path;
}

int SharedMemoryManager::getNextFenceIndex(int index_start) {

    int next_fence_index = index_start;
    int type = fenceType(index_start);
    bool isAPolygon = type == MAV_CMD_NAV_FENCE_POLYGON_VERTEX_INCLUSION || type == MAV_CMD_NAV_FENCE_POLYGON_VERTEX_EXCLUSION;
    if (isAPolygon) {
        // Go at the end of the polygon
        // Param1 is the number of vertices
        next_fence_index += getFenceParam(index_start, 1);
    }
    else {
        ++next_fence_index;
    }

    // Make sure we don't return a out of bounds index.
    if (next_fence_index > fenceCount()) {
        next_fence_index = -1;
    }

    return next_fence_index;
}

int SharedMemoryManager::getMissionNumber() {
    return m_missionData->missionCount;
}

int SharedMemoryManager::currentMissionItem() const
{
    if(!m_missionConnected)return 0;
    return m_missionData->myCurrentMission.currentMissionItem;
}




//missionData_t *SharedMemoryManager::mission()
//{
//    if(!m_connected)return nullptr;
//    return m_missionData;
//}
