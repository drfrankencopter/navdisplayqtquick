#ifndef LISTOFVOXELS_H
#define LISTOFVOXELS_H

#include <QObject>
#include <QQmlEngine>
#include <vector>
#include <QGeoPolygon>
#include <QAbstractListModel>

class ListOfVoxels : public QAbstractListModel
{
    Q_OBJECT
public:
    ListOfVoxels() = default;

    Q_PROPERTY(int count READ count NOTIFY countChanged)

    int count() const;
    Q_INVOKABLE QVariant get(int index);
    void add(const QGeoPolygon& voxel);
    void add(const ListOfVoxels& other);
    void clearAll();

    int rowCount(const QModelIndex &parent = QModelIndex()) const override { Q_UNUSED(parent); return static_cast<int>(_voxels.size()); }
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

    static void declareQml() {
        qmlRegisterInterface<ListOfVoxels>("ListOfVoxels", 1);
    }

signals:
    void countChanged(int newCount);

private:
    enum Roles
    {
        NameRole = Qt::UserRole,
        GetRole
    };

    std::vector<QGeoPolygon> _voxels;
};

Q_DECLARE_METATYPE(ListOfVoxels*)

#endif // LISTOFVOXELS_H
