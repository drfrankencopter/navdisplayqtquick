#ifndef PRESETMANAGER_H
#define PRESETMANAGER_H

#include <QObject>
#include "navsettings.h"

class PresetManager : public QObject
{
    Q_OBJECT
public:
    explicit PresetManager(NavSettings* settings,QObject *parent = nullptr);
    Q_INVOKABLE void            changePreset(QString key);
    Q_INVOKABLE void            newPreset(QString key);
    Q_INVOKABLE void            removePreset(QString key);
    Q_INVOKABLE void            setPresetElement(QString key,QVariant value);
    Q_INVOKABLE QVariant        findPresetElement(QString key);
    Q_INVOKABLE QList<QString>  getPresets();

private:
    QString      _currentPreset;
    NavSettings* _settings;

signals:
    void currentPresetChanged(QString name);
};

#endif // PRESETMANAGER_H
