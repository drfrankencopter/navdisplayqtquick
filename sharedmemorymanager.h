#pragma once
#ifndef SHAREDMEMORYMANAGER_H
#define SHAREDMEMORYMANAGER_H
#include <QObject>
#include <QString>
#include <QQmlListProperty>
#include <QGeoCoordinate>
#include "mavNrc/globals.h"
#include "mavNrc/frl_file_types.h"
#include "sharedmemorythread.h"
class SharedMemoryManager: public QObject
{
    Q_OBJECT
public:
    SharedMemoryManager();
    ~SharedMemoryManager();
    Q_INVOKABLE bool connected();
    Q_INVOKABLE shared_data_t data();
    Q_INVOKABLE double lat();
    Q_INVOKABLE double lng();
    Q_INVOKABLE double hdg();
    Q_INVOKABLE double eastVel();
    Q_INVOKABLE double northVel();

    Q_INVOKABLE int waypointCount();
    Q_INVOKABLE QString waypointType(int index);
    Q_INVOKABLE double waypointLat(int index);
    Q_INVOKABLE double waypointLon(int index);
    Q_INVOKABLE double waypointAlt(int index);
    Q_INVOKABLE double waypointParam(int index,int para);

    // Geofences
    Q_INVOKABLE int fenceCount();
    Q_INVOKABLE QString fenceTypeString(int index);
    Q_INVOKABLE float getFenceParam(int index, int param_no);
    Q_INVOKABLE QGeoCoordinate getFenceCoordinates(int index);
    Q_INVOKABLE QList<QVariant> getPolygonPath(int index_start);
    Q_INVOKABLE int getNextFenceIndex(int index_start);

    Q_INVOKABLE int getMissionNumber();

    Q_PROPERTY(int currentMissionItem READ currentMissionItem WRITE setCurrentMissionItem NOTIFY currentMissionItemChanged)

    missionData_t* mission();
    int currentMissionItem() const;
    void setCurrentMissionItem(int newCurrentMissionItem);

signals:
    void currentMissionItemChanged();
    void newMissionAvailable();

public slots:
    void missionNumberChanged();

private:
    bool connect();
    void printItemDetails(const mavlink_mission_item_int_t& i) const;
    int fenceType(int index) const;

    bool m_controllerConnected = false;
    bool m_missionConnected = false;
    shared_data_t* m_sharedMemoryData;
    missionData_t* m_missionData;
    int m_currentMissionItem;

    SharedMemoryThread* _lookUpThread;

};
#endif // SHAREDMEMORYMANAGER_H
