#include <math.h>
#include <QDebug>
#include "vehicledatamanager.h"

#ifdef _WIN32
constexpr TCHAR sharedMemName[] = TEXT("asraSharedMem");
#else
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
const char* sharedMemName = "/asraSharedMem";
#endif
constexpr double FT_TO_M = 0.3048;

VehicleDataManager::VehicleDataManager()
    : _isSharedMemoryConnected(false)
{
    _isSharedMemoryConnected = _connectToSharedMemory();

    if (_isSharedMemoryConnected)
    {
        _memThread = new VehicleMemoryThread(_vehicle_data);

        connect(_memThread, &VehicleMemoryThread::engageChanged, this, &VehicleDataManager::handleEngageChanged);
        connect(_memThread, &VehicleMemoryThread::engageNumberChanged, this, &VehicleDataManager::handleEngageNumberChanged);
        connect(_memThread, &VehicleMemoryThread::minimizeSwitchChanged, this, &VehicleDataManager::handleMinimizeSwitchChanged);

        _memThread->start();
    }
}

void VehicleDataManager::handleEngageChanged(bool engageStatus)
{
    emit engageStatusChanged(engageStatus);
}

void VehicleDataManager::handleEngageNumberChanged(unsigned int newEngageNumber)
{
    emit engageNumberChanged(newEngageNumber);
}

void VehicleDataManager::handleMinimizeSwitchChanged(bool isMinimized)
{
    emit minimizeSwitchChanged(isMinimized);
}

bool VehicleDataManager::_connectToSharedMemory()
{
#ifdef _WIN32
    _hMapFile = OpenFileMapping(
                FILE_MAP_READ,
                FALSE,
                sharedMemName);

    if (_hMapFile == NULL)
    {
        qDebug() << "Failed mapping vehicle file..";
        return false;
    }

    qDebug() << "Mapped vehicle file..";
    _vehicle_data = (shared_data_t *) MapViewOfFile(_hMapFile,
                                                     FILE_MAP_READ, // read permission
                                                     0,
                                                     0,
                                                     sizeof(shared_data_t));

    if (_vehicle_data == NULL)
    {
        CloseHandle(_hMapFile);
        qDebug() << "Failed connecting vehicle file..";
        return false;
    }
#else
    _hMapFile = shm_open(sharedMemName, O_CREAT | O_RDWR, 0666); // Create file descriptor for shared mem. Create the mem if it doesn't already exist. Set

    if (_hMapFile < 0)
    {
        qDebug() << "Failed mapping vehicle file..";
        return false;
    }

    ftruncate(_hMapFile, sizeof(shared_data_t));

    qDebug() << "Mapped vehicle file..";

    _vehicle_data = (shared_data_t *)mmap(NULL, sizeof(shared_data_t), PROT_READ | PROT_WRITE, MAP_SHARED, _hMapFile, 0);
    if (_vehicle_data == MAP_FAILED)
    {
        close(_hMapFile);
        qDebug() << "Failed connecting vehicle file..";
        return false;
    }
#endif

    qDebug() << "Connected to vehicle file..";

    return true;
}

double VehicleDataManager::lat() const
{
    return _vehicle_data->hg1700.lat;
}

double VehicleDataManager::lon() const
{
    return _vehicle_data->hg1700.lng;
}

float VehicleDataManager::alt() const
{
    return _vehicle_data->hg1700.alt;   // ft
}

float VehicleDataManager::heading() const
{
    return _vehicle_data->hg1700.hdg;
}

float VehicleDataManager::eastVel() const
{
    return _vehicle_data->hg1700.ve * FT_TO_M;
}

float VehicleDataManager::northVel() const
{
    return _vehicle_data->hg1700.vn * FT_TO_M;
}

float VehicleDataManager::verticalVel() const
{
    return _vehicle_data->hg1700.vz * FT_TO_M;
}

float VehicleDataManager::groundSpeed() const
{
    float ve = eastVel();
    float vn = northVel();
    return sqrt(ve*ve + vn*vn);
}

Q_INVOKABLE bool VehicleDataManager::engageStatus() const
{
    return _vehicle_data->fcc_configuration.fbw_engaged;
}

Q_INVOKABLE int VehicleDataManager::engageNumber() const
{
    return static_cast<int>(_vehicle_data->fcc_configuration.engagement_number);
}
