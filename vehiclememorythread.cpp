#include "vehiclememorythread.h"

VehicleMemoryThread::VehicleMemoryThread(shared_data_t* shm, QObject *parent)
    : QThread{parent},
      _shm(shm),
      _isEngaged(false)
{
    _engageNumber = -1;
}

void VehicleMemoryThread::run() {
    while (true) {
        usleep(10000);

        if (_shm->fcc_configuration.engagement_number != _engageNumber) {
            _engageNumber = _shm->fcc_configuration.engagement_number;
            emit engageNumberChanged(_engageNumber);
        }

        if (_shm->fcc_configuration.fbw_engaged != _isEngaged) {
            _isEngaged = static_cast<bool>(_shm->fcc_configuration.fbw_engaged);
            emit engageChanged(_isEngaged);
        }

        if ((_shm->ffs_output.ffs_switches & (1 << 12)) != _isMinimized) {
            _isMinimized = (_shm->ffs_output.ffs_switches & (1 << 12));
            emit minimizeSwitchChanged(_isMinimized);
        }

    }
}
