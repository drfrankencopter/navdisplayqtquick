import QtQuick 2.2
import QtQuick.Controls 2.0
import QtQml.Models 2.2
import "Menus"
Rectangle {
    id: menuBackground
    anchors.fill: parent
    property real alpha:0.1
    color: Qt.rgba(0.1,0.1,0.1,alpha)

    NumberAnimation {
        target: menuBackground
        property: "alpha"
        from:0.1
        to:0.7
        duration: 400
        easing.type: Easing.InOutQuad
        running:menuBackground.visible
    }
    NumberAnimation {
        target: menuBackground
        property: "opacity"
        from:menuBackground.opacity
        to:1
        duration: 200
        easing.type: Easing.InOutQuad
        running:menuBackground.visible
    }
    NumberAnimation {
        id:menuFade
        target: menuBackground
        property: "opacity"
        from: menuBackground.opacity
        to:0
        duration: 200
        easing.type: Easing.InOutQuad
        onStopped: {
            menuScreen.visible=false
        }
    }
    MouseArea{
        anchors.fill: parent
        onClicked:menuFade.start()
    }

    StyleMenu{
        id:styleMenu
        visible:false
        objectName: "Style Menu"

    }

    ControllerMenu{
        id:controllerMenu
        visible:false
        objectName:"Controller Menu"
    }

    MissionMenu{
        id:missionMenu
        visible:false
        objectName: "Mission Menu"
    }

    MiscMenu{
        id:miscMenu
        visible:false
        objectName: "Misc Menu"
    }

    Rectangle{
        id:menuHolder
        anchors.centerIn: parent
        radius: 5
        width:parent.width * 0.85
        height: parent.height * 0.85
        color: "white"
        clip:true
        MouseArea{
            anchors.fill: parent
        }

        NumberAnimation {
            target: menuHolder
            property: "height"
            from:0
            to:menuHolder.parent.height * 0.85
            duration: 400
            easing.type: Easing.OutQuad
            running:menuHolder.visible
        }
        Item{
            anchors{
                top:parent.top
                right:parent.right
                bottom:menuStackView.top
                left:parent.left
            }
            MouseArea{
                anchors.fill: parent
            }

            Image{
                id:headerBackImg
                source:"qrc:/imgs/imgs/back_icon.png"
                anchors{
                    left:parent.left
                    margins:10
                }
                height:parent.height*0.6
                width:parent.height*0.6
                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        if(menuStackView.depth>1){
                            menuStackView.pop()
                        }else{
                            menuFade.start()
                        }
                    }

                    onEntered: {
                        cursorShape = Qt.PointingHandCursor
                    }

                    onExited: {
                        cursorShape = Qt.ArrowCursor
                    }
                }
            }

            Text {
                id: headerTxt
                anchors{
                    left:headerBackImg.right
                    verticalCenter: headerBackImg.verticalCenter
                }
                font{
                    pixelSize: 20
                    underline:true
                }
                width:parent.width*2
                text: menuStackView.currentItem.objectName
                clip:true
                onTextChanged: NumberAnimation {
                    target: headerTxt
                    property: "width"
                    from: 0
                    to:headerTxt.width
                    duration: 700
                    easing.type: Easing.InOutQuad
                }
            }
        }

        StackView{
            id: menuStackView
            anchors{
                left: parent.left
                right:parent.right
                top:parent.top
                topMargin:parent.height*0.1
                bottom:parent.bottom
            }
            property var menuMap: {
                "Style Menu": styleMenu,
                "Controller Menu": controllerMenu,
                "Mission Menu": missionMenu,
                "Misc Menu": miscMenu
            }

            initialItem: ListView{
                id:listview
                objectName: "Main Menu"
                spacing:(height/count)-(240/listview.count)
                model:ListModel{
                    ListElement {
                        menu:"Style Menu"
                        title: "Style"
                        icon:"qrc:/imgs/imgs/layer.png"
                    }
                    ListElement {
                        menu:"Controller Menu"
                        title: "Controller"
                        icon:"qrc:/imgs/imgs/Controller.png"
                    }
                    ListElement {
                        menu:"Mission Menu"
                        title: "Mission"
                        icon: "qrc:/imgs/imgs/waypoint.png"
                    }
                    ListElement {
                        menu:"Misc Menu"
                        title: "Miscellaneous"
                        icon: "qrc:/imgs/imgs/setting_icon.png"
                    }

                }
                delegate: Rectangle{
                    id:menuDelegateItem
                    color: menuColor
                    width:parent.width*0.8
                    height: (menuHolder.height/3)/listview.count
                    radius:4
                    anchors.horizontalCenter:parent.horizontalCenter

                    Image{
                        id:menuDelegateImg
                        source:model.icon
                        height:parent.height*0.5
                        width:parent.height*0.5
                        anchors{
                            verticalCenter: parent.verticalCenter
                            left:parent.left
                            leftMargin:10
                        }
                    }

                    Text{
                        id:menuDelegateText
                        text: model.title
                        anchors{
                            verticalCenter: parent.verticalCenter
                            left:menuDelegateImg.right
                            leftMargin:10
                        }
                        font.pixelSize: 20

                    }
                    MouseArea{
                        anchors.fill: parent
                        hoverEnabled: true
                        onClicked: {
                            menuStackView.push(menuStackView.menuMap[model.menu])
                        }
                        onEntered: {
                            cursorShape = Qt.PointingHandCursor
                            parent.color = Qt.rgba(0.9,0.9,0.9,1)
                            hoverOnAnim.start()
                        }

                        onExited: {
                            cursorShape = Qt.ArrowCursor
                            parent.color = Qt.rgba(0.9,0.9,0.9,0.5)
                            hoverOffAnim.start()
                        }
                        NumberAnimation {
                            id:hoverOnAnim
                            target: menuDelegateItem
                            property: "width"
                            from:menuDelegateItem.width
                            to:menuDelegateItem.width*1.2
                            duration: 300
                            easing.type: Easing.OutQuart
                        }
                        NumberAnimation {
                            id:hoverOffAnim
                            target: menuDelegateItem
                            property: "width"
                            from:menuDelegateItem.width
                            to:menuDelegateItem.parent.width*0.8
                            duration: 300
                            easing.type: Easing.OutQuart
                        }
                    }

                }
            }
            pushEnter: Transition {
                PropertyAnimation {
                    property: "opacity"
                    from: 0
                    to:1
                    duration: 200
                }
            }
            pushExit: Transition {
                PropertyAnimation {
                    property: "opacity"
                    from: 1
                    to:0
                    duration: 200
                }
            }
            popEnter: Transition {
                PropertyAnimation {
                    property: "opacity"
                    from: 0
                    to:1
                    duration: 200
                }
            }
            popExit: Transition {
                PropertyAnimation {
                    property: "opacity"
                    from: 1
                    to:0
                    duration: 200
                }
            }
        }
    }
}
