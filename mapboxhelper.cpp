#include "mapboxhelper.h"

#include <QJsonDocument>
#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonArray>
#include <limits>

void MapboxHelper::getAltitudeAt(double lat, double lon,double distance)
{
    QUrl url("https://api.mapbox.com/v4/mapbox.mapbox-terrain-v2/tilequery/"+QString::number(lon)+","+QString::number(lat)+".json?&access_token="+"pk.eyJ1IjoiY3ZsYWQiLCJhIjoiY2t5YnU5ZWFpMDBteTJvcGF5dGJyMXczeCJ9.yuupYYFrEW7-mNY7j213Wg");
    QNetworkReply* rq = m_manager.get(QNetworkRequest(url));
    connect(rq, &QNetworkReply::readyRead, this, [=](){
            QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
            if (reply)
            {
                QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());

                if (doc.isNull())
                {
                    qDebug() << "Invalid Json";
                    return;
                }

                auto alt_result = _parseForAltitude(doc);

                emit groundAltitudeReady(distance, alt_result.first, alt_result.second);
            }
});
}

void MapboxHelper::getAltitudeAt(double lat, double lon)
{
    QUrl url("https://api.mapbox.com/v4/mapbox.mapbox-terrain-v2/tilequery/"+QString::number(lon)+","+QString::number(lat)+".json?&access_token="+"pk.eyJ1IjoiY3ZsYWQiLCJhIjoiY2t5YnU5ZWFpMDBteTJvcGF5dGJyMXczeCJ9.yuupYYFrEW7-mNY7j213Wg");
    QNetworkReply* rq = m_manager.get(QNetworkRequest(url));
    connect(rq, &QNetworkReply::readyRead, this, &MapboxHelper::handleGroundAltitude);
}

void MapboxHelper::handleGroundAltitude()
{

    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());

    if (reply)
    {
        QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());

        if (doc.isNull())
        {
            qDebug() << "Invalid Json";
            return;
        }

        auto alt_result = _parseForAltitude(doc);

        emit groundAltitudeReadyAtPoint(alt_result.first, alt_result.second);
    }
}

QPair<double, bool> MapboxHelper::_parseForAltitude(const QJsonDocument& doc)
{
    QPair<double, bool> result;
    result.first    = std::numeric_limits<double>::min();
    result.second   = false;

    auto features = doc.object()["features"].toArray();
    for (auto it = features.begin(); it != features.end(); it++)
    {
        auto properties = it->toObject()["properties"];
        auto layer_type = properties.toObject()["tilequery"].toObject()["layer"].toString();

        if (layer_type == "contour")
        {
            double ele = properties.toObject()["ele"].toDouble();
            result.first    = std::max(result.first, ele);
            result.second   = true;
        }
    }

    return result;
}
