#ifndef MAPCONTROLLER_H
#define MAPCONTROLLER_H


#include <QObject>
#include <QTimer>
#include <memory>
class SharedMemoryManager;
class MapController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(double lat READ lat WRITE setLat NOTIFY update)
    Q_PROPERTY(double lng READ lng WRITE setLng NOTIFY update)
    Q_PROPERTY(double rot READ rot WRITE setRot NOTIFY update)

public:
    explicit MapController(QObject *parent = nullptr,SharedMemoryManager* sharedmem=nullptr);
    ~MapController();
    bool isRunning();
public slots:
    void toggle();
private:
    std::unique_ptr<QThread> m_thread;
    QTimer* m_timer;
    SharedMemoryManager* m_sharedMemory;
private:
    double lat();
    void setLat(double lat);
    double lng();
    void setLng(double lng);
    double rot();
    void setRot(double rot);
signals:
    void update();
};

#endif // MAPCONTROLLER_H
