#ifndef LIGHTPALETTE_H
#define LIGHTPALETTE_H

#include "palettebase.h"
#include <QObject>

class LightPalette : public PaletteBase
{
    Q_OBJECT
public:
    explicit LightPalette(QObject *parent = nullptr);

    DEFINE_COLOR_PARAM(hudColor, "#ffffff")     // Should be white
    DEFINE_COLOR_PARAM(hudTextColor, "#000000") // Should be grayish
    DEFINE_COLOR_PARAM(hudTextHoverColor, "#000000")
    DEFINE_COLOR_PARAM(hudHoverColor, "#eeeeee")
    DEFINE_COLOR_PARAM(generalText, "#000000")
    DEFINE_COLOR_PARAM(hudIcons, "#ffffff")
    DEFINE_COLOR_PARAM(rangeRings, "white")
    DEFINE_COLOR_PARAM(climbArrow, "white")
};

#endif // LIGHTPALETTE_H
