#include <QDebug>

#include "paletteholder.h"

PaletteHolder::PaletteHolder(NavSettings* settings, QObject *parent)
    : QObject{parent}
    , _currentType(PaletteBase::PaletteType::Undefined)
    , _currentPalette(nullptr)
{
    auto savedPalette = static_cast<PaletteBase::PaletteType>(settings->colorPalette());
    changePalette(savedPalette);
    connect(settings, &NavSettings::colorPaletteChanged, this, &PaletteHolder::handleColorPaletteChanged);

}

void PaletteHolder::handleColorPaletteChanged(int type)
{
    changePalette(type);
}

void PaletteHolder::changePalette(int type)
{
    changePalette(static_cast<PaletteBase::PaletteType>(type));
}

void PaletteHolder::changePalette(PaletteBase::PaletteType type)
{
    if (_currentType == type) {
        // Nothing to do
        return;
    }

    PaletteBase* newPalette = PaletteBase::createPalette(type);

    if (newPalette == nullptr) {
        // Do nothing on wrong type.
        return;
    }

    delete _currentPalette;
    _currentPalette = newPalette;
    _currentType    = type;
    emit paletteChanged();
}
