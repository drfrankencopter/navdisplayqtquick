#include <QDebug>

#include "palettebase.h"
#include "lightpalette.h"
#include "darkpalette.h"

PaletteBase::PaletteBase(QObject *parent)
    : QObject{parent}
{

}

PaletteBase* PaletteBase::createPalette(PaletteType type)
{
    PaletteBase* newPalette = nullptr;

    switch (type) {
    case PaletteBase::Light:
        newPalette = new LightPalette();
        break;
    case PaletteBase::Dark:
        newPalette = new DarkPalette();
        break;
    case PaletteBase::HighContrast:
        //newPalette = new HighContrastPalette();
        break;
    default:
        qDebug() << "Unrecognized palette type.";
        return newPalette;
    }

    return newPalette;
}
