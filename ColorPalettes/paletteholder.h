#ifndef PALETTEHOLDER_H
#define PALETTEHOLDER_H

#include <QObject>

#include "navsettings.h"
#include "palettebase.h"

class PaletteHolder : public QObject
{
    Q_OBJECT
public:
    explicit PaletteHolder(NavSettings* settings, QObject *parent = nullptr);

    Q_PROPERTY(QColor hudColor          READ hudColor           NOTIFY paletteChanged)
    Q_PROPERTY(QColor hudTextColor      READ hudTextColor       NOTIFY paletteChanged)
    Q_PROPERTY(QColor hudTextHoverColor READ hudTextHoverColor  NOTIFY paletteChanged)
    Q_PROPERTY(QColor hudHoverColor     READ hudHoverColor      NOTIFY paletteChanged)
    Q_PROPERTY(QColor generalText       READ generalText        NOTIFY paletteChanged)
    Q_PROPERTY(QColor hudIcons          READ hudIcons           NOTIFY paletteChanged)
    Q_PROPERTY(QColor rangeRings        READ rangeRings         NOTIFY paletteChanged)
    Q_PROPERTY(QColor climbArrow        READ climbArrow         NOTIFY paletteChanged)

    QColor  hudColor()          const { return _currentPalette->hudColor(); }
    QColor  hudTextColor()      const { return _currentPalette->hudTextColor(); }
    QColor  hudTextHoverColor() const { return _currentPalette->hudTextHoverColor(); }
    QColor  hudHoverColor()     const { return _currentPalette->hudHoverColor(); }
    QColor  generalText()       const { return _currentPalette->generalText(); }
    QColor  hudIcons()          const { return _currentPalette->hudIcons(); }
    QColor  rangeRings()        const { return _currentPalette->rangeRings(); }
    QColor  climbArrow()        const { return _currentPalette->climbArrow(); }

    Q_INVOKABLE int getCurrentType() { return static_cast<int>(_currentType); }

signals:
    void paletteChanged();

public slots:
    void handleColorPaletteChanged(int type);

private:
    void changePalette(int type);
    void changePalette(PaletteBase::PaletteType type);

    PaletteBase::PaletteType    _currentType;
    PaletteBase*                _currentPalette;
};

#endif // PALETTEHOLDER_H
