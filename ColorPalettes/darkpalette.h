#ifndef DARKPALETTE_H
#define DARKPALETTE_H

#include "palettebase.h"
#include <QObject>

class DarkPalette : public PaletteBase
{
    Q_OBJECT
public:
    explicit DarkPalette(QObject *parent = nullptr);

    DEFINE_COLOR_PARAM(hudColor, "#555555")     // Should be grayish
    DEFINE_COLOR_PARAM(hudTextColor, "#ffffff") // Should be white
    DEFINE_COLOR_PARAM(hudTextHoverColor, "#eeeeee")
    DEFINE_COLOR_PARAM(hudHoverColor, "#444444")
    DEFINE_COLOR_PARAM(generalText, "#ffffff")
    DEFINE_COLOR_PARAM(hudIcons, "#000000")
    DEFINE_COLOR_PARAM(rangeRings, "black")
    DEFINE_COLOR_PARAM(climbArrow, "black")
};

#endif // DARKPALETTE_H
