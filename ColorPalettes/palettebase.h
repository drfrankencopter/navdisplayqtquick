#ifndef PALETTEBASE_H
#define PALETTEBASE_H

#include <QObject>
#include <QColor>
#include <QQmlEngine>

#define DEFINE_COLOR_PARAM(paramName, colorString)          \
    private:                                                \
    QColor _ ## paramName = QColor(colorString);           \
    public:                                                 \
    QColor paramName() override { return _ ## paramName; }  \

#define DECLARE_COLOR_PARAM(paramName)                      \
    virtual QColor paramName() = 0;                         \

// It's the base classe for every color palette.
// This color palette base determines the properties the function should have to define a color palette.
// For example, each color palettes needs to have the same colors defined. If we don't have a color
// for the Hud Menu what do we display?
//
// All you should have to do to create new color palette is create a new class and inherit this base class.
// You will of course need to define every color in your new palette.

class PaletteBase : public QObject
{
    Q_OBJECT
public:
    explicit PaletteBase(QObject *parent = nullptr);

    enum PaletteType {
        Light,
        Dark,
        HighContrast,
        Undefined
    };

    Q_ENUM(PaletteType)

    static PaletteBase* createPalette(PaletteType type);
    static void declareQml() {
        qmlRegisterUncreatableType<PaletteBase>("Palettes", 1, 0, "Palette", "Just for enums access");
    }

    // Declare all the color names here
    DECLARE_COLOR_PARAM(hudColor)
    DECLARE_COLOR_PARAM(hudTextColor)
    DECLARE_COLOR_PARAM(hudTextHoverColor)
    DECLARE_COLOR_PARAM(hudHoverColor)
    DECLARE_COLOR_PARAM(generalText)
    DECLARE_COLOR_PARAM(hudIcons)
    DECLARE_COLOR_PARAM(rangeRings)
    DECLARE_COLOR_PARAM(climbArrow)
};

#endif // PALETTEBASE_H
