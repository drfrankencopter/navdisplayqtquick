#include "navsettings.h"
#include <QDebug>
NavSettings::NavSettings()
    : settings(new QSettings("National Research Council", "NavDisplay"))
{
}

NavSettings::~NavSettings()
{
    delete settings;
}

void NavSettings::setValue(const QString& key, const QVariant& value)
{
    settings->setValue(key, value);
}

int NavSettings::getintsetting (const QString& key, const QVariant& def) const
{
    return settings->value(key, def).toInt();
}

bool NavSettings::getboolsetting (const QString& key, const QVariant& def) const
{
    return settings->value(key, def).toBool();
}

float NavSettings::getfloatsetting (const QString& key, const QVariant& def) const
{
    return settings->value(key, def).toFloat();
}

double NavSettings::getdoublesetting (const QString& key, const QVariant& def) const
{
    return settings->value(key, def).toDouble();
}

QString NavSettings::getQStringsetting (const QString& key, const QVariant& def) const
{
    return settings->value(key, def).toString();
}

QColor NavSettings::getQColorsetting (const QString& key, const QVariant& def) const
{
    return settings->value(key, def).value<QColor>();
}
