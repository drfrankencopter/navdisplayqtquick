#include "mapcontroller.h"
#include "sharedmemorymanager.h"
#include "QThread"
#include "QDebug"
#include <qtimer.h>
#define MAP_REFRESH_RATE 5

MapController::MapController(QObject *parent,SharedMemoryManager* sharedmem)
    : QObject{parent},m_sharedMemory(sharedmem)
{
    m_thread.reset(new QThread);
    m_timer=new QTimer();
    //connect(m_timer,&QTimer::timeout,this,&MapController::updateMap);
    //connect(m_window,&MapWindow::onToggleController,this,&MapController::toggleLoop); //Connect here
    this->moveToThread(m_thread.get());
    m_timer->moveToThread(m_thread.get());
    m_thread->start();
}

MapController::~MapController()
{

    if(m_thread->isRunning()){
        m_thread.release();
    }
    m_timer->deleteLater();
}

void MapController::toggle()
{
    if(!m_timer->isActive()){
        qDebug()<<"starting timer";
        m_timer->start((1000/MAP_REFRESH_RATE));
    }
    else if(m_timer->isActive()){
        qDebug()<<"stoping timer";
        m_timer->stop();
    }
}

double MapController::lat()
{
    if(!m_sharedMemory->connected())
           return 0;

}

void MapController::setLat(double lat)
{

}

double MapController::lng()
{
    if(!m_sharedMemory->connected())
           return 0;
}

void MapController::setLng(double lng)
{

}

double MapController::rot()
{
    if(!m_sharedMemory->connected())
           return 0;
}

void MapController::setRot(double rot)
{

}


bool MapController::isRunning()
{
    return m_timer->isActive();
}



//void MapController::updateMap(){
//    //qDebug()<<"Updating..";

//    if(!m_window->sharedMemory()->connected())
//        return;
//    auto data = m_window->sharedMemory()->data();
//    if(!data)
//        return;
//    m_viewType->updateMap(m_window, QPair<double,double>{data->hg1700.lat,data->hg1700.lng},data->hg1700.hdg);
//    //zoom under conditions?
//}


